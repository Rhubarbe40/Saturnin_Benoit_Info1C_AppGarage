"""
    Fichier : gestion_films_genres_crud.py
    Auteur : OM 2021.05.01
    Gestions des "routes" FLASK et des données pour l'association entre les films et les genres.
"""
import sys

import pymysql
from flask import flash
from flask import redirect
from flask import render_template
from flask import request
from flask import session
from flask import url_for

from APP_Garage import obj_mon_application
from APP_Garage.database.connect_db_context_manager import MaBaseDeDonnee
from APP_Garage.erreurs.exceptions import *
from APP_Garage.erreurs.msg_erreurs import *
from APP_Garage.persPneu.gestion_pers_wtf_forms import FormWTFAjouterPersonne
from APP_Garage.persPneu.gestion_pers_wtf_forms import FormWTFDeletePersonne
from APP_Garage.persPneu.gestion_pers_wtf_forms import FormWTFUpdatePersonne

"""
    Nom : persPneu_afficher
    Auteur : OM 2021.05.01
    Définition d'une "route" /persPneu_afficher
    
    But : Afficher les personnes avec les pneus associés pour chaque personne.
    
    Paramètres : id_pneu_sel = 0 >> toutes les personnes.
                 id_pneu_sel = "n" affiche la personne dont l'id est "n"
                 
"""


@obj_mon_application.route("/persPneu_afficher/<int:id_personne_sel>", methods=['GET', 'POST'])
def persPneu_afficher(id_personne_sel):
    if request.method == "GET":
        try:
            try:
                # Renvoie une erreur si la connexion est perdue.
                MaBaseDeDonnee().connexion_bd.ping(False)
            except Exception as Exception_init_persPneu_afficher:
                code, msg = Exception_init_persPneu_afficher.args
                flash(f"{error_codes.get(code, msg)} ", "danger")
                flash(f"Exception _init_persPneu_afficher problème de connexion BD : {sys.exc_info()[0]} "
                      f"{Exception_init_persPneu_afficher.args[0]} , "
                      f"{Exception_init_persPneu_afficher}", "danger")
                raise MaBdErreurConnexion(f"{msg_erreurs['ErreurConnexionBD']['message']} {erreur.args[0]}")

            with MaBaseDeDonnee().connexion_bd.cursor() as mc_afficher:
                strsql_persPneu_afficher_data = """SELECT id_personne, nom_personne, prenom_personne, naissance_personne, num_tel, mail_pers,
                                                            GROUP_CONCAT(Taille_pneu) as PneuPersonne FROM t_avoir_pneu
                                                            RIGHT JOIN t_personne ON t_personne.id_personne = t_avoir_pneu.fk_personne
                                                            LEFT JOIN t_pneu ON t_pneu.id_pneu = t_avoir_pneu.fk_pneu
                                                            GROUP BY id_personne"""
                if id_personne_sel == 0:
                    # le paramètre 0 permet d'afficher tous les films
                    # Sinon le paramètre représente la valeur de l'id du film
                    mc_afficher.execute(strsql_persPneu_afficher_data)
                else:
                    # Constitution d'un dictionnaire pour associer l'id de la personne sélectionné avec un nom de variable
                    valeur_id_personne_selected_dictionnaire = {"value_id_personne_selected": id_personne_sel}
                    # En MySql l'instruction HAVING fonctionne comme un WHERE... mais doit être associée à un GROUP BY
                    # L'opérateur += permet de concaténer une nouvelle valeur à la valeur de gauche préalablement définie.
                    strsql_persPneu_afficher_data += """ HAVING id_personne= %(value_id_personne_selected)s"""

                    mc_afficher.execute(strsql_persPneu_afficher_data, valeur_id_personne_selected_dictionnaire)

                # Récupère les données de la requête.
                data_persPneu_afficher = mc_afficher.fetchall()
                print("data_pneu ", data_persPneu_afficher, " Type : ", type(data_persPneu_afficher))

                # Différencier les messages.
                if not data_persPneu_afficher and id_personne_sel == 0:
                    flash("""La table "t_personne" est vide. !""", "warning")
                elif not data_persPneu_afficher and id_personne_sel > 0:
                    # Si l'utilisateur change l'id_personne dans l'URL et qu'il ne correspond à aucune personne
                    flash(f"La personne {id_personne_sel} demandé n'existe pas !!", "warning")
                else:
                    flash(f"Données Personnes et pneus affichés !!", "success")

        except Exception as Exception_persPneu_afficher:
            code, msg = Exception_persPneu_afficher.args
            flash(f"{error_codes.get(code, msg)} ", "danger")
            flash(f"Exception persPneu_afficher : {sys.exc_info()[0]} "
                  f"{Exception_persPneu_afficher.args[0]} , "
                  f"{Exception_persPneu_afficher}", "danger")

    # Envoie la page "HTML" au serveur.
    return render_template("persPneu/persPneu_afficher.html", data=data_persPneu_afficher)


"""
    nom: edit_persPneu_selected
    On obtient un objet "objet_dumpbd"

    Récupère la liste de toutes les pneus des personnes sélectionné par le bouton "MODIFIER" de "persPneu_afficher.html"
    
    Dans une liste déroulante particulière (tags-selector-tagselect), on voit :
    1) Tous les pneus contenus dans la "t_pneu".
    2) Les pneus attribués au personne selectionné.
    3) Les pneus non-attribués au personne sélectionné.

    On signale les erreurs importantes

"""


@obj_mon_application.route("/edit_persPneu_selected", methods=['GET', 'POST'])
def edit_persPneu_selected():
    if request.method == "GET":
        try:
            with MaBaseDeDonnee().connexion_bd.cursor() as mc_afficher:
                strsql_pneu_afficher = """SELECT id_pneu, Taille_pneu FROM t_pneu ORDER BY id_pneu ASC"""
                mc_afficher.execute(strsql_pneu_afficher)
            data_pneu_all = mc_afficher.fetchall()
            print("dans edit_persPneu_selected ---> data_pneu_all", data_pneu_all)

            # Récupère la valeur de "id_personne" du formulaire html "persPneu_afficher.html"
            # l'utilisateur clique sur le bouton "Modifier" et on récupère la valeur de "id_personne"
            # grâce à la variable "id_persPneu_edit_html" dans le fichier "persPneu_afficher.html"
            # href="{{ url_for('edit_persPneu_selected', id_persPneu_edit_html=row.id_personne) }}"
            id_persPneu_edit = request.values['id_persPneu_edit_html']

            # Mémorise l'id de la Peronne dans une variable de session
            # (ici la sécurité de l'application n'est pas engagée)
            # il faut éviter de stocker des données sensibles dans des variables de sessions.
            session['session_id_persPneu_edit'] = id_persPneu_edit

            # Constitution d'un dictionnaire pour associer l'id du film sélectionné avec un nom de variable
            valeur_id_personne_selected_dictionnaire = {"value_id_personne_selected": id_persPneu_edit}

            # Récupère les données grâce à 3 requêtes MySql définie dans la fonction persPneu_afficher_data
            # 1) Sélection de la personne choisi
            # 2) Sélection des pneus "déjà" attribués pour la personne.
            # 3) Sélection des pneus "pas encore" attribués pour la personne choisi.
            # ATTENTION à l'ordre d'assignation des variables retournées par la fonction "persPneu_afficher_data"
            data_persPneu_selected, data_persPneu_non_attribues, data_persPneu_attribues = \
                persPneu_afficher_data(valeur_id_personne_selected_dictionnaire)

            print(data_persPneu_selected)
            lst_data_personne_selected = [item['id_personne'] for item in data_persPneu_selected]
            print("lst_data_personne_selected  ", lst_data_personne_selected,
                  type(lst_data_personne_selected))

            # Dans le composant "tags-selector-tagselect" on doit connaître
            # les genres qui ne sont pas encore sélectionnés.
            lst_data_persPneu_non_attribues = [item['id_pneu'] for item in data_persPneu_non_attribues]
            session['session_lst_data_persPneu_non_attribues'] = lst_data_persPneu_non_attribues
            print("lst_data_persPneu_non_attribues  ", lst_data_persPneu_non_attribues,
                  type(lst_data_persPneu_non_attribues))

            # Dans le composant "tags-selector-tagselect" on doit connaître
            # les genres qui sont déjà sélectionnés.
            lst_data_persPneu_old_attribues = [item['id_pneu'] for item in data_persPneu_attribues]
            session['session_lst_data_persPneu_old_attribues'] = lst_data_persPneu_old_attribues
            print("lst_data_persPneu_old_attribues  ", lst_data_persPneu_old_attribues,
                  type(lst_data_persPneu_old_attribues))

            print(" data data_persPneu_selected", data_persPneu_selected, "type ", type(data_persPneu_selected))
            print(" data data_persPneu_non_attribues ", data_persPneu_non_attribues, "type ",
                  type(data_persPneu_non_attribues))
            print(" data_persPneu_attribues ", data_persPneu_attribues, "type ",
                  type(data_persPneu_attribues))

            # Extrait les valeurs contenues dans la table "t_pneu", colonne "Taille_pneu"
            # Le composant javascript "tagify" pour afficher les tags n'a pas besoin de l'id_pneu
            lst_data_persPneu_non_attribues = [item['Taille_pneu'] for item in data_persPneu_non_attribues]
            print("lst_all_pneu gf_edit_persPneu_selected ", lst_data_persPneu_non_attribues,
                  type(lst_data_persPneu_non_attribues))

        except Exception as Exception_edit_persPneu_selected:
            code, msg = Exception_edit_persPneu_selected.args
            flash(f"{error_codes.get(code, msg)} ", "danger")
            flash(f"Exception edit_persPneu_selected : {sys.exc_info()[0]} "
                  f"{Exception_edit_persPneu_selected.args[0]} , "
                  f"{Exception_edit_persPneu_selected}", "danger")

    return render_template("persPneu/persPneu_modifier_tags_dropbox.html",
                           data_genres=data_pneu_all,
                           data_personne_selected=data_persPneu_selected,
                           data_pneu_attribues=data_persPneu_attribues,
                           data_pneu_non_attribues=data_persPneu_non_attribues)


"""
    nom: update_persPneu_selected

    Récupère la liste de tous les pneus de la personne sélectionnée par le bouton "MODIFIER" de "persPneu_afficher.html"
    
    Dans une liste déroulante particulière (tags-selector-tagselect), on voit :
    1) Tous les pneus contenus dans la "t_pneu".
    2) Les pneus attribués à la personne selectionné.
    3) Les persPneu non-attribués à la personne sélectionné.

    On signale les erreurs importantes
"""


@obj_mon_application.route("/update_persPneu_selected", methods=['GET', 'POST'])
def update_persPneu_selected():
    if request.method == "POST":
        try:
            # Récupère l'id de la personne sélectionnée
            id_personne_selected = session['session_id_persPneu_edit']
            print("session['session_id_persPneu_edit'] ", session['session_id_persPneu_edit'])

            # Récupère la liste des pneus qui ne sont pas associés à la personne sélectionnée.
            old_lst_data_persPneu_non_attribues = session['session_lst_data_persPneu_non_attribues']
            print("old_lst_data_persPneu_non_attribues ", old_lst_data_persPneu_non_attribues)

            # Récupère la liste des pneus qui sont associés à la personne sélectionnée.
            old_lst_data_persPneu_attribues = session['session_lst_data_persPneu_old_attribues']
            print("old_lst_data_persPneu_old_attribues ", old_lst_data_persPneu_attribues)

            # Effacer toutes les variables de session.
            session.clear()

            # Récupère ce que l'utilisateur veut modifier comme pneus dans le composant "tags-selector-tagselect"
            # dans le fichier "persPneu_modifier_tags_dropbox.html"
            new_lst_str_persPneu = request.form.getlist('name_select_tags')
            print("new_lst_str_persPneu ", new_lst_str_persPneu)

            # OM 2021.05.02 Exemple : Dans "name_select_tags" il y a ['4','65','2']
            # On transforme en une liste de valeurs numériques. [4,65,2]
            new_lst_int_persPneu_old = list(map(int, new_lst_str_persPneu))
            print("new_lst_persPneu ", new_lst_int_persPneu_old, "type new_lst_persPneu ",
                  type(new_lst_int_persPneu_old))

            # Pour apprécier la facilité de la vie en Python... "les ensembles en Python"
            # https://fr.wikibooks.org/wiki/Programmation_Python/Ensembles
            # OM 2021.05.02 Une liste de "id_pneu" qui doivent être effacés de la table intermédiaire "t_avoir_pneu".
            lst_diff_pneu_delete_b = list(
                set(old_lst_data_persPneu_attribues) - set(new_lst_int_persPneu_old))
            print("lst_diff_pneu_delete_b ", lst_diff_pneu_delete_b)

            # Une liste de "id_pneu" qui doivent être ajoutés à la "t_avoir_pneu"
            lst_diff_pneu_insert_a = list(
                set(new_lst_int_persPneu_old) - set(old_lst_data_persPneu_attribues))
            print("lst_diff_pneu_insert_a ", lst_diff_pneu_insert_a)

            # SQL pour insérer une nouvelle association entre
            # "fk_personne"/"id_personne" et "fk_pneu"/"id_pneu" dans la "t_avoir_pneu"
            strsql_insert_persPneu = """INSERT INTO t_avoir_pneu (id_avoir_pneu, fk_pneu, fk_personne)
                                                    VALUES (NULL, %(value_fk_pneu)s, %(value_fk_personne)s)"""

            # SQL pour effacer une (des) association(s) existantes entre "id_personne" et "id_pneu" dans la "t_avoir_pneu"
            strsql_delete_persPneu = """DELETE FROM t_avoir_pneu WHERE fk_pneu = %(value_fk_pneu)s AND fk_personne = %(value_fk_personne)s"""

            with MaBaseDeDonnee() as mconn_bd:
                # Pour la personne sélectionné, parcourir la liste des pneus à INSÉRER dans la "t_avoir_pneu".
                # Si la liste est vide, la boucle n'est pas parcourue.
                for id_pneu_ins in lst_diff_pneu_insert_a:
                    # Constitution d'un dictionnaire pour associer l'id de la personne sélectionné avec un nom de variable
                    # et "id_pneu_ins" (l'id du pneu dans la liste) associé à une variable.
                    valeurs_personne_sel_pneu_sel_dictionnaire = {"value_fk_personne": id_personne_selected,
                                                               "value_fk_pneu": id_pneu_ins}

                    mconn_bd.mabd_execute(strsql_insert_persPneu, valeurs_personne_sel_pneu_sel_dictionnaire)

                # Pour la personne sélectionné, parcourir la liste des pneus à EFFACER dans la "t_avoir_pneu".
                # Si la liste est vide, la boucle n'est pas parcourue.
                for id_pneu_del in lst_diff_pneu_delete_b:
                    # Constitution d'un dictionnaire pour associer l'id de la personne sélectionné avec un nom de variable
                    # et "id_pneu_del" (l'id du pneu dans la liste) associé à une variable.
                    valeurs_personne_sel_pneu_sel_dictionnaire = {"value_fk_personne": id_personne_selected,
                                                               "value_fk_pneu": id_pneu_del}

                    # Du fait de l'utilisation des "context managers" on accède au curseur grâce au "with".
                    # la subtilité consiste à avoir une méthode "mabd_execute" dans la classe "MaBaseDeDonnee"
                    # ainsi quand elle aura terminé l'insertion des données le destructeur de la classe "MaBaseDeDonnee"
                    # sera interprété, ainsi on fera automatiquement un commit
                    mconn_bd.mabd_execute(strsql_delete_persPneu, valeurs_personne_sel_pneu_sel_dictionnaire)

        except Exception as Exception_update_persPneu_selected:
            code, msg = Exception_update_persPneu_selected.args
            flash(f"{error_codes.get(code, msg)} ", "danger")
            flash(f"Exception update_persPneu_selected : {sys.exc_info()[0]} "
                  f"{Exception_update_persPneu_selected.args[0]} , "
                  f"{Exception_update_persPneu_selected}", "danger")

    # Après cette mise à jour de la table intermédiaire "t_avoir_pneu",
    # on affiche les personnes et le(urs) pneu(s) associé(s).
    return redirect(url_for('persPneu_afficher', id_personne_sel=id_personne_selected))


"""
    nom: persPneu_afficher_data

    Récupère la liste de tous les pneus de la personne sélectionné par le bouton "MODIFIER" de "persPneu_afficher.html"
    Nécessaire pour afficher tous les "TAGS" des pneus, ainsi l'utilisateur voit les pneus à disposition

    On signale les erreurs importantes
"""


def persPneu_afficher_data(valeur_id_personne_selected_dict):
    print("valeur_id_personne_selected_dict...", valeur_id_personne_selected_dict)
    try:

        strsql_personne_selected = """SELECT id_personne, nom_personne, prenom_personne, naissance_personne, num_tel, mail_pers, GROUP_CONCAT(id_pneu) as PneuPersonne FROM t_avoir_pneu
                                        INNER JOIN t_personne ON t_personne.id_personne = t_avoir_pneu.fk_personne
                                        INNER JOIN t_pneu ON t_pneu.id_pneu = t_avoir_pneu.fk_pneu
                                        WHERE id_personne = %(value_id_personne_selected)s"""

        strsql_persPneu_non_attribues = """SELECT id_pneu, Taille_pneu FROM t_pneu WHERE id_pneu not in(SELECT id_pneu as idPneuPersonne FROM t_avoir_pneu
                                                    INNER JOIN t_personne ON t_personne.id_personne = t_avoir_pneu.fk_personne
                                                    INNER JOIN t_pneu ON t_pneu.id_pneu = t_avoir_pneu.fk_pneu
                                                    WHERE id_personne = %(value_id_personne_selected)s)"""

        strsql_persPneu_attribues = """SELECT id_personne, id_pneu, Taille_pneu FROM t_avoir_pneu
                                            INNER JOIN t_personne ON t_personne.id_personne = t_avoir_pneu.fk_personne
                                            INNER JOIN t_pneu ON t_pneu.id_pneu = t_avoir_pneu.fk_pneu
                                            WHERE id_personne = %(value_id_personne_selected)s"""

        # Du fait de l'utilisation des "context managers" on accède au curseur grâce au "with".
        with MaBaseDeDonnee().connexion_bd.cursor() as mc_afficher:
            # Envoi de la commande MySql
            mc_afficher.execute(strsql_persPneu_non_attribues, valeur_id_personne_selected_dict)
            # Récupère les données de la requête.
            data_persPneu_non_attribues = mc_afficher.fetchall()
            # Affichage dans la console
            print("persPneu_afficher_data ----> data_persPneu_non_attribues ", data_persPneu_non_attribues,
                  " Type : ",
                  type(data_persPneu_non_attribues))

            # Envoi de la commande MySql
            mc_afficher.execute(strsql_personne_selected, valeur_id_personne_selected_dict)
            # Récupère les données de la requête.
            data_personne_selected = mc_afficher.fetchall()
            # Affichage dans la console
            print("data_personne_selected  ", data_personne_selected, " Type : ", type(data_personne_selected))

            # Envoi de la commande MySql
            mc_afficher.execute(strsql_persPneu_attribues, valeur_id_personne_selected_dict)
            # Récupère les données de la requête.
            data_persPneu_attribues = mc_afficher.fetchall()
            # Affichage dans la console
            print("data_persPneu_attribues ", data_persPneu_attribues, " Type : ",
                  type(data_persPneu_attribues))

            # Retourne les données des "SELECT"
            return data_personne_selected, data_persPneu_non_attribues, data_persPneu_attribues
    except pymysql.Error as pymysql_erreur:
        code, msg = pymysql_erreur.args
        flash(f"{error_codes.get(code, msg)} ", "danger")
        flash(f"pymysql.Error Erreur dans persPneu_afficher_data : {sys.exc_info()[0]} "
              f"{pymysql_erreur.args[0]} , "
              f"{pymysql_erreur}", "danger")
    except Exception as exception_erreur:
        code, msg = exception_erreur.args
        flash(f"{error_codes.get(code, msg)} ", "danger")
        flash(f"Exception Erreur dans persPneu_afficher_data : {sys.exc_info()[0]} "
              f"{exception_erreur.args[0]} , "
              f"{exception_erreur}", "danger")
    except pymysql.err.IntegrityError as IntegrityError_persPneu_afficher_data:
        code, msg = IntegrityError_persPneu_afficher_data.args
        flash(f"{error_codes.get(code, msg)} ", "danger")
        flash(f"pymysql.err.IntegrityError Erreur dans genres_films_afficher_data : {sys.exc_info()[0]} "
              f"{IntegrityError_persPneu_afficher_data.args[0]} , "
              f"{IntegrityError_persPneu_afficher_data}", "danger")


"""
    Auteur : OM 2021.03.22
    Définition d'une "route" /pers_ajouter

    Test : ex : http://127.0.0.1:5005/pers_ajouter

    Paramètres : sans

    But : Ajouter une personne

    Remarque :  Dans le champ "name_genre_html" du formulaire "pers/pers_ajouter.html",
                le contrôle de la saisie s'effectue ici en Python.
                On transforme la saisie en minuscules.
                On ne doit pas accepter des valeurs vides, des valeurs avec des chiffres,
                des valeurs avec des caractères qui ne sont pas des lettres.
                Pour comprendre [A-Za-zÀ-ÖØ-öø-ÿ] il faut se reporter à la table ASCII https://www.ascii-code.com/
                Accepte le trait d'union ou l'apostrophe, et l'espace entre deux mots, mais pas plus d'une occurence.
"""


@obj_mon_application.route("/pers_ajouter", methods=['GET', 'POST'])
def pers_ajouter_wtf():
    form = FormWTFAjouterPersonne()
    if request.method == "POST":
        try:
            try:
                # Renvoie une erreur si la connexion est perdue.
                MaBaseDeDonnee().connexion_bd.ping(False)
            except Exception as erreur:
                flash(f"Dans Gestion pers_pneu ...terrible erreur, il faut connecter une base de donnée", "danger")
                print(f"Exception grave Classe constructeur GestionPersPneu {erreur.args[0]}")
                raise MaBdErreurConnexion(f"{msg_erreurs['ErreurConnexionBD']['message']} {erreur.args[0]}")

            if form.validate_on_submit():
                name_genre_wtf = form.nom_genre_wtf.data
                prenom_genre_wtf = form.prenom_genre_wtf.data
                date_genre_wtf = form.date_genre_wtf.data
                tel_pers_wtf = form.tel_pers_wtf.data
                mail_pers_wtf = form.mail_pers_wtf.data

                name_genre = name_genre_wtf.capitalize()
                prenom_genre = prenom_genre_wtf.capitalize()
                date_genre = date_genre_wtf
                tel_pers = tel_pers_wtf
                mail_pers = mail_pers_wtf

                valeurs_insertion_dictionnaire = {"value_nom_personne": name_genre,
                                                  "value_prenom_personne": prenom_genre,
                                                  "value_naissance_personne": date_genre,
                                                  "value_num_tel": tel_pers,
                                                  "value_mail_pers": mail_pers}
                print("valeurs_insertion_dictionnaire ", valeurs_insertion_dictionnaire)

                strsql_insert_genre = """INSERT INTO t_personne (id_personne,nom_personne,prenom_personne,naissance_personne,num_tel,mail_pers) VALUES (NULL,%(value_nom_personne)s,%(value_prenom_personne)s,%(value_naissance_personne)s,%(value_num_tel)s,%(value_mail_pers)s)"""
                with MaBaseDeDonnee() as mconn_bd:
                    mconn_bd.mabd_execute(strsql_insert_genre, valeurs_insertion_dictionnaire)

                flash(f"Données insérées !!", "success")
                print(f"Données insérées !!")

                # Pour afficher et constater l'insertion de la valeur, on affiche en ordre inverse. (DESC)
                return redirect(url_for('persPneu_afficher', order_by='DESC', id_personne_sel=0))

        # ATTENTION à l'ordre des excepts, il est très important de respecter l'ordre.
        except pymysql.err.IntegrityError as erreur_genre_doublon:
            # Dérive "pymysql.err.IntegrityError" dans "MaBdErreurDoublon" fichier "erreurs/exceptions.py"
            # Ainsi on peut avoir un message d'erreur personnalisé.
            code, msg = erreur_genre_doublon.args

            flash(f"{error_codes.get(code, msg)} ", "warning")

        # OM 2020.04.16 ATTENTION à l'ordre des excepts, il est très important de respecter l'ordre.
        except (pymysql.err.OperationalError,
                pymysql.ProgrammingError,
                pymysql.InternalError,
                TypeError) as erreur_gest_genr_crud:
            code, msg = erreur_gest_genr_crud.args

            flash(f"{error_codes.get(code, msg)} ", "danger")
            flash(f"Erreur dans Gestion genres CRUD : {sys.exc_info()[0]} "
                  f"{erreur_gest_genr_crud.args[0]} , "
                  f"{erreur_gest_genr_crud}", "danger")

    return render_template("pers/pers_ajouter_wtf.html", form=form)


"""
    Auteur : OM 2021.03.29
    Définition d'une "route" /pers_update

    Test : ex cliquer sur le menu "genres" puis cliquer sur le bouton "EDIT" d'un "genre"

    Paramètres : sans

    But : Editer(update) un genre qui a été sélectionné dans le formulaire "genres_afficher.html"

    Remarque :  Dans le champ "nom_genre_update_wtf" du formulaire "genres/genre_update_wtf.html",
                le contrôle de la saisie s'effectue ici en Python.
                On transforme la saisie en minuscules.
                On ne doit pas accepter des valeurs vides, des valeurs avec des chiffres,
                des valeurs avec des caractères qui ne sont pas des lettres.
                Pour comprendre [A-Za-zÀ-ÖØ-öø-ÿ] il faut se reporter à la table ASCII https://www.ascii-code.com/
                Accepte le trait d'union ou l'apostrophe, et l'espace entre deux mots, mais pas plus d'une occurence.
"""


@obj_mon_application.route("/pers_update", methods=['GET', 'POST'])
def pers_update_wtf():
    # L'utilisateur vient de cliquer sur le bouton "EDIT". Récupère la valeur de "id_genre"
    id_personne_update = request.values['id_personne_btn_edit_html']

    # Objet formulaire pour l'UPDATE
    form_update = FormWTFUpdatePersonne()
    try:
        print(" on submit ", form_update.validate_on_submit())
        if form_update.validate_on_submit():
            # Récupèrer la valeur du champ depuis "genre_update_wtf.html" après avoir cliqué sur "SUBMIT".
            # Puis la convertir en lettres minuscules.

            nom_genre_update_wtf = form_update.nom_genre_update_wtf.data
            prenom_genre_update_wtf = form_update.prenom_genre_update_wtf.data
            date_genre_update_wtf = form_update.date_genre_update_wtf.data
            tel_pers_update_wtf = form_update.tel_pers_update_wtf.data
            mail_pers_update_wtf = form_update.mail_pers_update_wtf.data

            nom_genre_update = nom_genre_update_wtf.capitalize()
            prenom_genre_update = prenom_genre_update_wtf.capitalize()
            date_genre_update = date_genre_update_wtf
            tel_pers_update = tel_pers_update_wtf
            mail_pers_update = mail_pers_update_wtf

            valeur_update_dictionnaire = {"value_id_personne": id_personne_update,
                                          "value_nom_personne": nom_genre_update,
                                          "value_prenom_personne": prenom_genre_update,
                                          "value_naissance_personne": date_genre_update,
                                          "value_num_tel": tel_pers_update,
                                          "value_mail_pers": mail_pers_update}

            print("valeur_update_dictionnaire ", valeur_update_dictionnaire)

            str_sql_update_intitulegenre = """UPDATE t_personne SET nom_personne = %(value_nom_personne)s, prenom_personne = %(value_prenom_personne)s, naissance_personne = %(value_naissance_personne)s, num_tel = %(value_num_tel)s, mail_pers = %(value_mail_pers)s  WHERE id_personne = %(value_id_personne)s"""
            with MaBaseDeDonnee() as mconn_bd:
                mconn_bd.mabd_execute(str_sql_update_intitulegenre, valeur_update_dictionnaire)

            flash(f"Donnée mise à jour !!", "success")
            print(f"Donnée mise à jour !!")

            # afficher et constater que la donnée est mise à jour.
            # Affiche seulement la valeur modifiée, "ASC" et l'"id_genre_update"
            return redirect(url_for('persPneu_afficher', order_by="ASC", id_personne_sel=id_personne_update))
        elif request.method == "GET":
            # Opération sur la BD pour récupérer "id_genre" et "intitule_genre" de la "t_genre"
            str_sql_id_genre = "SELECT id_personne, nom_personne, prenom_personne, naissance_personne, num_tel, mail_pers FROM t_personne WHERE id_personne = %(value_id_personne)s"
            valeur_select_dictionnaire = {"value_id_personne": id_personne_update}
            mybd_curseur = MaBaseDeDonnee().connexion_bd.cursor()
            mybd_curseur.execute(str_sql_id_genre, valeur_select_dictionnaire)
            # Une seule valeur est suffisante "fetchone()", vu qu'il n'y a qu'un seul champ "nom genre" pour l'UPDATE
            data_nom_genre = mybd_curseur.fetchone()
            print("data_nom_genre ", data_nom_genre, " type ", type(data_nom_genre), " genre ",
                  data_nom_genre["nom_personne"])

            # Afficher la valeur sélectionnée dans le champ du formulaire "genre_update_wtf.html"
            form_update.nom_genre_update_wtf.data = data_nom_genre["nom_personne"]
            form_update.prenom_genre_update_wtf.data = data_nom_genre["prenom_personne"]
            form_update.date_genre_update_wtf.data = data_nom_genre["naissance_personne"]
            form_update.tel_pers_update_wtf.data = data_nom_genre["num_tel"]
            form_update.mail_pers_update_wtf.data = data_nom_genre["mail_pers"]

    # OM 2020.04.16 ATTENTION à l'ordre des excepts, il est très important de respecter l'ordre.
    except KeyError:
        flash(f"__KeyError dans pers_update_wtf : {sys.exc_info()[0]} {sys.exc_info()[1]} {sys.exc_info()[2]}",
              "danger")
    except ValueError:
        flash(f"Erreur dans pers_update_wtf : {sys.exc_info()[0]} {sys.exc_info()[1]}", "danger")
    except (pymysql.err.OperationalError,
            pymysql.ProgrammingError,
            pymysql.InternalError,
            pymysql.err.IntegrityError,
            TypeError) as erreur_gest_genr_crud:
        code, msg = erreur_gest_genr_crud.args
        flash(f"attention : {error_codes.get(code, msg)} {erreur_gest_genr_crud} ", "danger")
        flash(f"Erreur dans pers_update_wtf : {sys.exc_info()[0]} "
              f"{erreur_gest_genr_crud.args[0]} , "
              f"{erreur_gest_genr_crud}", "danger")
        flash(f"__KeyError dans pers_update_wtf : {sys.exc_info()[0]} {sys.exc_info()[1]} {sys.exc_info()[2]}",
              "danger")

    return render_template("pers/pers_update_wtf.html", form_update=form_update)


"""
    Auteur : OM 2021.04.08
    Définition d'une "route" /pers_delete

    Test : ex. cliquer sur le menu "genres" puis cliquer sur le bouton "DELETE" d'un "genre"

    Paramètres : sans

    But : Effacer(delete) un genre qui a été sélectionné dans le formulaire "persPneu_afficher.html"

    Remarque :  Dans le champ "nom_genre_delete_wtf" du formulaire "pers/pers_delete_wtf.html",
                le contrôle de la saisie est désactivée. On doit simplement cliquer sur "DELETE"
"""


@obj_mon_application.route("/pers_delete", methods=['GET', 'POST'])
def pers_delete_wtf():
    data_films_attribue_genre_delete = None
    btn_submit_del = None
    # L'utilisateur vient de cliquer sur le bouton "DELETE". Récupère la valeur de "id_genre"
    id_personne_delete = request.values['id_personne_btn_delete_html']

    # Objet formulaire pour effacer le genre sélectionné.
    form_delete = FormWTFDeletePersonne()
    try:
        print(" on submit ", form_delete.validate_on_submit())
        if request.method == "POST" and form_delete.validate_on_submit():

            if form_delete.submit_btn_annuler.data:
                return redirect(url_for("persPneu_afficher", order_by="ASC", id_personne_sel=0))

            if form_delete.submit_btn_conf_del.data:
                # Récupère les données afin d'afficher à nouveau
                # le formulaire "genres/genre_delete_wtf.html" lorsque le bouton "Etes-vous sur d'effacer ?" est cliqué.
                data_films_attribue_genre_delete = session['data_films_attribue_genre_delete']
                print("data_films_attribue_genre_delete ", data_films_attribue_genre_delete)

                flash(f"Effacer la personne de façon définitive de la BD !!!", "danger")
                # L'utilisateur vient de cliquer sur le bouton de confirmation pour effacer...
                # On affiche le bouton "Effacer genre" qui va irrémédiablement EFFACER le genre
                btn_submit_del = True

            if form_delete.submit_btn_del.data:
                valeur_delete_dictionnaire = {"value_id_personne": id_personne_delete}
                print("valeur_delete_dictionnaire ", valeur_delete_dictionnaire)

                str_sql_delete_pers_pneu = """DELETE FROM t_avoir_pneu WHERE fk_personne = %(value_id_personne)s"""
                str_sql_delete_pers_carte = """DELETE FROM t_avoir_carte_grise WHERE fk_personne = %(value_id_personne)s"""
                str_sql_delete_pers_adresse = """DELETE FROM t_avoir_adresse WHERE fk_personne = %(value_id_personne)s"""
                str_sql_delete_pers_facture = """DELETE FROM t_avoir_facture WHERE fk_personne = %(value_id_personne)s"""
                str_sql_delete_idgenre = """DELETE FROM t_personne WHERE id_personne = %(value_id_personne)s"""
                # Manière brutale d'effacer d'abord la "fk_genre", même si elle n'existe pas dans la "t_genre_film"
                # Ensuite on peut effacer le genre vu qu'il n'est plus "lié" (INNODB) dans la "t_genre_film"
                with MaBaseDeDonnee() as mconn_bd:
                    mconn_bd.mabd_execute(str_sql_delete_pers_pneu, valeur_delete_dictionnaire)
                    mconn_bd.mabd_execute(str_sql_delete_pers_carte, valeur_delete_dictionnaire)
                    mconn_bd.mabd_execute(str_sql_delete_pers_adresse, valeur_delete_dictionnaire)
                    mconn_bd.mabd_execute(str_sql_delete_pers_facture, valeur_delete_dictionnaire)
                    mconn_bd.mabd_execute(str_sql_delete_idgenre, valeur_delete_dictionnaire)

                flash(f"Personne définitivement effacé !!", "success")
                print(f"Personne définitivement effacé !!")

                # afficher les données
                return redirect(url_for('persPneu_afficher', order_by="ASC", id_personne_sel=0))

        if request.method == "GET":
            valeur_select_dictionnaire = {"value_id_personne": id_personne_delete}
            print(id_personne_delete, type(id_personne_delete))

            # Requête qui affiche tous les films_genres qui ont le genre que l'utilisateur veut effacer
            str_sql_genres_films_delete = """SELECT id_avoir_pneu, Taille_pneu, id_personne, nom_personne FROM t_avoir_pneu 
                                            INNER JOIN t_personne ON t_avoir_pneu.fk_personne = t_personne.id_personne
                                            INNER JOIN t_pneu ON t_avoir_pneu.fk_pneu = t_pneu.id_pneu
                                            WHERE fk_personne = %(value_id_personne)s"""

            mybd_curseur = MaBaseDeDonnee().connexion_bd.cursor()

            mybd_curseur.execute(str_sql_genres_films_delete, valeur_select_dictionnaire)
            data_films_attribue_genre_delete = mybd_curseur.fetchall()
            print("data_films_attribue_genre_delete...", data_films_attribue_genre_delete)

            # Nécessaire pour mémoriser les données afin d'afficher à nouveau
            # le formulaire "genres/genre_delete_wtf.html" lorsque le bouton "Etes-vous sur d'effacer ?" est cliqué.
            session['data_films_attribue_genre_delete'] = data_films_attribue_genre_delete

            # Opération sur la BD pour récupérer "id_personne" et "nom_personne" de la "t_personne"
            str_sql_id_genre = "SELECT id_personne, nom_personne FROM t_personne WHERE id_personne = %(value_id_personne)s"

            mybd_curseur.execute(str_sql_id_genre, valeur_select_dictionnaire)
            # Une seule valeur est suffisante "fetchone()",
            # vu qu'il n'y a qu'un seul champ "nom genre" pour l'action DELETE
            data_nom_genre = mybd_curseur.fetchone()
            print("data_nom_genre ", data_nom_genre, " type ", type(data_nom_genre), " pneu ",
                  data_nom_genre["nom_personne"])

            # Afficher la valeur sélectionnée dans le champ du formulaire "genre_delete_wtf.html"
            form_delete.nom_genre_delete_wtf.data = data_nom_genre["nom_personne"]

            # Le bouton pour l'action "DELETE" dans le form. "genre_delete_wtf.html" est caché.
            btn_submit_del = False

    # OM 2020.04.16 ATTENTION à l'ordre des excepts, il est très important de respecter l'ordre.
    except KeyError:
        flash(f"__KeyError dans pers_delete_wtf : {sys.exc_info()[0]} {sys.exc_info()[1]} {sys.exc_info()[2]}",
              "danger")
    except ValueError:
        flash(f"Erreur dans pers_delete_wtf : {sys.exc_info()[0]} {sys.exc_info()[1]}", "danger")
    except (pymysql.err.OperationalError,
            pymysql.ProgrammingError,
            pymysql.InternalError,
            pymysql.err.IntegrityError,
            TypeError) as erreur_gest_genr_crud:
        code, msg = erreur_gest_genr_crud.args
        flash(f"attention : {error_codes.get(code, msg)} {erreur_gest_genr_crud} ", "danger")

        flash(f"Erreur dans pers_delete_wtf : {sys.exc_info()[0]} "
              f"{erreur_gest_genr_crud.args[0]} , "
              f"{erreur_gest_genr_crud}", "danger")

        flash(f"__KeyError dans genre_delete_wtf : {sys.exc_info()[0]} {sys.exc_info()[1]} {sys.exc_info()[2]}",
              "danger")

    return render_template("pers/pers_delete_wtf.html",
                           form_delete=form_delete,
                           btn_submit_del=btn_submit_del,
                           data_personne_associes=data_films_attribue_genre_delete)

