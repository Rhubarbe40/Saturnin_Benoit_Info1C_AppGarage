"""
    Fichier : gestion_genres_wtf_forms.py
    Auteur : OM 2021.03.22
    Gestion des formulaires avec WTF
"""
from flask_wtf import FlaskForm
from wtforms import StringField
from wtforms import DateField
from wtforms import SubmitField
from wtforms.validators import Length
from wtforms.validators import Regexp


class FormWTFAjouterFacture(FlaskForm):
    """
        Dans le formulaire "genres_ajouter_wtf.html" on impose que le champ soit rempli.
        Définition d'un "bouton" submit avec un libellé personnalisé.
    """
    num_facture_regexp = "^[a-zA-Z0-9_.-]*$"
    num_facture_wtf = StringField("Clavioter le N° de la facture ", validators=[Length(min=2, max=40, message="min 2 max 40"),
                                                                   Regexp(num_facture_regexp,
                                                                          message="Pas de chiffres, de caractères "
                                                                                  "spéciaux, "
                                                                                  "d'espace à double, de double "
                                                                                  "apostrophe, de double trait union")
                                                                   ])
    cout_facture_regexp = "^[a-zA-Z0-9_.-]*$"
    cout_facture_wtf = StringField("Clavioter le montant de la facture ", validators=[Length(min=2, max=40, message="min 2 max 40"),
                                                                   Regexp(cout_facture_regexp,
                                                                          message="")
                                                                   ])

    submit = SubmitField("Enregistrer la facture")


class FormWTFUpdateFacture(FlaskForm):
    """
        Dans le formulaire "genre_update_wtf.html" on impose que le champ soit rempli.
        Définition d'un "bouton" submit avec un libellé personnalisé.
    """
    num_facture_update_regexp = "^[a-zA-Z0-9_.-]*$"
    num_facture_update_wtf = StringField("Clavioter le N° de la facture ", validators=[Length(min=2, max=20, message="min 2 max 20"),
                                                                    Regexp(num_facture_update_regexp,
                                                                          message="Pas de chiffres, de "
                                                                                         "caractères "
                                                                                         "spéciaux, "
                                                                                         "d'espace à double, de double "
                                                                                         "apostrophe, de double trait "
                                                                                         "union")
                                                                          ])
    cout_facture_update_regexp = "^[a-zA-Z0-9_.-]*$"
    cout_facture_update_wtf = StringField("Clavioter le montant de la facture ", validators=[Length(min=2, max=40, message="min 2 max 20"),
                                                                   Regexp(cout_facture_update_regexp,
                                                                          message="Pas de chiffres, de caractères "
                                                                                  "spéciaux, "
                                                                                  "d'espace à double, de double "
                                                                                  "apostrophe, de double trait union")
                                                                   ])

    submit = SubmitField("Mettre à jour la facture")


class FormWTFDeleteFacture(FlaskForm):
    """
        Dans le formulaire "genre_delete_wtf.html"

        nom_genre_delete_wtf : Champ qui reçoit la valeur du genre, lecture seule. (readonly=true)
        submit_btn_del : Bouton d'effacement "DEFINITIF".
        submit_btn_conf_del : Bouton de confirmation pour effacer un "genre".
        submit_btn_annuler : Bouton qui permet d'afficher la table "t_genre".
    """
    num_facture_delete_wtf = StringField("Effacer cette facture")
    submit_btn_del = SubmitField("CONFIRMER")
    submit_btn_conf_del = SubmitField("EFFACER ?")
    submit_btn_annuler = SubmitField("Annuler")
