"""
    Fichier : gestion_films_genres_crud.py
    Auteur : OM 2021.05.01
    Gestions des "routes" FLASK et des données pour l'association entre les films et les genres.
"""
import sys

import pymysql
from flask import flash
from flask import redirect
from flask import render_template
from flask import request
from flask import session
from flask import url_for

from APP_Garage import obj_mon_application
from APP_Garage.database.connect_db_context_manager import MaBaseDeDonnee
from APP_Garage.erreurs.exceptions import *
from APP_Garage.erreurs.msg_erreurs import *
from APP_Garage.facturePers.gestion_facture_wtf_forms import FormWTFAjouterFacture
from APP_Garage.facturePers.gestion_facture_wtf_forms import FormWTFDeleteFacture
from APP_Garage.facturePers.gestion_facture_wtf_forms import FormWTFUpdateFacture

"""
    Nom : facturePers_afficher
    Auteur : OM 2021.05.01
    Définition d'une "route" /facturePers_afficher
    
    But : Afficher les personnes avec les personnes associés pour chaque facture.
    
    Paramètres : id_facture_sel = 0 >> toutes les facture.
                 id_facture_sel = "n" affiche la facture dont l'id est "n"
                 
"""


@obj_mon_application.route("/facturePers_afficher/<int:id_facture_sel>", methods=['GET', 'POST'])
def facturePers_afficher(id_facture_sel):
    data_facture_rappel={}
    data_facture_payer={}
    if request.method == "GET":
        try:
            try:
                # Renvoie une erreur si la connexion est perdue.
                MaBaseDeDonnee().connexion_bd.ping(False)
            except Exception as Exception_init_facturePers_afficher:
                code, msg = Exception_init_facturePers_afficher.args
                flash(f"{error_codes.get(code, msg)} ", "danger")
                flash(f"Exception _init_facturePers_afficher problème de connexion BD : {sys.exc_info()[0]} "
                      f"{Exception_init_facturePers_afficher.args[0]} , "
                      f"{Exception_init_facturePers_afficher}", "danger")
                raise MaBdErreurConnexion(f"{msg_erreurs['ErreurConnexionBD']['message']} {erreur.args[0]}")

            with MaBaseDeDonnee().connexion_bd.cursor() as mc_afficher:
                strsql_facturePers_afficher_data = """SELECT id_facture, num_facture, cout_facture, date_facture,
                                                            GROUP_CONCAT(nom_personne," ", prenom_personne) as facturePers FROM t_avoir_facture
                                                            RIGHT JOIN t_facture ON t_facture.id_facture = t_avoir_facture.fk_facture
                                                            LEFT JOIN t_personne ON t_personne.id_personne = t_avoir_facture.fk_personne
                                                            """
                if id_facture_sel == 0:

                    strsql_facturePers_afficher_data += """LEFT JOIN t_rappeler_facture ON t_facture.id_facture = t_rappeler_facture.fk_facture 
                                                            LEFT JOIN t_payer_facture ON t_facture.id_facture = t_payer_facture.fk_facture 
                                                            WHERE t_payer_facture.fk_facture IS NULL AND t_rappeler_facture.fk_facture IS NULL
                                                            GROUP BY id_facture"""

                    strsql_facturePers_rappel_data = """SELECT id_facture, num_facture, cout_facture, date_rappel,
                                                                                GROUP_CONCAT(nom_personne," ", prenom_personne) as facturePers FROM t_avoir_facture
                                                                                RIGHT JOIN t_facture ON t_facture.id_facture = t_avoir_facture.fk_facture
                                                                                LEFT JOIN t_personne ON t_personne.id_personne = t_avoir_facture.fk_personne
                                                                                INNER JOIN t_rappeler_facture ON t_facture.id_facture = t_rappeler_facture.fk_facture
                                                                                GROUP BY id_facture"""
                    strsql_facturePers_payer_data = """SELECT id_facture, num_facture, cout_facture, date_paiement,
                                                                                                    GROUP_CONCAT(nom_personne," ", prenom_personne) as facturePers FROM t_avoir_facture
                                                                                                    RIGHT JOIN t_facture ON t_facture.id_facture = t_avoir_facture.fk_facture
                                                                                                    LEFT JOIN t_personne ON t_personne.id_personne = t_avoir_facture.fk_personne
                                                                                                    INNER JOIN t_payer_facture ON t_facture.id_facture = t_payer_facture.fk_facture
                                                                                                    GROUP BY id_facture"""
                    # le paramètre 0 permet d'afficher tous les films
                    # Sinon le paramètre représente la valeur de l'id du film
                    mc_afficher.execute(strsql_facturePers_rappel_data)
                    data_facture_rappel = mc_afficher.fetchall()

                    mc_afficher.execute(strsql_facturePers_payer_data)
                    data_facture_payer = mc_afficher.fetchall()
                    mc_afficher.execute(strsql_facturePers_afficher_data)


                else:
                    # Constitution d'un dictionnaire pour associer l'id de la personne sélectionné avec un nom de variable
                    valeur_id_facture_selected_dictionnaire = {"value_id_facture_selected": id_facture_sel}
                    # En MySql l'instruction HAVING fonctionne comme un WHERE... mais doit être associée à un GROUP BY
                    # L'opérateur += permet de concaténer une nouvelle valeur à la valeur de gauche préalablement définie.
                    strsql_facturePers_afficher_data += """GROUP BY id_facture HAVING id_facture= %(value_id_facture_selected)s"""

                    mc_afficher.execute(strsql_facturePers_afficher_data, valeur_id_facture_selected_dictionnaire)

                # Récupère les données de la requête.
                data_facturePers_afficher = mc_afficher.fetchall()



                print("data_pers ", data_facturePers_afficher, " Type : ", type(data_facturePers_afficher))

                # Différencier les messages.
                if not data_facturePers_afficher and id_facture_sel == 0:
                    flash("""Aucunes Factures en attente. Elles se situent probablement dans les rappels !""", "warning")
                elif not data_facturePers_afficher and id_facture_sel > 0:
                    # Si l'utilisateur change l'id_carte_grise dans l'URL et qu'il ne correspond à aucune personne
                    flash(f"La facture ID {id_facture_sel} demandé n'existe pas !!", "warning")
                else:
                    flash(f"Données factures et personnes affichés !!", "success")

        except Exception as Exception_facturePers_afficher:
            code, msg = Exception_facturePers_afficher.args
            flash(f"{error_codes.get(code, msg)} ", "danger")
            flash(f"Exception facturePers_afficher : {sys.exc_info()[0]} "
                  f"{Exception_facturePers_afficher.args[0]} , "
                  f"{Exception_facturePers_afficher}", "danger")

    # Envoie la page "HTML" au serveur.
    return render_template("facturePers/facturePers_afficher.html", data=data_facturePers_afficher, rappel=data_facture_rappel, payer=data_facture_payer, number=id_facture_sel)


"""
    nom: edit_facturePers_selected
    On obtient un objet "objet_dumpbd"

    Récupère la liste de toutes les personnes de la factures sélectionnée par le bouton "MODIFIER" de "facturePers_afficher.html"
    
    Dans une liste déroulante particulière (tags-selector-tagselect), on voit :
    1) Tous les personnes contenus dans la "t_personnes".
    2) Les personnes attribués à la facture selectionné.
    3) Les personnes non-attribués à la facture sélectionné.

    On signale les erreurs importantes

"""


@obj_mon_application.route("/edit_facturePers_selected", methods=['GET', 'POST'])
def edit_facturePers_selected():
    if request.method == "GET":
        try:
            with MaBaseDeDonnee().connexion_bd.cursor() as mc_afficher:
                strsql_pers_afficher = """SELECT id_personne, nom_personne FROM t_personne ORDER BY id_personne ASC"""
                mc_afficher.execute(strsql_pers_afficher)
            data_pers_all = mc_afficher.fetchall()
            print("dans edit_facturePers_selected ---> data_pers_all", data_pers_all)

            # Récupère la valeur de "id_carte_grise" du formulaire html "cartePers_afficher.html"
            # l'utilisateur clique sur le bouton "Modifier" et on récupère la valeur de "id_carte_grise"
            # grâce à la variable "id_cartePers_edit_html" dans le fichier "cartePers_afficher.html"
            # href="{{ url_for('edit_cartePers_selected', id_cartePers_edit_html=row.id_carte_grise) }}"
            id_facturePers_edit = request.values['id_facturePers_edit_html']

            # Mémorise l'id de la carte grise dans une variable de session
            # (ici la sécurité de l'application n'est pas engagée)
            # il faut éviter de stocker des données sensibles dans des variables de sessions.
            session['session_id_facturePers_edit'] = id_facturePers_edit

            # Constitution d'un dictionnaire pour associer l'id du film sélectionné avec un nom de variable
            valeur_id_facture_selected_dictionnaire = {"value_id_facture_selected": id_facturePers_edit}

            # Récupère les données grâce à 3 requêtes MySql définie dans la fonction cartePers_afficher_data
            # 1) Sélection de la personne choisi
            # 2) Sélection des personnes "déjà" attribués pour la carte.
            # 3) Sélection des personnes "pas encore" attribués pour la carte choisi.
            # ATTENTION à l'ordre d'assignation des variables retournées par la fonction "facturePers_afficher_data"
            data_facturePers_selected, data_facturePers_non_attribues, data_facturePers_attribues = \
                facturePers_afficher_data(valeur_id_facture_selected_dictionnaire)

            print(data_facturePers_selected)
            lst_data_facture_selected = [item['id_facture'] for item in data_facturePers_selected]
            print("lst_data_facture_selected  ", lst_data_facture_selected,
                  type(lst_data_facture_selected))

            # Dans le composant "tags-selector-tagselect" on doit connaître
            # les genres qui ne sont pas encore sélectionnés.
            lst_data_facturePers_non_attribues = [item['id_personne'] for item in data_facturePers_non_attribues]
            session['session_lst_data_facturePers_non_attribues'] = lst_data_facturePers_non_attribues
            print("lst_data_facturePers_non_attribues  ", lst_data_facturePers_non_attribues,
                  type(lst_data_facturePers_non_attribues))

            # Dans le composant "tags-selector-tagselect" on doit connaître
            # les genres qui sont déjà sélectionnés.
            lst_data_facturePers_old_attribues = [item['id_personne'] for item in data_facturePers_attribues]
            session['session_lst_data_facturePers_old_attribues'] = lst_data_facturePers_old_attribues
            print("lst_data_facturePers_old_attribues  ", lst_data_facturePers_old_attribues,
                  type(lst_data_facturePers_old_attribues))

            print(" data data_facturePers_selected", data_facturePers_selected, "type ", type(data_facturePers_selected))
            print(" data data_facturePers_non_attribues ", data_facturePers_non_attribues, "type ",
                  type(data_facturePers_non_attribues))
            print(" data_facturePers_attribues ", data_facturePers_attribues, "type ",
                  type(data_facturePers_attribues))

            # Extrait les valeurs contenues dans la table "t_personne", colonne "nom_personne_"
            # Le composant javascript "tagify" pour afficher les tags n'a pas besoin de l'id_personne
            lst_data_facturePers_non_attribues = [item['nom_personne'] for item in data_facturePers_non_attribues]
            print("lst_all_pers gf_edit_facturePers_selected ", lst_data_facturePers_non_attribues,
                  type(lst_data_facturePers_non_attribues))

        except Exception as Exception_edit_facturePers_selected:
            code, msg = Exception_edit_facturePers_selected.args
            flash(f"{error_codes.get(code, msg)} ", "danger")
            flash(f"Exception edit_facturePers_selected : {sys.exc_info()[0]} "
                  f"{Exception_edit_facturePers_selected.args[0]} , "
                  f"{Exception_edit_facturePers_selected}", "danger")

    return render_template("facturePers/facturePers_modifier_tags_dropbox.html",
                           #data_genres=data_pers_all,
                           data_facture_selected=data_facturePers_selected,
                           data_pers_attribues=data_facturePers_attribues,
                           data_pers_non_attribues=data_facturePers_non_attribues)


"""
    nom: update_cartePers_selected

    Récupère la liste de toutes les personnes de la carte grise sélectionnée par le bouton "MODIFIER" de "cartePers_afficher.html"
    
    Dans une liste déroulante particulière (tags-selector-tagselect), on voit :
    1) Tous les peronnes contenus dans la "t_personne".
    2) Les personnes attribués à la carte grise selectionné.
    3) Les cartePers non-attribués à la carte grise sélectionné.

    On signale les erreurs importantes
"""


@obj_mon_application.route("/update_facturePers_selected", methods=['GET', 'POST'])
def update_facturePers_selected():
    if request.method == "POST":
        try:
            # Récupère l'id de l'facture' sélectionnée
            id_facture_selected = session['session_id_facturePers_edit']
            print("session['session_id_facturePers_edit'] ", session['session_id_facturePers_edit'])

            # Récupère la liste des personnes qui ne sont pas associés à l'facture sélectionnée.
            old_lst_data_facturePers_non_attribues = session['session_lst_data_facturePers_non_attribues']
            print("old_lst_data_facturePers_non_attribues ", old_lst_data_facturePers_non_attribues)

            # Récupère la liste des personnes qui sont associés à la carte grise sélectionnée.
            old_lst_data_facturePers_attribues = session['session_lst_data_facturePers_old_attribues']
            print("old_lst_data_facturePers_old_attribues ", old_lst_data_facturePers_attribues)

            # Effacer toutes les variables de session.
            session.clear()

            # Récupère ce que l'utilisateur veut modifier comme personne dans le composant "tags-selector-tagselect"
            # dans le fichier "cartePers_modifier_tags_dropbox.html"
            new_lst_str_facturePers = request.form.getlist('name_select_tags')
            print("new_lst_str_facturePers ", new_lst_str_facturePers)

            # OM 2021.05.02 Exemple : Dans "name_select_tags" il y a ['4','65','2']
            # On transforme en une liste de valeurs numériques. [4,65,2]
            new_lst_int_facturePers_old = list(map(int, new_lst_str_facturePers))
            print("new_lst_facturePers ", new_lst_int_facturePers_old, "type new_lst_facturePers ",
                  type(new_lst_int_facturePers_old))

            # Pour apprécier la facilité de la vie en Python... "les ensembles en Python"
            # https://fr.wikibooks.org/wiki/Programmation_Python/Ensembles
            # OM 2021.05.02 Une liste de "id_personne" qui doivent être effacés de la table intermédiaire "t_avoir_carte_grise".
            lst_diff_pers_delete_b = list(
                set(old_lst_data_facturePers_attribues) - set(new_lst_int_facturePers_old))
            print("lst_diff_pers_delete_b ", lst_diff_pers_delete_b)

            # Une liste de "id_personne" qui doivent être ajoutés à la "t_avoir_carte_grise"
            lst_diff_pers_insert_a = list(
                set(new_lst_int_facturePers_old) - set(old_lst_data_facturePers_attribues))
            print("lst_diff_pers_insert_a ", lst_diff_pers_insert_a)

            # SQL pour insérer une nouvelle association entre
            # "fk_carte_grise"/"id_carte_grise" et "fk_personne"/"id_personne" dans la "t_avoir_carte_grise"
            strsql_insert_facturePers = """INSERT INTO t_avoir_facture (id_avoir_facture, fk_personne, fk_facture)
                                                    VALUES (NULL, %(value_fk_personne)s, %(value_fk_facture)s)"""

            # SQL pour effacer une (des) association(s) existantes entre "id_carte_grise" et "id_personne" dans la "t_avoir_carte_grise"
            strsql_delete_facturePers = """DELETE FROM t_avoir_facture WHERE fk_personne = %(value_fk_personne)s AND fk_facture = %(value_fk_facture)s"""

            with MaBaseDeDonnee() as mconn_bd:
                # Pour la carte grise sélectionné, parcourir la liste des personnes à INSÉRER dans la "t_avoir_carte_grise".
                # Si la liste est vide, la boucle n'est pas parcourue.
                for id_pers_ins in lst_diff_pers_insert_a:
                    # Constitution d'un dictionnaire pour associer l'id de la carte grise sélectionné avec un nom de variable
                    # et "id_pers_ins" (l'id de la personne dans la liste) associé à une variable.
                    valeurs_facture_sel_pers_sel_dictionnaire = {"value_fk_facture": id_facture_selected,
                                                                 "value_fk_personne": id_pers_ins}

                    mconn_bd.mabd_execute(strsql_insert_facturePers, valeurs_facture_sel_pers_sel_dictionnaire)

                # Pour la carte grise sélectionné, parcourir la liste des personnes à EFFACER dans la "t_avoir_carte_grise".
                # Si la liste est vide, la boucle n'est pas parcourue.
                for id_pers_del in lst_diff_pers_delete_b:
                    # Constitution d'un dictionnaire pour associer l'id de la personne sélectionné avec un nom de variable
                    # et "id_pers_del" (l'id du personne dans la liste) associé à une variable.
                    valeurs_facture_sel_pers_sel_dictionnaire = {"value_fk_facture": id_facture_selected,
                                                                 "value_fk_personne": id_pers_del}

                    # Du fait de l'utilisation des "context managers" on accède au curseur grâce au "with".
                    # la subtilité consiste à avoir une méthode "mabd_execute" dans la classe "MaBaseDeDonnee"
                    # ainsi quand elle aura terminé l'insertion des données le destructeur de la classe "MaBaseDeDonnee"
                    # sera interprété, ainsi on fera automatiquement un commit
                    mconn_bd.mabd_execute(strsql_delete_facturePers, valeurs_facture_sel_pers_sel_dictionnaire)

        except Exception as Exception_update_facturePers_selected:
            code, msg = Exception_update_facturePers_selected.args
            flash(f"{error_codes.get(code, msg)} ", "danger")
            flash(f"Exception update_cartePers_selected : {sys.exc_info()[0]} "
                  f"{Exception_update_facturePers_selected.args[0]} , "
                  f"{Exception_update_facturePers_selected}", "danger")

    # Après cette mise à jour de la table intermédiaire "t_avoir_carte_grise",
    # on affiche les cartes grises et le(urs) personne(s) associé(s).
    return redirect(url_for('facturePers_afficher', id_facture_sel=id_facture_selected))


"""
    nom: facturePers_afficher_data

    Récupère la liste de toutes les personnes de la facture sélectionné par le bouton "MODIFIER" de "facturePers_afficher.html"
    Nécessaire pour afficher tous les "TAGS" des personnes, ainsi l'utilisateur voit les personnes à disposition

    On signale les erreurs importantes
"""


def facturePers_afficher_data(valeur_id_facture_selected_dict):
    print("valeur_id_facture_selected_dict...", valeur_id_facture_selected_dict)
    try:

        strsql_facture_selected = """SELECT id_facture, num_facture, cout_facture, GROUP_CONCAT(id_personne) as facturePers FROM t_avoir_facture
                                        INNER JOIN t_facture ON t_facture.id_facture = t_avoir_facture.fk_facture
                                        INNER JOIN t_personne ON t_personne.id_personne = t_avoir_facture.fk_personne
                                        WHERE id_facture = %(value_id_facture_selected)s"""

        strsql_facturePers_non_attribues = """SELECT id_personne, nom_personne FROM t_personne WHERE id_personne not in(SELECT id_personne as idfacturePers FROM t_avoir_facture
                                                    INNER JOIN t_facture ON t_facture.id_facture = t_avoir_facture.fk_facture
                                                    INNER JOIN t_personne ON t_personne.id_personne = t_avoir_facture.fk_personne
                                                    WHERE id_facture = %(value_id_facture_selected)s)"""

        strsql_facturePers_attribues = """SELECT id_facture, id_personne, nom_personne FROM t_avoir_facture
                                            INNER JOIN t_facture ON t_facture.id_facture = t_avoir_facture.fk_facture
                                            INNER JOIN t_personne ON t_personne.id_personne = t_avoir_facture.fk_personne
                                            WHERE id_facture = %(value_id_facture_selected)s"""

        # Du fait de l'utilisation des "context managers" on accède au curseur grâce au "with".
        with MaBaseDeDonnee().connexion_bd.cursor() as mc_afficher:
            # Envoi de la commande MySql
            mc_afficher.execute(strsql_facturePers_non_attribues, valeur_id_facture_selected_dict)
            # Récupère les données de la requête.
            data_facturePers_non_attribues = mc_afficher.fetchall()
            # Affichage dans la console
            print("facturePers_afficher_data ----> data_facturePers_non_attribues ", data_facturePers_non_attribues,
                  " Type : ",
                  type(data_facturePers_non_attribues))

            # Envoi de la commande MySql
            mc_afficher.execute(strsql_facture_selected, valeur_id_facture_selected_dict)
            # Récupère les données de la requête.
            data_facture_selected = mc_afficher.fetchall()
            # Affichage dans la console
            print("data_facture_selected  ", data_facture_selected, " Type : ", type(data_facture_selected))

            # Envoi de la commande MySql
            mc_afficher.execute(strsql_facturePers_attribues, valeur_id_facture_selected_dict)
            # Récupère les données de la requête.
            data_facturePers_attribues = mc_afficher.fetchall()
            # Affichage dans la console
            print("data_facturePers_attribues ", data_facturePers_attribues, " Type : ",
                  type(data_facturePers_attribues))

            # Retourne les données des "SELECT"
            return data_facture_selected, data_facturePers_non_attribues, data_facturePers_attribues
    except pymysql.Error as pymysql_erreur:
        code, msg = pymysql_erreur.args
        flash(f"{error_codes.get(code, msg)} ", "danger")
        flash(f"pymysql.Error Erreur dans facturePers_afficher_data : {sys.exc_info()[0]} "
              f"{pymysql_erreur.args[0]} , "
              f"{pymysql_erreur}", "danger")
    except Exception as exception_erreur:
        code, msg = exception_erreur.args
        flash(f"{error_codes.get(code, msg)} ", "danger")
        flash(f"Exception Erreur dans facturePers_afficher_data : {sys.exc_info()[0]} "
              f"{exception_erreur.args[0]} , "
              f"{exception_erreur}", "danger")
    except pymysql.err.IntegrityError as IntegrityError_facturePers_afficher_data:
        code, msg = IntegrityError_facturePers_afficher_data.args
        flash(f"{error_codes.get(code, msg)} ", "danger")
        flash(f"pymysql.err.IntegrityError Erreur dans facturePers_afficher_data : {sys.exc_info()[0]} "
              f"{IntegrityError_facturePers_afficher_data.args[0]} , "
              f"{IntegrityError_facturePers_afficher_data}", "danger")


"""
    Auteur : OM 2021.03.22
    Définition d'une "route" /facture_ajouter

    Test : ex : http://127.0.0.1:5005/pers_ajouter

    Paramètres : sans

    But : Ajouter une facture

    Remarque :  Dans le champ "name_genre_html" du formulaire "facture/facture_ajouter.html",
                le contrôle de la saisie s'effectue ici en Python.
                On transforme la saisie en minuscules.
                On ne doit pas accepter des valeurs vides, des valeurs avec des chiffres,
                des valeurs avec des caractères qui ne sont pas des lettres.
                Pour comprendre [A-Za-zÀ-ÖØ-öø-ÿ] il faut se reporter à la table ASCII https://www.ascii-code.com/
                Accepte le trait d'union ou l'apostrophe, et l'espace entre deux mots, mais pas plus d'une occurence.
"""


@obj_mon_application.route("/facture_ajouter", methods=['GET', 'POST'])
def facture_ajouter_wtf():
    form = FormWTFAjouterFacture()
    if request.method == "POST":
        try:
            try:
                # Renvoie une erreur si la connexion est perdue.
                MaBaseDeDonnee().connexion_bd.ping(False)
            except Exception as erreur:
                flash(f"Dans Gestion facture_pers ...terrible erreur, il faut connecter une base de donnée", "danger")
                print(f"Exception grave Classe constructeur Gestion facture_pers {erreur.args[0]}")
                raise MaBdErreurConnexion(f"{msg_erreurs['ErreurConnexionBD']['message']} {erreur.args[0]}")

            if form.validate_on_submit():
                num_facture_wtf = form.num_facture_wtf.data
                cout_facture_wtf = form.cout_facture_wtf.data

                num_facture = num_facture_wtf.capitalize()
                cout_facture = cout_facture_wtf.capitalize()

                valeurs_insertion_dictionnaire = {"value_num_facture": num_facture,
                                                  "value_cout_facture": cout_facture}

                print("valeurs_insertion_dictionnaire ", valeurs_insertion_dictionnaire)

                strsql_insert_carte = """INSERT INTO t_facture (id_facture,num_facture,cout_facture) VALUES (NULL,%(value_num_facture)s,%(value_cout_facture)s)"""
                with MaBaseDeDonnee() as mconn_bd:
                    mconn_bd.mabd_execute(strsql_insert_carte, valeurs_insertion_dictionnaire)

                flash(f"La factures a été ajoutée avec succès !!", "success")
                flash(f"N'oubliez pas de l'associer avec une personne pour valider la date d'établisement de la facture !!", "warning")
                print(f"Données insérées !!")

                # Pour afficher et constater l'insertion de la valeur, on affiche en ordre inverse. (DESC)
                return redirect(url_for('facturePers_afficher', order_by='DESC', id_facture_sel=0))

        # ATTENTION à l'ordre des excepts, il est très important de respecter l'ordre.
        except pymysql.err.IntegrityError as erreur_carte_doublon:
            # Dérive "pymysql.err.IntegrityError" dans "MaBdErreurDoublon" fichier "erreurs/exceptions.py"
            # Ainsi on peut avoir un message d'erreur personnalisé.
            code, msg = erreur_carte_doublon.args

            flash(f"{error_codes.get(code, msg)} ", "warning")

        # OM 2020.04.16 ATTENTION à l'ordre des excepts, il est très important de respecter l'ordre.
        except (pymysql.err.OperationalError,
                pymysql.ProgrammingError,
                pymysql.InternalError,
                TypeError) as erreur_gest_genr_crud:
            code, msg = erreur_gest_genr_crud.args

            flash(f"{error_codes.get(code, msg)} ", "danger")
            flash(f"Erreur dans Gestion genres CRUD : {sys.exc_info()[0]} "
                  f"{erreur_gest_genr_crud.args[0]} , "
                  f"{erreur_gest_genr_crud}", "danger")

    return render_template("facture/facture_ajouter_wtf.html", form=form)


"""
    Auteur : OM 2021.03.29
    Définition d'une "route" /carte_update

    Test : ex cliquer sur le menu "carte / Personne" puis cliquer sur le bouton "EDIT" d'une "carte grise"

    Paramètres : sans

    But : Editer(update) une carte grise qui a été sélectionné dans le formulaire "cartePers_afficher.html"

    Remarque :  Dans le champ "nom_genre_update_wtf" du formulaire "genres/genre_update_wtf.html",
                le contrôle de la saisie s'effectue ici en Python.
                On transforme la saisie en minuscules.
                On ne doit pas accepter des valeurs vides, des valeurs avec des chiffres,
                des valeurs avec des caractères qui ne sont pas des lettres.
                Pour comprendre [A-Za-zÀ-ÖØ-öø-ÿ] il faut se reporter à la table ASCII https://www.ascii-code.com/
                Accepte le trait d'union ou l'apostrophe, et l'espace entre deux mots, mais pas plus d'une occurence.
"""


@obj_mon_application.route("/facture_update", methods=['GET', 'POST'])
def facture_update_wtf():
    # L'utilisateur vient de cliquer sur le bouton "EDIT". Récupère la valeur de "id_genre"
    id_facture_update = request.values['id_facture_btn_edit_html']

    # Objet formulaire pour l'UPDATE
    form_update = FormWTFUpdateFacture()
    try:
        print(" on submit ", form_update.validate_on_submit())
        if form_update.validate_on_submit():
            # Récupèrer la valeur du champ depuis "genre_update_wtf.html" après avoir cliqué sur "SUBMIT".
            # Puis la convertir en lettres minuscules.

            num_facture_update_wtf = form_update.num_facture_update_wtf.data
            cout_facture_update_wtf = form_update.cout_facture_update_wtf.data

            num_facture_update = num_facture_update_wtf.capitalize()
            cout_facture_update = cout_facture_update_wtf

            valeur_update_dictionnaire = {"value_id_facture": id_facture_update,
                                          "value_num_facture": num_facture_update,
                                          "value_cout_facture": cout_facture_update}

            print("valeur_update_dictionnaire ", valeur_update_dictionnaire)

            str_sql_update_intitulegenre = """UPDATE t_facture SET num_facture = %(value_num_facture)s, cout_facture = %(value_cout_facture)s  WHERE id_facture = %(value_id_facture)s"""
            with MaBaseDeDonnee() as mconn_bd:
                mconn_bd.mabd_execute(str_sql_update_intitulegenre, valeur_update_dictionnaire)

            flash(f"Donnée mise à jour !!", "success")
            print(f"Donnée mise à jour !!")

            # afficher et constater que la donnée est mise à jour.
            # Affiche seulement la valeur modifiée, "ASC" et l'"id_genre_update"
            return redirect(url_for('facturePers_afficher', order_by="ASC", id_facture_sel=id_facture_update))
        elif request.method == "GET":
            # Opération sur la BD pour récupérer "id_genre" et "intitule_genre" de la "t_genre"
            str_sql_id_genre = "SELECT id_facture, num_facture, cout_facture FROM t_facture WHERE id_facture = %(value_id_facture)s"
            valeur_select_dictionnaire = {"value_id_facture": id_facture_update}
            mybd_curseur = MaBaseDeDonnee().connexion_bd.cursor()
            mybd_curseur.execute(str_sql_id_genre, valeur_select_dictionnaire)
            # Une seule valeur est suffisante "fetchone()", vu qu'il n'y a qu'un seul champ "nom genre" pour l'UPDATE
            data_nom_genre = mybd_curseur.fetchone()
            print("data_nom_genre ", data_nom_genre, " type ", type(data_nom_genre), " genre ",
                  data_nom_genre["num_facture"])

            # Afficher la valeur sélectionnée dans le champ du formulaire "genre_update_wtf.html"
            form_update.num_facture_update_wtf.data = data_nom_genre["num_facture"]
            form_update.cout_facture_update_wtf.data = data_nom_genre["cout_facture"]

    # OM 2020.04.16 ATTENTION à l'ordre des excepts, il est très important de respecter l'ordre.
    except KeyError:
        flash(f"__KeyError dans facture_update_wtf : {sys.exc_info()[0]} {sys.exc_info()[1]} {sys.exc_info()[2]}",
              "danger")
    except ValueError:
        flash(f"Erreur dans facture_update_wtf : {sys.exc_info()[0]} {sys.exc_info()[1]}", "danger")
    except (pymysql.err.OperationalError,
            pymysql.ProgrammingError,
            pymysql.InternalError,
            pymysql.err.IntegrityError,
            TypeError) as erreur_gest_genr_crud:
        code, msg = erreur_gest_genr_crud.args
        flash(f"attention : {error_codes.get(code, msg)} {erreur_gest_genr_crud} ", "danger")
        flash(f"Erreur dans facture_update_wtf : {sys.exc_info()[0]} "
              f"{erreur_gest_genr_crud.args[0]} , "
              f"{erreur_gest_genr_crud}", "danger")
        flash(f"__KeyError dans facture_update_wtf : {sys.exc_info()[0]} {sys.exc_info()[1]} {sys.exc_info()[2]}",
              "danger")

    return render_template("facture/facture_update_wtf.html", form_update=form_update)

@obj_mon_application.route("/facture_rappel", methods=['GET', 'POST'])
def facture_rappel():
    id_facture_rappel = request.values['id_facture_rappel_btn_edit_html']

    valeur_rappel_dictionnaire = {"id_facture_rappel": id_facture_rappel}

    with MaBaseDeDonnee() as mconn_bd:
        str_sql_facture_rappel="""INSERT INTO `t_rappeler_facture`(`id_rappeler_facture`, `fk_facture`) VALUES (NULL,%(id_facture_rappel)s)"""
        mconn_bd.mabd_execute(str_sql_facture_rappel, valeur_rappel_dictionnaire)

    return redirect(url_for('facturePers_afficher', order_by="ASC", id_facture_sel=0))

@obj_mon_application.route("/facture_payer", methods=['GET', 'POST'])
def facture_payer():
    id_facture_payer = request.values['id_facture_payer_btn_edit_html']

    valeur_payer_dictionnaire = {"id_facture_payer": id_facture_payer}

    with MaBaseDeDonnee() as mconn_bd:
        str_sql_facture_payer="""INSERT INTO `t_payer_facture`(`id_acquiter_facture`, `fk_facture`) VALUES (NULL,%(id_facture_payer)s)"""
        mconn_bd.mabd_execute(str_sql_facture_payer, valeur_payer_dictionnaire)

        str_sql_facture_rappel_delete="""DELETE FROM `t_rappeler_facture` WHERE fk_facture = %(id_facture_payer)s"""
        mconn_bd.mabd_execute(str_sql_facture_rappel_delete, valeur_payer_dictionnaire)

    return redirect(url_for('facturePers_afficher', order_by="ASC", id_facture_sel=0))


"""
    Auteur : OM 2021.04.08
    Définition d'une "route" /facture_delete

    Test : ex. cliquer sur le menu "genres" puis cliquer sur le bouton "DELETE" d'un "genre"

    Paramètres : sans

    But : Effacer(delete) un genre qui a été sélectionné dans le formulaire "persPneu_afficher.html"

    Remarque :  Dans le champ "nom_genre_delete_wtf" du formulaire "pers/pers_delete_wtf.html",
                le contrôle de la saisie est désactivée. On doit simplement cliquer sur "DELETE"
"""


@obj_mon_application.route("/facture_delete", methods=['GET', 'POST'])
def facture_delete_wtf():
    data_films_attribue_genre_delete = None
    btn_submit_del = None
    # L'utilisateur vient de cliquer sur le bouton "DELETE". Récupère la valeur de "id_genre"
    id_facture_delete = request.values['id_facture_btn_delete_html']

    # Objet formulaire pour effacer le genre sélectionné.
    form_delete = FormWTFDeleteFacture()
    try:
        print(" on submit ", form_delete.validate_on_submit())
        if request.method == "POST" and form_delete.validate_on_submit():

            if form_delete.submit_btn_annuler.data:
                return redirect(url_for("facturePers_afficher", order_by="ASC", id_facture_sel=0))

            if form_delete.submit_btn_conf_del.data:
                # Récupère les données afin d'afficher à nouveau
                # le formulaire "genres/genre_delete_wtf.html" lorsque le bouton "Etes-vous sur d'effacer ?" est cliqué.
                data_films_attribue_genre_delete = session['data_films_attribue_genre_delete']
                print("data_films_attribue_genre_delete ", data_films_attribue_genre_delete)

                flash(f"Effacer la Facture de façon définitive de votre base de donnée ???", "danger")
                # L'utilisateur vient de cliquer sur le bouton de confirmation pour effacer...
                # On affiche le bouton "Effacer genre" qui va irrémédiablement EFFACER le genre
                btn_submit_del = True

            if form_delete.submit_btn_del.data:
                valeur_delete_dictionnaire = {"value_id_facture": id_facture_delete}
                print("valeur_delete_dictionnaire ", valeur_delete_dictionnaire)

                str_sql_delete_facture_pers = """DELETE FROM t_avoir_facture WHERE fk_facture = %(value_id_facture)s"""
                str_sql_delete_rappel_facture = """DELETE FROM t_rappeler_facture WHERE fk_facture = %(value_id_facture)s"""
                str_sql_delete_payer_facture = """DELETE FROM t_payer_facture WHERE fk_facture = %(value_id_facture)s"""
                str_sql_delete_attente_facture = """DELETE FROM t_facture_attente WHERE fk_id_facture = %(value_id_facture)s"""
                str_sql_delete_id_facture = """DELETE FROM t_facture WHERE id_facture = %(value_id_facture)s"""
                # Manière brutale d'effacer d'abord la "fk_genre", même si elle n'existe pas dans la "t_genre_film"
                # Ensuite on peut effacer le genre vu qu'il n'est plus "lié" (INNODB) dans la "t_genre_film"
                with MaBaseDeDonnee() as mconn_bd:
                    mconn_bd.mabd_execute(str_sql_delete_facture_pers, valeur_delete_dictionnaire)
                    mconn_bd.mabd_execute(str_sql_delete_rappel_facture, valeur_delete_dictionnaire)
                    mconn_bd.mabd_execute(str_sql_delete_payer_facture, valeur_delete_dictionnaire)
                    mconn_bd.mabd_execute(str_sql_delete_attente_facture, valeur_delete_dictionnaire)
                    mconn_bd.mabd_execute(str_sql_delete_id_facture, valeur_delete_dictionnaire)

                flash(f"Facture définitivement effacé !!", "success")
                print(f"Facture définitivement effacé !!")

                # afficher les données
                return redirect(url_for('facturePers_afficher', order_by="ASC", id_facture_sel=0))

        if request.method == "GET":
            valeur_select_dictionnaire = {"value_id_facture": id_facture_delete}
            print(id_facture_delete, type(id_facture_delete))

            # Requête qui affiche tous les films_genres qui ont le genre que l'utilisateur veut effacer
            str_sql_genres_films_delete = """SELECT id_avoir_facture, nom_personne, prenom_personne, naissance_personne, id_facture, num_facture FROM t_avoir_facture 
                                            INNER JOIN t_facture ON t_avoir_facture.fk_facture = t_facture.id_facture
                                            INNER JOIN t_personne ON t_avoir_facture.fk_personne = t_personne.id_personne
                                            WHERE fk_facture = %(value_id_facture)s"""

            mybd_curseur = MaBaseDeDonnee().connexion_bd.cursor()

            mybd_curseur.execute(str_sql_genres_films_delete, valeur_select_dictionnaire)
            data_films_attribue_genre_delete = mybd_curseur.fetchall()
            print("data_films_attribue_genre_delete...", data_films_attribue_genre_delete)

            # Nécessaire pour mémoriser les données afin d'afficher à nouveau
            # le formulaire "genres/genre_delete_wtf.html" lorsque le bouton "Etes-vous sur d'effacer ?" est cliqué.
            session['data_films_attribue_genre_delete'] = data_films_attribue_genre_delete

            # Opération sur la BD pour récupérer "id_personne" et "nom_personne" de la "t_personne"
            str_sql_id_genre = "SELECT id_facture, num_facture FROM t_facture WHERE id_facture = %(value_id_facture)s"

            mybd_curseur.execute(str_sql_id_genre, valeur_select_dictionnaire)
            # Une seule valeur est suffisante "fetchone()",
            # vu qu'il n'y a qu'un seul champ "nom genre" pour l'action DELETE
            data_marque = mybd_curseur.fetchone()
            print("data_nom_genre ", data_marque, " type ", type(data_marque), " facture ",
                  data_marque["num_facture"])

            # Afficher la valeur sélectionnée dans le champ du formulaire "genre_delete_wtf.html"
            form_delete.num_facture_delete_wtf.data = data_marque["num_facture"]

            # Le bouton pour l'action "DELETE" dans le form. "genre_delete_wtf.html" est caché.
            btn_submit_del = False

    # OM 2020.04.16 ATTENTION à l'ordre des excepts, il est très important de respecter l'ordre.
    except KeyError:
        flash(f"__KeyError dans facture_delete_wtf : {sys.exc_info()[0]} {sys.exc_info()[1]} {sys.exc_info()[2]}",
              "danger")
    except ValueError:
        flash(f"Erreur dans facture_delete_wtf : {sys.exc_info()[0]} {sys.exc_info()[1]}", "danger")
    except (pymysql.err.OperationalError,
            pymysql.ProgrammingError,
            pymysql.InternalError,
            pymysql.err.IntegrityError,
            TypeError) as erreur_gest_genr_crud:
        code, msg = erreur_gest_genr_crud.args
        flash(f"attention : {error_codes.get(code, msg)} {erreur_gest_genr_crud} ", "danger")

        flash(f"Erreur dans pers_delete_wtf : {sys.exc_info()[0]} "
              f"{erreur_gest_genr_crud.args[0]} , "
              f"{erreur_gest_genr_crud}", "danger")

        flash(f"__KeyError dans Facture_delete_wtf : {sys.exc_info()[0]} {sys.exc_info()[1]} {sys.exc_info()[2]}",
              "danger")

    return render_template("facture/facture_delete_wtf.html",
                           form_delete=form_delete,
                           btn_submit_del=btn_submit_del,
                           data_personne_associes=data_films_attribue_genre_delete)

