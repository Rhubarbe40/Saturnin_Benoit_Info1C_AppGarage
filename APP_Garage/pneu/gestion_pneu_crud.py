"""
    Fichier : gestion_pneu_crud.py
    Auteur : OM 2021.03.16
    Gestions des "routes" FLASK et des données pour les pneus.
"""
import sys

import pymysql
from flask import flash
from flask import redirect
from flask import render_template
from flask import request
from flask import session
from flask import url_for

from APP_Garage import obj_mon_application
from APP_Garage.database.connect_db_context_manager import MaBaseDeDonnee
from APP_Garage.erreurs.exceptions import *
from APP_Garage.erreurs.msg_erreurs import *
from APP_Garage.pneu.gestion_pneu_wtf_forms import FormWTFAjouterPneu
from APP_Garage.pneu.gestion_pneu_wtf_forms import FormWTFDeletePneu
from APP_Garage.pneu.gestion_pneu_wtf_forms import FormWTFUpdatePneu

"""
    Auteur : OM 2021.03.16
    Définition d'une "route" /pneu_afficher
    
    Test : ex : http://127.0.0.1:5005/pneu_afficher
    
    Paramètres : order_by : ASC : Ascendant, DESC : Descendant
                id_pneu_sel = 0 >> tous les pneus.
                id_pneu_sel = "n" affiche les pneu dont l'id est "n"
"""


@obj_mon_application.route("/pneu_afficher/<string:order_by>/<int:id_pneu_sel>", methods=['GET', 'POST'])
def pneu_afficher(order_by, id_pneu_sel):
    if request.method == "GET":
        try:
            try:
                # Renvoie une erreur si la connexion est perdue.
                MaBaseDeDonnee().connexion_bd.ping(False)
            except Exception as erreur:
                flash(f"Dans Gestion pneus ...terrible erreur, il faut connecter une base de donnée", "danger")
                print(f"Exception grave Classe constructeur GestionPneus {erreur.args[0]}")
                raise MaBdErreurConnexion(f"{msg_erreurs['ErreurConnexionBD']['message']} {erreur.args[0]}")

            with MaBaseDeDonnee().connexion_bd.cursor() as mc_afficher:
                if order_by == "ASC" and id_pneu_sel == 0:
                    strsql_pneu_afficher = """SELECT id_pneu, Taille_pneu FROM t_pneu ORDER BY id_pneu ASC"""
                    mc_afficher.execute(strsql_pneu_afficher)
                elif order_by == "ASC":
                    # C'EST LA QUE VOUS ALLEZ DEVOIR PLACER VOTRE PROPRE LOGIQUE MySql
                    # la commande MySql classique est "SELECT * FROM t_genre"
                    # Pour "lever"(raise) une erreur s'il y a des erreurs sur les noms d'attributs dans la table
                    # donc, je précise les champs à afficher
                    # Constitution d'un dictionnaire pour associer l'id des pneus sélectionnés avec un nom de variable
                    valeur_id_pneu_selected_dictionnaire = {"value_id_pneu_selected": id_pneu_sel}
                    strsql_pneu_afficher = """SELECT id_pneu, Taille_pneu FROM t_pneu  WHERE id_pneu = %(value_id_pneu_selected)s"""

                    mc_afficher.execute(strsql_pneu_afficher, valeur_id_pneu_selected_dictionnaire)
                else:
                    strsql_pneu_afficher = """SELECT id_pneu, Taille_pneu FROM t_pneu ORDER BY id_pneu DESC"""

                    mc_afficher.execute(strsql_pneu_afficher)

                data_pneu = mc_afficher.fetchall()

                print("data_pneu ", data_pneu, " Type : ", type(data_pneu))

                # Différencier les messages si la table est vide.
                if not data_pneu and id_pneu_sel == 0:
                    flash("""La table "t_pneu" est vide. !!""", "warning")
                elif not data_pneu and id_pneu_sel > 0:
                    # Si l'utilisateur change l'id_pneu dans l'URL et que les pneus n'existent pas,
                    flash(f"Les pneus demandé n'existent pas !!", "warning")
                else:
                    # Dans tous les autres cas, c'est que la table "t_pneu" est vide.
                    # OM 2020.04.09 La ligne ci-dessous permet de donner un sentiment rassurant aux utilisateurs.
                    flash(f"Pneus affichés !!", "success")

        except Exception as erreur:
            print(f"RGG Erreur générale. pneu_afficher")
            # OM 2020.04.09 On dérive "Exception" par le "@obj_mon_application.errorhandler(404)"
            # fichier "run_mon_app.py"
            # Ainsi on peut avoir un message d'erreur personnalisé.
            flash(f"RGG Exception {erreur} pneu_afficher", "danger")
            raise Exception(f"RGG Erreur générale. {erreur}")
            # raise MaBdErreurOperation(f"RGG Exception {msg_erreurs['ErreurNomBD']['message']} {erreur}")

    # Envoie la page "HTML" au serveur.
    return render_template("pneu/pneu_afficher.html", data=data_pneu)


"""
    Auteur : OM 2021.03.22
    Définition d'une "route" /pneu_ajouter
    
    Test : ex : http://127.0.0.1:5005/pneu_ajouter
    
    Paramètres : sans
    
    But : Ajouter une taille pour des pneus
    
    Remarque :  Dans le champ "name_pneu_html" du formulaire "pneu/pneu_ajouter.html",
                le contrôle de la saisie s'effectue ici en Python.
                On transforme la saisie en minuscules.
                On ne doit pas accepter des valeurs vides, des valeurs avec des chiffres,
                des valeurs avec des caractères qui ne sont pas des lettres.
                Pour comprendre [A-Za-zÀ-ÖØ-öø-ÿ] il faut se reporter à la table ASCII https://www.ascii-code.com/
                Accepte le trait d'union ou l'apostrophe, et l'espace entre deux mots, mais pas plus d'une occurence.
"""


@obj_mon_application.route("/pneu_ajouter", methods=['GET', 'POST'])
def pneu_ajouter_wtf():
    form = FormWTFAjouterPneu()
    if request.method == "POST":
        try:
            try:
                # Renvoie une erreur si la connexion est perdue.
                MaBaseDeDonnee().connexion_bd.ping(False)
            except Exception as erreur:
                flash(f"Dans Gestion pneu ...terrible erreur, il faut connecter une base de donnée", "danger")
                print(f"Exception grave Classe constructeur GestionPneu {erreur.args[0]}")
                raise MaBdErreurConnexion(f"{msg_erreurs['ErreurConnexionBD']['message']} {erreur.args[0]}")

            if form.validate_on_submit():
                name_pneu_wtf = form.nom_pneu_wtf.data

                name_pneu = name_pneu_wtf
                valeurs_insertion_dictionnaire = {"value_pneu": name_pneu}
                print("valeurs_insertion_dictionnaire ", valeurs_insertion_dictionnaire)

                strsql_insert_pneu = """INSERT INTO t_pneu (id_pneu,Taille_pneu) VALUES (NULL,%(value_pneu)s)"""
                with MaBaseDeDonnee() as mconn_bd:
                    mconn_bd.mabd_execute(strsql_insert_pneu, valeurs_insertion_dictionnaire)

                flash(f"Données insérées !!", "success")
                print(f"Données insérées !!")

                # Pour afficher et constater l'insertion de la valeur, on affiche en ordre inverse. (DESC)
                return redirect(url_for('pneu_afficher', order_by='DESC', id_pneu_sel=0))

        # ATTENTION à l'ordre des excepts, il est très important de respecter l'ordre.
        except pymysql.err.IntegrityError as erreur_pneu_doublon:
            # Dérive "pymysql.err.IntegrityError" dans "MaBdErreurDoublon" fichier "erreurs/exceptions.py"
            # Ainsi on peut avoir un message d'erreur personnalisé.
            code, msg = erreur_pneu_doublon.args

            flash(f"{error_codes.get(code, msg)} ", "warning")

        # OM 2020.04.16 ATTENTION à l'ordre des excepts, il est très important de respecter l'ordre.
        except (pymysql.err.OperationalError,
                pymysql.ProgrammingError,
                pymysql.InternalError,
                TypeError) as erreur_gest_pneu_crud:
            code, msg = erreur_gest_pneu_crud.args

            flash(f"{error_codes.get(code, msg)} ", "danger")
            flash(f"Erreur dans Gestion pneu CRUD : {sys.exc_info()[0]} "
                  f"{erreur_gest_pneu_crud.args[0]} , "
                  f"{erreur_gest_pneu_crud}", "danger")

    return render_template("pneu/pneu_ajouter_wtf.html", form=form)


"""
    Auteur : OM 2021.03.29
    Définition d'une "route" /pneu_update
    
    Test : ex cliquer sur le menu "pneus" puis cliquer sur le bouton "Modifier" de "pneus"
    
    Paramètres : sans
    
    But : modifier(update) les pneus qui ont été sélectionné dans le formulaire "pneu_afficher.html"
    
    Remarque :  Dans le champ "nom_pneu_update_wtf" du formulaire "pneu/pneu_update_wtf.html",
                le contrôle de la saisie s'effectue ici en Python.
                On transforme la saisie en minuscules.
                On ne doit pas accepter des valeurs vides, des valeurs avec des chiffres,
                des valeurs avec des caractères qui ne sont pas des lettres.
                Pour comprendre [A-Za-zÀ-ÖØ-öø-ÿ] il faut se reporter à la table ASCII https://www.ascii-code.com/
                Accepte le trait d'union ou l'apostrophe, et l'espace entre deux mots, mais pas plus d'une occurence.
"""


@obj_mon_application.route("/pneu_update", methods=['GET', 'POST'])
def pneu_update_wtf():

    # L'utilisateur vient de cliquer sur le bouton "Modifier". Récupère la valeur de "id_pneu"
    id_pneu_update = request.values['id_pneu_btn_edit_html']

    # Objet formulaire pour l'UPDATE
    form_update = FormWTFUpdatePneu()
    try:
        print(" on submit ", form_update.validate_on_submit())
        if form_update.validate_on_submit():
            # Récupèrer la valeur du champ depuis "pneu_update_wtf.html" après avoir cliqué sur "SUBMIT".
            # Puis la convertir en lettres minuscules.
            name_pneu_update = form_update.nom_pneu_update_wtf.data#va prendre les données du form_update.nom_pneu_update_wtf du fichier
            name_pneu_update = name_pneu_update#lover() mets tous les charatères en minuscule

            valeur_update_dictionnaire = {"value_id_pneu": id_pneu_update, "value_name_taille": name_pneu_update}#il faut introduire les données dans le dictionnaire "value_id_pneu": id_pneu_update. Elle ensuite reprise dans le UPDATE de la ligne du dessous
            print("valeur_update_dictionnaire ", valeur_update_dictionnaire)

            str_sql_update_Taillepneu = """UPDATE t_pneu SET Taille_pneu = %(value_name_taille)s WHERE id_pneu = %(value_id_pneu)s"""
            with MaBaseDeDonnee() as mconn_bd:
                mconn_bd.mabd_execute(str_sql_update_Taillepneu, valeur_update_dictionnaire)

            flash(f"Donnée mise à jour !!", "success")
            print(f"Donnée mise à jour !!")

            # afficher et constater que la donnée est mise à jour.
            # Affiche seulement la valeur modifiée, "ASC" et l'"id_pneu_update"
            return redirect(url_for('pneu_afficher', order_by="ASC", id_pneu_sel=id_pneu_update))
        elif request.method == "GET":
            # Opération sur la BD pour récupérer "id_pneu" et "Taille_pneu" de la "t_pneu"
            str_sql_id_pneu = "SELECT id_pneu, Taille_pneu FROM t_pneu WHERE id_pneu = %(value_id_pneu)s"
            valeur_select_dictionnaire = {"value_id_pneu": id_pneu_update}
            mybd_curseur = MaBaseDeDonnee().connexion_bd.cursor()
            mybd_curseur.execute(str_sql_id_pneu, valeur_select_dictionnaire)
            # Une seule valeur est suffisante "fetchone()", vu qu'il n'y a qu'un seul champ "nom pneus" pour l'UPDATE
            data_nom_pneu = mybd_curseur.fetchone()
            print("data_nom_pneu ", data_nom_pneu, " type ", type(data_nom_pneu), " pneu ",
                  data_nom_pneu["Taille_pneu"])#doit avoir le nom du champ

            # Afficher la valeur sélectionnée dans le champ du formulaire "pneu_update_wtf.html"
            form_update.nom_pneu_update_wtf.data = data_nom_pneu["Taille_pneu"]#ajouter les autres nom wtf selon

    # OM 2020.04.16 ATTENTION à l'ordre des excepts, il est très important de respecter l'ordre.
    except KeyError:
        flash(f"__KeyError dans pneu_update_wtf : {sys.exc_info()[0]} {sys.exc_info()[1]} {sys.exc_info()[2]}",
              "danger")
    except ValueError:
        flash(f"Erreur dans pneu_update_wtf : {sys.exc_info()[0]} {sys.exc_info()[1]}", "danger")
    except (pymysql.err.OperationalError,
            pymysql.ProgrammingError,
            pymysql.InternalError,
            pymysql.err.IntegrityError,
            TypeError) as erreur_gest_pneu_crud:
        code, msg = erreur_gest_pneu_crud.args
        flash(f"attention : {error_codes.get(code, msg)} {erreur_gest_pneu_crud} ", "danger")
        flash(f"Erreur dans pneu_update_wtf : {sys.exc_info()[0]} "
              f"{erreur_gest_pneu_crud.args[0]} , "
              f"{erreur_gest_pneu_crud}", "danger")
        flash(f"__KeyError dans pneu_update_wtf : {sys.exc_info()[0]} {sys.exc_info()[1]} {sys.exc_info()[2]}",
              "danger")

    return render_template("pneu/pneu_update_wtf.html", form_update=form_update)


"""
    Auteur : OM 2021.04.08
    Définition d'une "route" /pneu_delete
    
    Test : ex. cliquer sur le menu "pneus" puis cliquer sur le bouton "Effacer" de "pneus"
    
    Paramètres : sans
    
    But : Effacer(delete) des pneus qui ont été sélectionné dans le formulaire "pneu_afficher.html"
    
    Remarque :  Dans le champ "nom_pneu_delete_wtf" du formulaire "pneu/pneu_delete_wtf.html",
                le contrôle de la saisie est désactivée. On doit simplement cliquer sur "Effacer"
"""


@obj_mon_application.route("/pneu_delete", methods=['GET', 'POST'])
def pneu_delete_wtf():
    data_pers_pneu_attribue_pneu_delete = None
    btn_submit_del = None
    # L'utilisateur vient de cliquer sur le bouton "EFFACER". Récupère la valeur de "id_pneu"
    id_pneu_delete = request.values['id_pneu_btn_delete_html']

    # Objet formulaire pour effacer les pneus sélectionnés.
    form_delete = FormWTFDeletePneu()
    try:
        print(" on submit ", form_delete.validate_on_submit())
        if request.method == "POST" and form_delete.validate_on_submit():

            if form_delete.submit_btn_annuler.data:
                return redirect(url_for("pneu_afficher", order_by="ASC", id_pneu_sel=0))

            if form_delete.submit_btn_conf_del.data:
                # Récupère les données afin d'afficher à nouveau
                # le formulaire "pneu/pneu_delete_wtf.html" lorsque le bouton "Etes-vous sur d'effacer ?" est cliqué.
                data_pers_pneu_attribue_pneu_delete = session['data_pers_pneu_attribue_pneu_delete']
                print("data_pers_pneu_attribue_pneu_delete ", data_pers_pneu_attribue_pneu_delete)

                flash(f"Effacer les pneus de façon définitive de la BD !!!", "danger")
                # L'utilisateur vient de cliquer sur le bouton de confirmation pour effacer...
                # On affiche le bouton "Effacer de pneu" qui va irrémédiablement EFFACER la taille de pneus
                btn_submit_del = True

            if form_delete.submit_btn_del.data:
                valeur_delete_dictionnaire = {"value_id_pneu": id_pneu_delete}
                print("valeur_delete_dictionnaire ", valeur_delete_dictionnaire)

                str_sql_delete_nomPers_pneu = """DELETE FROM t_avoir_pneu WHERE fk_pneu = %(value_id_pneu)s"""
                str_sql_delete_idpneu = """DELETE FROM t_pneu WHERE id_pneu = %(value_id_pneu)s"""
                # Manière brutale d'effacer d'abord la "fk_pneu", même si elle n'existe pas dans la "t_avoir_pneu"
                # Ensuite on peut effacer les pneus vu qu'il ne sont plus "lié" (INNODB) dans la "t_avoir_pneu"
                with MaBaseDeDonnee() as mconn_bd:
                    mconn_bd.mabd_execute(str_sql_delete_nomPers_pneu, valeur_delete_dictionnaire)
                    mconn_bd.mabd_execute(str_sql_delete_idpneu, valeur_delete_dictionnaire)

                flash(f"Pneus définitivement effacé !!", "success")
                print(f"Pneus définitivement effacé !!")

                # afficher les données
                return redirect(url_for('pneu_afficher', order_by="ASC", id_pneu_sel=0))

        if request.method == "GET":
            valeur_select_dictionnaire = {"value_id_pneu": id_pneu_delete}
            print(id_pneu_delete, type(id_pneu_delete))

            # Requête qui affiche toutes les personnes qui ont les pneus que l'utilisateur veut effacer
            str_sql_avoir_pneu_delete = """SELECT id_avoir_pneu, nom_personne, prenom_personne, id_pneu, Taille_pneu FROM t_avoir_pneu 
                                            INNER JOIN t_personne ON t_avoir_pneu.fk_personne = t_personne.id_personne
                                            INNER JOIN t_pneu ON t_avoir_pneu.fk_pneu = t_pneu.id_pneu
                                            WHERE fk_pneu = %(value_id_pneu)s"""

            mybd_curseur = MaBaseDeDonnee().connexion_bd.cursor()

            mybd_curseur.execute(str_sql_avoir_pneu_delete, valeur_select_dictionnaire)
            data_pers_pneu_attribue_pneu_delete = mybd_curseur.fetchall()
            print("data_pers_pneu_attribue_pneu_delete...", data_pers_pneu_attribue_pneu_delete)

            # Nécessaire pour mémoriser les données afin d'afficher à nouveau
            # le formulaire "pneu/pneu_delete_wtf.html" lorsque le bouton "Etes-vous sur d'effacer ?" est cliqué.
            session['data_pers_pneu_attribue_pneu_delete'] = data_pers_pneu_attribue_pneu_delete

            # Opération sur la BD pour récupérer "id_pneu" et "Taille_pneu" de la "t_pneu"
            str_sql_id_pneu = "SELECT id_pneu, Taille_pneu FROM t_pneu WHERE id_pneu = %(value_id_pneu)s"

            mybd_curseur.execute(str_sql_id_pneu, valeur_select_dictionnaire)
            # Une seule valeur est suffisante "fetchone()",
            # vu qu'il n'y a qu'un seul champ "nom pneu" pour l'action DELETE
            data_nom_pneu = mybd_curseur.fetchone()
            print("data_nom_pneu ", data_nom_pneu, " type ", type(data_nom_pneu), " pneu ",
                  data_nom_pneu["Taille_pneu"])

            # Afficher la valeur sélectionnée dans le champ du formulaire "pneu_delete_wtf.html"
            form_delete.nom_pneu_delete_wtf.data = data_nom_pneu["Taille_pneu"]#ajouter les autres action selon

            # Le bouton pour l'action "DELETE" dans le form. "pneu_delete_wtf.html" est caché.
            btn_submit_del = False

    # OM 2020.04.16 ATTENTION à l'ordre des excepts, il est très important de respecter l'ordre.
    except KeyError:
        flash(f"__KeyError dans pneu_delete_wtf : {sys.exc_info()[0]} {sys.exc_info()[1]} {sys.exc_info()[2]}",
              "danger")
    except ValueError:
        flash(f"Erreur dans pneu_delete_wtf : {sys.exc_info()[0]} {sys.exc_info()[1]}", "danger")
    except (pymysql.err.OperationalError,
            pymysql.ProgrammingError,
            pymysql.InternalError,
            pymysql.err.IntegrityError,
            TypeError) as erreur_gest_pneu_crud:
        code, msg = erreur_gest_pneu_crud.args
        flash(f"attention : {error_codes.get(code, msg)} {erreur_gest_pneu_crud} ", "danger")

        flash(f"Erreur dans pneu_delete_wtf : {sys.exc_info()[0]} "
              f"{erreur_gest_pneu_crud.args[0]} , "
              f"{erreur_gest_pneu_crud}", "danger")

        flash(f"__KeyError dans pneu_delete_wtf : {sys.exc_info()[0]} {sys.exc_info()[1]} {sys.exc_info()[2]}",
              "danger")

    return render_template("pneu/pneu_delete_wtf.html",
                           form_delete=form_delete,
                           btn_submit_del=btn_submit_del,
                           data_pers_pneu_associes=data_pers_pneu_attribue_pneu_delete)
