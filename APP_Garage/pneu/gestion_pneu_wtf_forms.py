"""
    Fichier : gestion_marquePneu_wtf_forms.py
    Auteur : OM 2021.03.22
    Gestion des formulaires avec WTF
"""
from flask_wtf import FlaskForm
from wtforms import StringField
from wtforms import SubmitField
from wtforms.validators import Length
from wtforms.validators import Regexp


class FormWTFAjouterPneu(FlaskForm):
    """
        Dans le formulaire "pneu_ajouter_wtf.html" on impose que le champ soit rempli.
        Définition d'un "bouton" submit avec un libellé personnalisé.
    """
    nom_pneu_regexp = "^(\d{1,3}(?:\.\d)?)(?:[/X](\d{1,2}(?:\.\d)?)?)?([RBD])(\d{1,2}\d)(\ )(\d{1,2}\d)(\S)?$"
    nom_pneu_wtf = StringField("Clavioter la taille ", validators=[Length(min=2, max=20, message="min 2 max 20"),
                                                                   Regexp(nom_pneu_regexp,
                                                                          message="Avec ce format : 205/65R17 91H")
                                                                   ])#pour des champs suppl., copier le code depuis nom_pneu_regex jusqu'à la parenthèse
    submit = SubmitField("Enregistrer taille")


class FormWTFUpdatePneu(FlaskForm):
    """
        Dans le formulaire "marquePneu_update_wtf.html" on impose que le champ soit rempli.
        Définition d'un "bouton" submit avec un libellé personnalisé.
    """
    nom_pneu_update_regexp = "^(\d{1,3}(?:\.\d)?)(?:[/X](\d{1,2}(?:\.\d)?)?)?([RBD])(\d{1,2}\d)(\ )(\d{1,2}\d)(\S)?$"
    nom_pneu_update_wtf = StringField("Modifier ces pneus ", validators=[Length(min=2, max=20, message="min 2 max 20"),
                                                                          Regexp(nom_pneu_update_regexp,
                                                                                 message="Pas de chiffres, de "
                                                                                         "caractères "
                                                                                         "spéciaux, "
                                                                                         "d'espace à double, de double "
                                                                                         "apostrophe, de double trait "
                                                                                         "union")
                                                                          ])
    submit = SubmitField("Mettre à jour")


class FormWTFDeletePneu(FlaskForm):
    """
        Dans le formulaire "pneu_delete_wtf.html"

        nom_pneus_delete_wtf : Champ qui reçoit la valeur du genre, lecture seule. (readonly=true)
        submit_btn_del : Bouton d'effacement "DEFINITIF".
        submit_btn_conf_del : Bouton de confirmation pour effacer un "pneu".
        submit_btn_annuler : Bouton qui permet d'afficher la table "t_pneu".
    """
    nom_pneu_delete_wtf = StringField("Effacer ces pneus")
    submit_btn_del = SubmitField("Confirmer")
    submit_btn_conf_del = SubmitField("Supprimer")
    submit_btn_annuler = SubmitField("Annuler")
