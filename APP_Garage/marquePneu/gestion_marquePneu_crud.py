"""
    Fichier : gestion_genres_crud.py
    Auteur : OM 2021.03.16
    Gestions des "routes" FLASK et des données pour les genres.
"""
import sys

import pymysql
from flask import flash
from flask import redirect
from flask import render_template
from flask import request
from flask import session
from flask import url_for

from APP_Garage import obj_mon_application
from APP_Garage.database.connect_db_context_manager import MaBaseDeDonnee
from APP_Garage.erreurs.exceptions import *
from APP_Garage.erreurs.msg_erreurs import *
from APP_Garage.marquePneu.gestion_marquePneu_wtf_forms import FormWTFAjouterMarquePneu
from APP_Garage.marquePneu.gestion_marquePneu_wtf_forms import FormWTFDeleteMarquePneu
from APP_Garage.marquePneu.gestion_marquePneu_wtf_forms import FormWTFUpdateMarquePneu

"""
    Auteur : OM 2021.03.16
    Définition d'une "route" /marquePneu_afficher
    
    Test : ex : http://127.0.0.1:5005/marquePneu_afficher
    
    Paramètres : order_by : ASC : Ascendant, DESC : Descendant
                id_marque_pneu_sel = 0 >> toutes les marques.
                id_marque_pneu_sel = "n" affiche la marque de pneu dont l'id est "n"
"""


@obj_mon_application.route("/marquePneu_afficher/<string:order_by>/<int:id_marque_pneu_sel>", methods=['GET', 'POST'])
def marquePneu_afficher(order_by, id_marque_pneu_sel):
    if request.method == "GET":
        try:
            try:
                # Renvoie une erreur si la connexion est perdue.
                MaBaseDeDonnee().connexion_bd.ping(False)
            except Exception as erreur:
                flash(f"Dans Gestion genres ...terrible erreur, il faut connecter une base de donnée", "danger")
                print(f"Exception grave Classe constructeur GestionGenres {erreur.args[0]}")
                raise MaBdErreurConnexion(f"{msg_erreurs['ErreurConnexionBD']['message']} {erreur.args[0]}")

            with MaBaseDeDonnee().connexion_bd.cursor() as mc_afficher:
                if order_by == "ASC" and id_marque_pneu_sel == 0:
                    strsql_marquesPneu_afficher = """SELECT id_marque_pneu, marque_pneu FROM t_marque_pneu ORDER BY id_marque_pneu ASC"""
                    mc_afficher.execute(strsql_marquesPneu_afficher)
                elif order_by == "ASC":
                    # C'EST LA QUE VOUS ALLEZ DEVOIR PLACER VOTRE PROPRE LOGIQUE MySql
                    # la commande MySql classique est "SELECT * FROM t_genre"
                    # Pour "lever"(raise) une erreur s'il y a des erreurs sur les noms d'attributs dans la table
                    # donc, je précise les champs à afficher
                    # Constitution d'un dictionnaire pour associer l'id de la marque de pneus sélectionné avec un nom de variable
                    valeur_id_marque_pneu_selected_dictionnaire = {"value_id_marque_pneu_selected": id_marque_pneu_sel}
                    strsql_marquesPneu_afficher = """SELECT id_marque_pneu, marque_pneu FROM t_marque_pneu  WHERE id_marque_pneu = %(value_id_marque_pneu_selected)s"""

                    mc_afficher.execute(strsql_marquesPneu_afficher, valeur_id_marque_pneu_selected_dictionnaire)
                else:
                    strsql_marquesPneu_afficher = """SELECT id_marque_pneu, marque_pneu FROM t_marque_pneu ORDER BY id_marque_pneu DESC"""

                    mc_afficher.execute(strsql_marquesPneu_afficher)

                data_marque_pneu = mc_afficher.fetchall()

                print("data_marque_pneu ", data_marque_pneu, " Type : ", type(data_marque_pneu))

                # Différencier les messages si la table est vide.
                if not data_marque_pneu and id_marque_pneu_sel == 0:
                    flash("""La table "t_marque_pneu" est vide. !!""", "warning")
                elif not data_marque_pneu and id_marque_pneu_sel > 0:
                    # Si l'utilisateur change l'id_marque_pneu dans l'URL et que la marque n'existe pas,
                    flash(f"La marque demandé n'existe pas !!", "warning")
                else:
                    # Dans tous les autres cas, c'est que la table "t_genre" est vide.
                    # OM 2020.04.09 La ligne ci-dessous permet de donner un sentiment rassurant aux utilisateurs.
                    flash(f"Marques de pneus affichés !!", "success")

        except Exception as erreur:
            print(f"RGG Erreur générale. marquePneu_afficher")
            # OM 2020.04.09 On dérive "Exception" par le "@obj_mon_application.errorhandler(404)"
            # fichier "run_mon_app.py"
            # Ainsi on peut avoir un message d'erreur personnalisé.
            flash(f"RGG Exception {erreur} marquePneu_afficher", "danger")
            raise Exception(f"RGG Erreur générale. {erreur}")
            # raise MaBdErreurOperation(f"RGG Exception {msg_erreurs['ErreurNomBD']['message']} {erreur}")

    # Envoie la page "HTML" au serveur.
    return render_template("marquePneu/marquePneu_afficher.html", data=data_marque_pneu)


"""
    Auteur : OM 2021.03.22
    Définition d'une "route" /marquePneu_ajouter
    
    Test : ex : http://127.0.0.1:5005/marquePneu_ajouter
    
    Paramètres : sans
    
    But : Ajouter une marque pour des pneus
    
    Remarque :  Dans le champ "name_genre_html" du formulaire "marquePneu/marquePneu_ajouter.html",
                le contrôle de la saisie s'effectue ici en Python.
                On transforme la saisie en minuscules.
                On ne doit pas accepter des valeurs vides, des valeurs avec des chiffres,
                des valeurs avec des caractères qui ne sont pas des lettres.
                Pour comprendre [A-Za-zÀ-ÖØ-öø-ÿ] il faut se reporter à la table ASCII https://www.ascii-code.com/
                Accepte le trait d'union ou l'apostrophe, et l'espace entre deux mots, mais pas plus d'une occurence.
"""


@obj_mon_application.route("/marquePneu_ajouter", methods=['GET', 'POST'])
def marquePneu_ajouter_wtf():
    form = FormWTFAjouterMarquePneu()
    if request.method == "POST":
        try:
            try:
                # Renvoie une erreur si la connexion est perdue.
                MaBaseDeDonnee().connexion_bd.ping(False)
            except Exception as erreur:
                flash(f"Dans Gestion genres ...terrible erreur, il faut connecter une base de donnée", "danger")
                print(f"Exception grave Classe constructeur GestionGenres {erreur.args[0]}")
                raise MaBdErreurConnexion(f"{msg_erreurs['ErreurConnexionBD']['message']} {erreur.args[0]}")

            if form.validate_on_submit():
                name_marque_pneu_wtf = form.nom_marque_pneu_wtf.data

                name_marque_pneu = name_marque_pneu_wtf.capitalize()
                valeurs_insertion_dictionnaire = {"value_marque_pneu": name_marque_pneu}
                print("valeurs_insertion_dictionnaire ", valeurs_insertion_dictionnaire)

                strsql_insert_marque_pneu = """INSERT INTO t_marque_pneu (id_marque_pneu,marque_pneu) VALUES (NULL,%(value_marque_pneu)s)"""
                with MaBaseDeDonnee() as mconn_bd:
                    mconn_bd.mabd_execute(strsql_insert_marque_pneu, valeurs_insertion_dictionnaire)

                flash(f"Données insérées !!", "success")
                print(f"Données insérées !!")

                # Pour afficher et constater l'insertion de la valeur, on affiche en ordre inverse. (DESC)
                return redirect(url_for('marquePneu_afficher', order_by='DESC', id_marque_pneu_sel=0))

        # ATTENTION à l'ordre des excepts, il est très important de respecter l'ordre.
        except pymysql.err.IntegrityError as erreur_marque_pneu_doublon:
            # Dérive "pymysql.err.IntegrityError" dans "MaBdErreurDoublon" fichier "erreurs/exceptions.py"
            # Ainsi on peut avoir un message d'erreur personnalisé.
            code, msg = erreur_marque_pneu_doublon.args

            flash(f"{error_codes.get(code, msg)} ", "warning")

        # OM 2020.04.16 ATTENTION à l'ordre des excepts, il est très important de respecter l'ordre.
        except (pymysql.err.OperationalError,
                pymysql.ProgrammingError,
                pymysql.InternalError,
                TypeError) as erreur_gest_marquePneu_crud:
            code, msg = erreur_gest_marquePneu_crud.args

            flash(f"{error_codes.get(code, msg)} ", "danger")
            flash(f"Erreur dans Gestion marquePneu CRUD : {sys.exc_info()[0]} "
                  f"{erreur_gest_marquePneu_crud.args[0]} , "
                  f"{erreur_gest_marquePneu_crud}", "danger")

    return render_template("marquePneu/marquePneu_ajouter_wtf.html", form=form)


"""
    Auteur : OM 2021.03.29
    Définition d'une "route" /marquePneu_update
    
    Test : ex cliquer sur le menu "genres" puis cliquer sur le bouton "EDIT" d'une "marque de pneus"
    
    Paramètres : sans
    
    But : Editer(update) une marque de pneus qui a été sélectionné dans le formulaire "marquePneu_afficher.html"
    
    Remarque :  Dans le champ "nom_marque_pneu_update_wtf" du formulaire "marquePneu/marquePneu_update_wtf.html",
                le contrôle de la saisie s'effectue ici en Python.
                On transforme la saisie en minuscules.
                On ne doit pas accepter des valeurs vides, des valeurs avec des chiffres,
                des valeurs avec des caractères qui ne sont pas des lettres.
                Pour comprendre [A-Za-zÀ-ÖØ-öø-ÿ] il faut se reporter à la table ASCII https://www.ascii-code.com/
                Accepte le trait d'union ou l'apostrophe, et l'espace entre deux mots, mais pas plus d'une occurence.
"""


@obj_mon_application.route("/marquePneu_update", methods=['GET', 'POST'])
def marquePneu_update_wtf():

    # L'utilisateur vient de cliquer sur le bouton "EDIT". Récupère la valeur de "id_marque_pneu"
    id_marque_pneu_update = request.values['id_marque_pneu_btn_edit_html']

    # Objet formulaire pour l'UPDATE
    form_update = FormWTFUpdateMarquePneu()
    try:
        print(" on submit ", form_update.validate_on_submit())
        if form_update.validate_on_submit():
            # Récupèrer la valeur du champ depuis "marquePneu_update_wtf.html" après avoir cliqué sur "SUBMIT".
            # Puis la convertir en lettres minuscules.
            name_marque_pneu_update = form_update.nom_marque_pneu_update_wtf.data
            name_marque_pneu_update = name_marque_pneu_update.capitalize()

            valeur_update_dictionnaire = {"value_id_marque_pneu": id_marque_pneu_update, "value_name_marquePneu": name_marque_pneu_update}
            print("valeur_update_dictionnaire ", valeur_update_dictionnaire)

            str_sql_update_marque_pneu = """UPDATE t_marque_pneu SET marque_pneu = %(value_name_marquePneu)s WHERE id_marque_pneu = %(value_id_marque_pneu)s"""
            with MaBaseDeDonnee() as mconn_bd:
                mconn_bd.mabd_execute(str_sql_update_marque_pneu, valeur_update_dictionnaire)

            flash(f"Donnée mise à jour !!", "success")
            print(f"Donnée mise à jour !!")

            # afficher et constater que la donnée est mise à jour.
            # Affiche seulement la valeur modifiée, "ASC" et l'"id_marque_pneu_update"
            return redirect(url_for('marquePneu_afficher', order_by="ASC", id_marque_pneu_sel=id_marque_pneu_update))
        elif request.method == "GET":
            # Opération sur la BD pour récupérer "id_genre" et "intitule_genre" de la "t_genre"
            str_sql_id_marque_pneu = "SELECT id_marque_pneu, marque_pneu FROM t_marque_pneu WHERE id_marque_pneu = %(value_id_marque_pneu)s"
            valeur_select_dictionnaire = {"value_id_marque_pneu": id_marque_pneu_update}
            mybd_curseur = MaBaseDeDonnee().connexion_bd.cursor()
            mybd_curseur.execute(str_sql_id_marque_pneu, valeur_select_dictionnaire)
            # Une seule valeur est suffisante "fetchone()", vu qu'il n'y a qu'un seul champ "nom marque pneus" pour l'UPDATE
            data_nom_marque_pneu = mybd_curseur.fetchone()
            print("data_nom_marque_pneu ", data_nom_marque_pneu, " type ", type(data_nom_marque_pneu), " marque de pneus ",
                  data_nom_marque_pneu["marque_pneu"])

            # Afficher la valeur sélectionnée dans le champ du formulaire "marquePneu_update_wtf.html"
            form_update.nom_marque_pneu_update_wtf.data = data_nom_marque_pneu["marque_pneu"]

    # OM 2020.04.16 ATTENTION à l'ordre des excepts, il est très important de respecter l'ordre.
    except KeyError:
        flash(f"__KeyError dans marquePneu_update_wtf : {sys.exc_info()[0]} {sys.exc_info()[1]} {sys.exc_info()[2]}",
              "danger")
    except ValueError:
        flash(f"Erreur dans marquePneu_update_wtf : {sys.exc_info()[0]} {sys.exc_info()[1]}", "danger")
    except (pymysql.err.OperationalError,
            pymysql.ProgrammingError,
            pymysql.InternalError,
            pymysql.err.IntegrityError,
            TypeError) as erreur_gest_marquePneu_crud:
        code, msg = erreur_gest_marquePneu_crud.args
        flash(f"attention : {error_codes.get(code, msg)} {erreur_gest_marquePneu_crud} ", "danger")
        flash(f"Erreur dans marquePneu_update_wtf : {sys.exc_info()[0]} "
              f"{erreur_gest_marquePneu_crud.args[0]} , "
              f"{erreur_gest_marquePneu_crud}", "danger")
        flash(f"__KeyError dans marquePneu_update_wtf : {sys.exc_info()[0]} {sys.exc_info()[1]} {sys.exc_info()[2]}",
              "danger")

    return render_template("marquePneu/marquePneu_update_wtf.html", form_update=form_update)


"""
    Auteur : OM 2021.04.08
    Définition d'une "route" /marquePneu_delete
    
    Test : ex. cliquer sur le menu "marque de pneus" puis cliquer sur le bouton "Effacer" d'une "marque de pneus"
    
    Paramètres : sans
    
    But : Effacer(delete) une marque qui a été sélectionné dans le formulaire "marquePneu_afficher.html"
    
    Remarque :  Dans le champ "nom_marquePneu_delete_wtf" du formulaire "marquePneu/marquePneu_delete_wtf.html",
                le contrôle de la saisie est désactivée. On doit simplement cliquer sur "Effacer"
"""


@obj_mon_application.route("/marquePneu_delete", methods=['GET', 'POST'])
def marquePneu_delete_wtf():
    data_Taille_pneu_attribue_marque_pneu_delete = None
    btn_submit_del = None
    # L'utilisateur vient de cliquer sur le bouton "DELETE". Récupère la valeur de "id_genre"
    id_marque_pneu_delete = request.values['id_marque_pneu_btn_delete_html']

    # Objet formulaire pour effacer la marque de pneus sélectionné.
    form_delete = FormWTFDeleteMarquePneu()
    try:
        print(" on submit ", form_delete.validate_on_submit())
        if request.method == "POST" and form_delete.validate_on_submit():

            if form_delete.submit_btn_annuler.data:
                return redirect(url_for("marquePneu_afficher", order_by="ASC", id_marque_pneu_sel=0))

            if form_delete.submit_btn_conf_del.data:
                # Récupère les données afin d'afficher à nouveau
                # le formulaire "marquePneu/marquePneu_delete_wtf.html" lorsque le bouton "Etes-vous sur d'effacer ?" est cliqué.
                data_Taille_pneu_attribue_marque_pneu_delete = session['data_Taille_pneu_attribue_marque_pneu_delete']
                print("data_Taille_pneu_attribue_marque_pneu_delete ", data_Taille_pneu_attribue_marque_pneu_delete)

                flash(f"Effacer la marque de pneus de façon définitive de la BD !!!", "danger")
                # L'utilisateur vient de cliquer sur le bouton de confirmation pour effacer...
                # On affiche le bouton "Effacer marque de pneu" qui va irrémédiablement EFFACER la marque de pneu
                btn_submit_del = True

            if form_delete.submit_btn_del.data:
                valeur_delete_dictionnaire = {"value_id_marque_pneu": id_marque_pneu_delete}
                print("valeur_delete_dictionnaire ", valeur_delete_dictionnaire)

                str_sql_delete_taille_marque = """DELETE FROM t_avoir_marque_pneu WHERE fk_marque_pneu = %(value_id_marque_pneu)s"""
                str_sql_delete_idmarquepneu = """DELETE FROM t_marque_pneu WHERE id_marque_pneu = %(value_id_marque_pneu)s"""
                # Manière brutale d'effacer d'abord la "fk_marque_pneus", même si elle n'existe pas dans la "t_avoir_marque_pneus"
                # Ensuite on peut effacer la marque depneus vu qu'il n'est plus "lié" (INNODB) dans la "t_avoir_marque_pneus"
                with MaBaseDeDonnee() as mconn_bd:
                    mconn_bd.mabd_execute(str_sql_delete_taille_marque, valeur_delete_dictionnaire)
                    mconn_bd.mabd_execute(str_sql_delete_idmarquepneu, valeur_delete_dictionnaire)

                flash(f"Marque de pneus définitivement effacé !!", "success")
                print(f"Marque de pneus définitivement effacé !!")

                # afficher les données
                return redirect(url_for('marquePneu_afficher', order_by="ASC", id_marque_pneu_sel=0))

        if request.method == "GET":
            valeur_select_dictionnaire = {"value_id_marque_pneu": id_marque_pneu_delete}
            print(id_marque_pneu_delete, type(id_marque_pneu_delete))

            # Requête qui affiche tous les films qui ont le genre que l'utilisateur veut effacer
            str_sql_avoir_marque_pneus_delete = """SELECT id_avoir_marque_pneu, Taille_pneu, id_marque_pneu, marque_pneu FROM t_avoir_marque_pneu 
                                            INNER JOIN t_pneu ON t_avoir_marque_pneu.fk_pneu = t_pneu.id_pneu
                                            INNER JOIN t_marque_pneu ON t_avoir_marque_pneu.fk_marque_pneu = t_marque_pneu.id_marque_pneu
                                            WHERE fk_marque_pneu = %(value_id_marque_pneu)s"""

            mybd_curseur = MaBaseDeDonnee().connexion_bd.cursor()

            mybd_curseur.execute(str_sql_avoir_marque_pneus_delete, valeur_select_dictionnaire)
            data_Taille_pneu_attribue_marque_pneu_delete = mybd_curseur.fetchall()
            print("data_Taille_pneu_attribue_marque_pneu_delete...", data_Taille_pneu_attribue_marque_pneu_delete)

            # Nécessaire pour mémoriser les données afin d'afficher à nouveau
            # le formulaire "marquePneu/marquePneu_delete_wtf.html" lorsque le bouton "Etes-vous sur d'effacer ?" est cliqué.
            session['data_Taille_pneu_attribue_marque_pneu_delete'] = data_Taille_pneu_attribue_marque_pneu_delete

            # Opération sur la BD pour récupérer "id_genre" et "intitule_genre" de la "t_genre"
            str_sql_id_marque_pneu = "SELECT id_marque_pneu, marque_pneu FROM t_marque_pneu WHERE id_marque_pneu = %(value_id_marque_pneu)s"

            mybd_curseur.execute(str_sql_id_marque_pneu, valeur_select_dictionnaire)
            # Une seule valeur est suffisante "fetchone()",
            # vu qu'il n'y a qu'un seul champ "nom marque pneu" pour l'action DELETE
            data_nom_marque_pneu = mybd_curseur.fetchone()
            print("data_nom_marque_pneu ", data_nom_marque_pneu, " type ", type(data_nom_marque_pneu), " marque_pneu ",
                  data_nom_marque_pneu["marque_pneu"])

            # Afficher la valeur sélectionnée dans le champ du formulaire "marquePneu_delete_wtf.html"
            form_delete.nom_marque_pneu_delete_wtf.data = data_nom_marque_pneu["marque_pneu"]

            # Le bouton pour l'action "DELETE" dans le form. "marquePneu_delete_wtf.html" est caché.
            btn_submit_del = False

    # OM 2020.04.16 ATTENTION à l'ordre des excepts, il est très important de respecter l'ordre.
    except KeyError:
        flash(f"__KeyError dans marquePneu_delete_wtf : {sys.exc_info()[0]} {sys.exc_info()[1]} {sys.exc_info()[2]}",
              "danger")
    except ValueError:
        flash(f"Erreur dans marquePneu_delete_wtf : {sys.exc_info()[0]} {sys.exc_info()[1]}", "danger")
    except (pymysql.err.OperationalError,
            pymysql.ProgrammingError,
            pymysql.InternalError,
            pymysql.err.IntegrityError,
            TypeError) as erreur_gest_marquePneu_crud:
        code, msg = erreur_gest_marquePneu_crud.args
        flash(f"attention : {error_codes.get(code, msg)} {erreur_gest_marquePneu_crud} ", "danger")

        flash(f"Erreur dans marquePneu_delete_wtf : {sys.exc_info()[0]} "
              f"{erreur_gest_marquePneu_crud.args[0]} , "
              f"{erreur_gest_marquePneu_crud}", "danger")

        flash(f"__KeyError dans marquePneu_delete_wtf : {sys.exc_info()[0]} {sys.exc_info()[1]} {sys.exc_info()[2]}",
              "danger")

    return render_template("marquePneu/marquePneu_delete_wtf.html",
                           form_delete=form_delete,
                           btn_submit_del=btn_submit_del,
                           data_Taille_pneu_associes=data_Taille_pneu_attribue_marque_pneu_delete)
