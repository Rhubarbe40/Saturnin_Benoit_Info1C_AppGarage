"""
    Fichier : gestion_marquePneu_wtf_forms.py
    Auteur : OM 2021.03.22
    Gestion des formulaires avec WTF
"""
from flask_wtf import FlaskForm
from wtforms import StringField
from wtforms import SubmitField
from wtforms.validators import Length
from wtforms.validators import Regexp


class FormWTFAjouterMarquePneu(FlaskForm):
    """
        Dans le formulaire "marquePneu_ajouter_wtf.html" on impose que le champ soit rempli.
        Définition d'un "bouton" submit avec un libellé personnalisé.
    """
    nom_marque_pneu_regexp = "^([A-Z]|[a-zÀ-ÖØ-öø-ÿ])[A-Za-zÀ-ÖØ-öø-ÿ]*['\- ]?[A-Za-zÀ-ÖØ-öø-ÿ]+$"
    nom_marque_pneu_wtf = StringField("Clavioter la marque ", validators=[Length(min=2, max=20, message="min 2 max 20"),
                                                                   Regexp(nom_marque_pneu_regexp,
                                                                          message="Pas de chiffres, de caractères "
                                                                                  "spéciaux, "
                                                                                  "d'espace à double, de double "
                                                                                  "apostrophe, de double trait union")
                                                                   ])
    submit = SubmitField("Enregistrer marque")


class FormWTFUpdateMarquePneu(FlaskForm):
    """
        Dans le formulaire "marquePneu_update_wtf.html" on impose que le champ soit rempli.
        Définition d'un "bouton" submit avec un libellé personnalisé.
    """
    nom_marque_pneu_update_regexp = "^([A-Z]|[a-zÀ-ÖØ-öø-ÿ])[A-Za-zÀ-ÖØ-öø-ÿ]*['\- ]?[A-Za-zÀ-ÖØ-öø-ÿ]+$"
    nom_marque_pneu_update_wtf = StringField("Modifier la marque de pneus ", validators=[Length(min=2, max=20, message="min 2 max 20"),
                                                                          Regexp(nom_marque_pneu_update_regexp,
                                                                                 message="Pas de chiffres, de "
                                                                                         "caractères "
                                                                                         "spéciaux, "
                                                                                         "d'espace à double, de double "
                                                                                         "apostrophe, de double trait "
                                                                                         "union")
                                                                          ])
    submit = SubmitField("Mettre à jour")


class FormWTFDeleteMarquePneu(FlaskForm):
    """
        Dans le formulaire "marquePneu_delete_wtf.html"

        nom_genre_delete_wtf : Champ qui reçoit la valeur du genre, lecture seule. (readonly=true)
        submit_btn_del : Bouton d'effacement "DEFINITIF".
        submit_btn_conf_del : Bouton de confirmation pour effacer un "genre".
        submit_btn_annuler : Bouton qui permet d'afficher la table "t_genre".
    """
    nom_marque_pneu_delete_wtf = StringField("Effacer cette marque de pneus")
    submit_btn_del = SubmitField("Confirmer")
    submit_btn_conf_del = SubmitField("Supprimer")
    submit_btn_annuler = SubmitField("Annuler")
