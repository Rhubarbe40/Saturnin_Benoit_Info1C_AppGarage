-- phpMyAdmin SQL Dump
-- version 4.5.4.1
-- http://www.phpmyadmin.net
--
-- Client :  localhost
-- Généré le :  Lun 29 Mars 2021 à 19:19
-- Version du serveur :  5.7.11
-- Version de PHP :  5.6.18

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `saturnin_benoit_info1c`
--

-- Détection si une autre base de donnée du même nom existe

DROP DATABASE IF EXISTS saturnin_benoit_info1c;

-- Création d'un nouvelle base de donnée

CREATE DATABASE IF NOT EXISTS saturnin_benoit_info1c;

-- Utilisation de cette base de donnée

USE saturnin_benoit_info1c;
-- --------------------------------------------------------

--
-- Structure de la table `t_adresse`
--

CREATE TABLE `t_adresse` (
  `id_adresse` int(11) NOT NULL,
  `nom_rue` varchar(50) DEFAULT NULL,
  `num_rue` varchar(11) DEFAULT NULL,
  `compl_adresse` varchar(50) DEFAULT NULL,
  `num_NPA` int(5) DEFAULT NULL,
  `nom_ville` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `t_adresse`
--

INSERT INTO `t_adresse` (`id_adresse`, `nom_rue`, `num_rue`, `compl_adresse`, `num_NPA`, `nom_ville`) VALUES
(4, 'Asdgsd', '1235 bis', 'SALUT', 1111, 'dejeggf'),
(5, 'Rue de la dîméçäàüè', '13 bis', '1er étage', 1307, 'Lussery-Villars');

-- --------------------------------------------------------

--
-- Structure de la table `t_avoir_adresse`
--

CREATE TABLE `t_avoir_adresse` (
  `id_avoir_adresse` int(11) NOT NULL,
  `fk_personne` int(11) NOT NULL,
  `fk_adresse` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `t_avoir_carte_grise`
--

CREATE TABLE `t_avoir_carte_grise` (
  `id_avoir_carte_grise` int(11) NOT NULL,
  `fk_personne` int(11) NOT NULL,
  `fk_carte_grise` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `t_avoir_carte_grise`
--

INSERT INTO `t_avoir_carte_grise` (`id_avoir_carte_grise`, `fk_personne`, `fk_carte_grise`) VALUES
(3, 3, 3),
(15, 4, 2);

-- --------------------------------------------------------

--
-- Structure de la table `t_avoir_facture`
--

CREATE TABLE `t_avoir_facture` (
  `id_avoir_facture` int(11) NOT NULL,
  `fk_facture` int(11) NOT NULL,
  `fk_personne` int(11) NOT NULL,
  `date_facture` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `t_avoir_facture`
--

INSERT INTO `t_avoir_facture` (`id_avoir_facture`, `fk_facture`, `fk_personne`, `date_facture`) VALUES
(2, 4, 4, '2021-05-28 21:52:13'),
(5, 5, 3, '2021-05-29 00:10:03'),
(8, 2, 4, '2021-05-29 00:21:46'),
(9, 3, 14, '2021-05-29 00:24:59'),
(13, 8, 14, '2021-05-29 23:59:46'),
(14, 13, 9, '2021-06-04 22:26:18'),
(15, 14, 5, '2021-06-05 11:08:31'),
(17, 15, 14, '2021-06-05 12:35:50'),
(18, 16, 9, '2021-06-05 23:58:58'),
(19, 17, 3, '2021-06-06 00:00:14'),
(20, 10, 3, '2021-06-08 22:35:20'),
(21, 12, 14, '2021-06-08 22:35:31');

-- --------------------------------------------------------

--
-- Structure de la table `t_avoir_marque_pneu`
--

CREATE TABLE `t_avoir_marque_pneu` (
  `id_avoir_marque_pneu` int(11) NOT NULL,
  `fk_pneu` int(11) NOT NULL,
  `fk_marque_pneu` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `t_avoir_marque_pneu`
--

INSERT INTO `t_avoir_marque_pneu` (`id_avoir_marque_pneu`, `fk_pneu`, `fk_marque_pneu`) VALUES
(1, 1, 1),
(2, 2, 6);

-- --------------------------------------------------------

--
-- Structure de la table `t_avoir_pneu`
--

CREATE TABLE `t_avoir_pneu` (
  `id_avoir_pneu` int(11) NOT NULL,
  `fk_pneu` int(11) NOT NULL,
  `fk_personne` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `t_avoir_pneu`
--

INSERT INTO `t_avoir_pneu` (`id_avoir_pneu`, `fk_pneu`, `fk_personne`) VALUES
(9, 7, 14),
(10, 1, 4),
(11, 5, 12),
(14, 1, 3);

-- --------------------------------------------------------

--
-- Structure de la table `t_carte_grise`
--

CREATE TABLE `t_carte_grise` (
  `id_carte_grise` int(11) NOT NULL,
  `marque_car` varchar(40) DEFAULT NULL,
  `modele_car` varchar(40) DEFAULT NULL,
  `num_chassis` varchar(30) DEFAULT NULL,
  `num_rcp_type` varchar(8) DEFAULT NULL,
  `date_expert` date DEFAULT NULL,
  `date_mise_circul` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `t_carte_grise`
--

INSERT INTO `t_carte_grise` (`id_carte_grise`, `marque_car`, `modele_car`, `num_chassis`, `num_rcp_type`, `date_expert`, `date_mise_circul`) VALUES
(2, 'Renault', 'Scénic', 'iubfgzoazfbgwe', '1rb487', '2020-12-12', '2019-12-12'),
(3, 'Renault', 'Mégane', 'siuadgbvfowz', '1rb459', '2021-05-31', '2012-04-05'),
(4, 'Audi', 'A3', 'agafgag18a4g', '1aa154', '2020-05-16', '2018-08-20'),
(5, 'Py', 'Talisman', 'qawergqerg184', '1rb498', '2020-05-16', '2012-04-05');

-- --------------------------------------------------------

--
-- Structure de la table `t_facture`
--

CREATE TABLE `t_facture` (
  `id_facture` int(11) NOT NULL,
  `num_facture` int(15) DEFAULT NULL,
  `cout_facture` float DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `t_facture`
--

INSERT INTO `t_facture` (`id_facture`, `num_facture`, `cout_facture`) VALUES
(2, 1032021002, NULL),
(3, 1032021003, NULL),
(4, 12548, 455),
(5, 12235, 1455),
(8, 12354, 455.55),
(10, 12548, 1548.25),
(11, 12388, 12540.9),
(12, 154897, 1254),
(13, 121598, 455.2),
(14, 1216549, 1548),
(15, 123544, 4587),
(16, 21548, 12548),
(17, 1548, 1236);

-- --------------------------------------------------------

--
-- Structure de la table `t_facture_attente`
--

CREATE TABLE `t_facture_attente` (
  `id_facture_attente` int(11) NOT NULL,
  `fk_id_facture` int(11) NOT NULL,
  `date_creation` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `t_marque_pneu`
--

CREATE TABLE `t_marque_pneu` (
  `id_marque_pneu` int(11) NOT NULL,
  `marque_pneu` varchar(40) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `t_marque_pneu`
--

INSERT INTO `t_marque_pneu` (`id_marque_pneu`, `marque_pneu`) VALUES
(1, 'Hankook'),
(2, 'Michelin'),
(3, 'Bridgestone'),
(4, 'Continental'),
(5, 'Lassa'),
(6, 'Toyo Tires'),
(7, 'Pirelli');

-- --------------------------------------------------------

--
-- Structure de la table `t_payer_facture`
--

CREATE TABLE `t_payer_facture` (
  `id_acquiter_facture` int(11) NOT NULL,
  `fk_facture` int(11) NOT NULL,
  `date_paiement` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `t_payer_facture`
--

INSERT INTO `t_payer_facture` (`id_acquiter_facture`, `fk_facture`, `date_paiement`) VALUES
(1, 5, '2021-05-29 00:19:02'),
(2, 4, '2021-05-29 00:19:12'),
(3, 2, '2021-05-29 21:37:14'),
(4, 3, '2021-05-29 23:47:23'),
(5, 8, '2021-06-02 14:20:05'),
(6, 14, '2021-06-05 11:08:51'),
(7, 15, '2021-06-05 12:37:41'),
(8, 11, '2021-06-05 12:39:07');

-- --------------------------------------------------------

--
-- Structure de la table `t_personne`
--

CREATE TABLE `t_personne` (
  `id_personne` int(11) NOT NULL,
  `nom_personne` varchar(42) DEFAULT NULL,
  `prenom_personne` varchar(42) DEFAULT NULL,
  `naissance_personne` date DEFAULT NULL,
  `num_tel` varchar(18) DEFAULT NULL,
  `mail_pers` varchar(254) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `t_personne`
--

INSERT INTO `t_personne` (`id_personne`, `nom_personne`, `prenom_personne`, `naissance_personne`, `num_tel`, `mail_pers`) VALUES
(3, 'Longchamp', 'Cédric', '1976-02-08', '079.310.07.05', 'atelier@mecaniquelongchamp.ch'),
(4, 'Delhausse', 'Eric', '2020-12-12', '079.310.07.05', 'saoaduifg@asdf.ch'),
(5, 'Andrade', 'Christopher', '1997-06-22', '', ''),
(9, 'Pasquier', 'William', '2000-05-16', NULL, NULL),
(10, 'Martin', 'Esteban', '2000-03-16', NULL, NULL),
(11, 'Blbla', 'Adadda', '2001-12-31', NULL, NULL),
(12, 'Saturnin', 'Benoît', '1989-02-27', '077.269.22.29', 'saturnin.benoit@protonmail.com'),
(14, 'Testfin', 'Delapartie', '2020-12-12', '078.123.56.56', 'isudbf@aisudg.ch');

-- --------------------------------------------------------

--
-- Structure de la table `t_pneu`
--

CREATE TABLE `t_pneu` (
  `id_pneu` int(11) NOT NULL,
  `Taille_pneu` varchar(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `t_pneu`
--

INSERT INTO `t_pneu` (`id_pneu`, `Taille_pneu`) VALUES
(1, '205/60R16 88T'),
(2, '205/55R16 88T'),
(3, '205/65R15 91H'),
(4, '205/65R15 91H'),
(5, '225/40r18 91v'),
(6, '245/25r19 94z'),
(7, '205/50R16 91W');

-- --------------------------------------------------------

--
-- Structure de la table `t_rappeler_facture`
--

CREATE TABLE `t_rappeler_facture` (
  `id_rappeler_facture` int(11) NOT NULL,
  `fk_facture` int(11) NOT NULL,
  `date_rappel` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `t_rappeler_facture`
--

INSERT INTO `t_rappeler_facture` (`id_rappeler_facture`, `fk_facture`, `date_rappel`) VALUES
(9, 10, '2021-06-01 21:00:11'),
(11, 12, '2021-06-02 14:44:18'),
(12, 13, '2021-06-04 22:26:32');

--
-- Index pour les tables exportées
--

--
-- Index pour la table `t_adresse`
--
ALTER TABLE `t_adresse`
  ADD PRIMARY KEY (`id_adresse`);

--
-- Index pour la table `t_avoir_adresse`
--
ALTER TABLE `t_avoir_adresse`
  ADD PRIMARY KEY (`id_avoir_adresse`),
  ADD KEY `fk_personne` (`fk_personne`),
  ADD KEY `fk_adresse` (`fk_adresse`);

--
-- Index pour la table `t_avoir_carte_grise`
--
ALTER TABLE `t_avoir_carte_grise`
  ADD PRIMARY KEY (`id_avoir_carte_grise`),
  ADD KEY `fk_personne` (`fk_personne`),
  ADD KEY `fk_hobby` (`fk_carte_grise`);

--
-- Index pour la table `t_avoir_facture`
--
ALTER TABLE `t_avoir_facture`
  ADD PRIMARY KEY (`id_avoir_facture`),
  ADD KEY `fk_facture` (`fk_facture`),
  ADD KEY `fk_personne` (`fk_personne`);

--
-- Index pour la table `t_avoir_marque_pneu`
--
ALTER TABLE `t_avoir_marque_pneu`
  ADD PRIMARY KEY (`id_avoir_marque_pneu`),
  ADD KEY `fk_pneu` (`fk_pneu`),
  ADD KEY `fk_marque_pneu` (`fk_marque_pneu`);

--
-- Index pour la table `t_avoir_pneu`
--
ALTER TABLE `t_avoir_pneu`
  ADD PRIMARY KEY (`id_avoir_pneu`),
  ADD KEY `fk_pneu` (`fk_pneu`),
  ADD KEY `fk_voiture` (`fk_personne`);

--
-- Index pour la table `t_carte_grise`
--
ALTER TABLE `t_carte_grise`
  ADD PRIMARY KEY (`id_carte_grise`);

--
-- Index pour la table `t_facture`
--
ALTER TABLE `t_facture`
  ADD PRIMARY KEY (`id_facture`);

--
-- Index pour la table `t_facture_attente`
--
ALTER TABLE `t_facture_attente`
  ADD PRIMARY KEY (`id_facture_attente`),
  ADD KEY `fk_id_facture` (`fk_id_facture`);

--
-- Index pour la table `t_marque_pneu`
--
ALTER TABLE `t_marque_pneu`
  ADD PRIMARY KEY (`id_marque_pneu`);

--
-- Index pour la table `t_payer_facture`
--
ALTER TABLE `t_payer_facture`
  ADD PRIMARY KEY (`id_acquiter_facture`),
  ADD KEY `fk_facture` (`fk_facture`);

--
-- Index pour la table `t_personne`
--
ALTER TABLE `t_personne`
  ADD PRIMARY KEY (`id_personne`);

--
-- Index pour la table `t_pneu`
--
ALTER TABLE `t_pneu`
  ADD PRIMARY KEY (`id_pneu`);

--
-- Index pour la table `t_rappeler_facture`
--
ALTER TABLE `t_rappeler_facture`
  ADD PRIMARY KEY (`id_rappeler_facture`),
  ADD KEY `fk_facture` (`fk_facture`);

--
-- AUTO_INCREMENT pour les tables exportées
--

--
-- AUTO_INCREMENT pour la table `t_adresse`
--
ALTER TABLE `t_adresse`
  MODIFY `id_adresse` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT pour la table `t_avoir_adresse`
--
ALTER TABLE `t_avoir_adresse`
  MODIFY `id_avoir_adresse` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `t_avoir_carte_grise`
--
ALTER TABLE `t_avoir_carte_grise`
  MODIFY `id_avoir_carte_grise` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT pour la table `t_avoir_facture`
--
ALTER TABLE `t_avoir_facture`
  MODIFY `id_avoir_facture` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;
--
-- AUTO_INCREMENT pour la table `t_avoir_marque_pneu`
--
ALTER TABLE `t_avoir_marque_pneu`
  MODIFY `id_avoir_marque_pneu` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT pour la table `t_avoir_pneu`
--
ALTER TABLE `t_avoir_pneu`
  MODIFY `id_avoir_pneu` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT pour la table `t_carte_grise`
--
ALTER TABLE `t_carte_grise`
  MODIFY `id_carte_grise` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT pour la table `t_facture`
--
ALTER TABLE `t_facture`
  MODIFY `id_facture` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;
--
-- AUTO_INCREMENT pour la table `t_facture_attente`
--
ALTER TABLE `t_facture_attente`
  MODIFY `id_facture_attente` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `t_marque_pneu`
--
ALTER TABLE `t_marque_pneu`
  MODIFY `id_marque_pneu` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT pour la table `t_payer_facture`
--
ALTER TABLE `t_payer_facture`
  MODIFY `id_acquiter_facture` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT pour la table `t_personne`
--
ALTER TABLE `t_personne`
  MODIFY `id_personne` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT pour la table `t_pneu`
--
ALTER TABLE `t_pneu`
  MODIFY `id_pneu` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT pour la table `t_rappeler_facture`
--
ALTER TABLE `t_rappeler_facture`
  MODIFY `id_rappeler_facture` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- Contraintes pour les tables exportées
--

--
-- Contraintes pour la table `t_avoir_adresse`
--
ALTER TABLE `t_avoir_adresse`
  ADD CONSTRAINT `t_avoir_adresse_ibfk_1` FOREIGN KEY (`fk_personne`) REFERENCES `t_personne` (`id_personne`),
  ADD CONSTRAINT `t_avoir_adresse_ibfk_2` FOREIGN KEY (`fk_adresse`) REFERENCES `t_adresse` (`id_adresse`);

--
-- Contraintes pour la table `t_avoir_carte_grise`
--
ALTER TABLE `t_avoir_carte_grise`
  ADD CONSTRAINT `t_avoir_carte_grise_ibfk_1` FOREIGN KEY (`fk_personne`) REFERENCES `t_personne` (`id_personne`),
  ADD CONSTRAINT `t_avoir_carte_grise_ibfk_2` FOREIGN KEY (`fk_carte_grise`) REFERENCES `t_carte_grise` (`id_carte_grise`);

--
-- Contraintes pour la table `t_avoir_facture`
--
ALTER TABLE `t_avoir_facture`
  ADD CONSTRAINT `t_avoir_facture_ibfk_1` FOREIGN KEY (`fk_facture`) REFERENCES `t_facture` (`id_facture`),
  ADD CONSTRAINT `t_avoir_facture_ibfk_2` FOREIGN KEY (`fk_personne`) REFERENCES `t_personne` (`id_personne`);

--
-- Contraintes pour la table `t_avoir_marque_pneu`
--
ALTER TABLE `t_avoir_marque_pneu`
  ADD CONSTRAINT `t_avoir_marque_pneu_ibfk_1` FOREIGN KEY (`fk_pneu`) REFERENCES `t_pneu` (`id_pneu`),
  ADD CONSTRAINT `t_avoir_marque_pneu_ibfk_2` FOREIGN KEY (`fk_marque_pneu`) REFERENCES `t_marque_pneu` (`id_marque_pneu`);

--
-- Contraintes pour la table `t_avoir_pneu`
--
ALTER TABLE `t_avoir_pneu`
  ADD CONSTRAINT `t_avoir_pneu_ibfk_1` FOREIGN KEY (`fk_pneu`) REFERENCES `t_pneu` (`id_pneu`),
  ADD CONSTRAINT `t_avoir_pneu_ibfk_2` FOREIGN KEY (`fk_personne`) REFERENCES `t_personne` (`id_personne`);

--
-- Contraintes pour la table `t_facture_attente`
--
ALTER TABLE `t_facture_attente`
  ADD CONSTRAINT `t_facture_attente_ibfk_1` FOREIGN KEY (`fk_id_facture`) REFERENCES `t_facture` (`id_facture`);

--
-- Contraintes pour la table `t_payer_facture`
--
ALTER TABLE `t_payer_facture`
  ADD CONSTRAINT `t_payer_facture_ibfk_1` FOREIGN KEY (`fk_facture`) REFERENCES `t_facture` (`id_facture`);

--
-- Contraintes pour la table `t_rappeler_facture`
--
ALTER TABLE `t_rappeler_facture`
  ADD CONSTRAINT `t_rappeler_facture_ibfk_1` FOREIGN KEY (`fk_facture`) REFERENCES `t_facture` (`id_facture`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
