"""
    Fichier : gestion_genres_wtf_forms.py
    Auteur : OM 2021.03.22
    Gestion des formulaires avec WTF
"""
from flask_wtf import FlaskForm
from wtforms import StringField
from wtforms import DateField
from wtforms import SubmitField
from wtforms.validators import Length
from wtforms.validators import Regexp


class FormWTFAjouterAdresse(FlaskForm):
    """
        Dans le formulaire "genres_ajouter_wtf.html" on impose que le champ soit rempli.
        Définition d'un "bouton" submit avec un libellé personnalisé.
    """
    nom_rue_regexp = "^[a-zA-Z0-9îüéäàçè_. '-]*$"
    nom_rue_wtf = StringField("Clavioter le nom de la rue ", validators=[Length(min=2, max=40, message="min 2 max 40"),
                                                                   Regexp(nom_rue_regexp,
                                                                          message="pas de symbole ou caractère spéciaux type @, # etc...")
                                                                   ])
    num_rue_regexp = "^[a-zA-Z0-9_. -]*$"
    num_rue_wtf = StringField("Clavioter le N° de la rue ", validators=[Length(min=2, max=40, message="min 2 max 40"),
                                                                   Regexp(num_rue_regexp,
                                                                          message="pas de symbole ou caractère spéciaux type @, # etc...")
                                                                   ])

    compl_adresse_wtf = StringField("Clavioter le complément d'adresse ")

    num_NPA_regexp = "^[0-9]*$"
    num_NPA_wtf = StringField("Clavioter le NPA ", validators=[Length(min=4, max=6, message="min 4, max 6"),
                                                                   Regexp(num_NPA_regexp,
                                                                          message="Uniquement des chiffres")
                                                                   ])
    nom_ville_regexp = "^[a-zA-Zîüéäàçè_. '-]+$"
    nom_ville_wtf = StringField("Clavioter le nom de la ville ", validators=[Length(min=1, max=40, message="max 40 lettres"),
                                                                   Regexp(nom_ville_regexp,
                                                                          message="pas de chiffre ")
                                                                   ])

    submit = SubmitField("Enregistrer l'adresse")


class FormWTFUpdateAdresse(FlaskForm):
    """
        Dans le formulaire "genre_update_wtf.html" on impose que le champ soit rempli.
        Définition d'un "bouton" submit avec un libellé personnalisé.
    """
    nom_rue_update_regexp = "^[a-zA-Z0-9îüéäàçè_.' -]*$"
    nom_rue_update_wtf = StringField("Clavioter le nom de la rue ", validators=[Length(min=2, max=20, message="min 2 max 20"),
                                                                    Regexp(nom_rue_update_regexp,
                                                                          message="pas de symbole ou caractère spéciaux type @, # etc...")
                                                                          ])
    num_rue_update_regexp = "^[a-zA-Z0-9_. -]*$"
    num_rue_update_wtf = StringField("Clavioter le N° de la rue ", validators=[Length(min=2, max=40, message="min 2 max 20"),
                                                                   Regexp(num_rue_update_regexp,
                                                                          message="pas de symbole ou caractère spéciaux type @, # etc...")
                                                                   ])

    compl_adresse_update_wtf = StringField("Clavioter le complément d'adresse ")

    num_NPA_update_regexp = "^[0-9]*$"
    num_NPA_update_wtf = StringField("Clavioter le NPA ", validators=[Length(min=4, max=6, message="min 4, max 6"),
                                                                   Regexp(num_NPA_update_regexp,
                                                                          message="Uniquement des chiffres")
                                                                   ])
    nom_ville_update_regexp = "^[a-zA-Z0-9îüéäàçè_. '-]*$"
    nom_ville_update_wtf = StringField("Clavioter le nom de la ville ", validators=[Length(min=1, max=40, message="max 40 lettres"),
                                                                   Regexp(nom_ville_update_regexp,
                                                                          message="pas de chiffre")
                                                                   ])
    submit = SubmitField("Mettre à jour l'adresse")


class FormWTFDeleteAdresse(FlaskForm):
    """
        Dans le formulaire "genre_delete_wtf.html"

        nom_genre_delete_wtf : Champ qui reçoit la valeur du genre, lecture seule. (readonly=true)
        submit_btn_del : Bouton d'effacement "DEFINITIF".
        submit_btn_conf_del : Bouton de confirmation pour effacer un "genre".
        submit_btn_annuler : Bouton qui permet d'afficher la table "t_genre".
    """
    nom_rue_delete_wtf = StringField("Effacer cette adresse")
    submit_btn_del = SubmitField("Effacer adresse")
    submit_btn_conf_del = SubmitField("Etes-vous sur d'effacer ?")
    submit_btn_annuler = SubmitField("Annuler")
