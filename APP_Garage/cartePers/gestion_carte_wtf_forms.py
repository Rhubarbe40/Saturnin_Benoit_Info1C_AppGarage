"""
    Fichier : gestion_genres_wtf_forms.py
    Auteur : OM 2021.03.22
    Gestion des formulaires avec WTF
"""
from flask_wtf import FlaskForm
from wtforms import StringField, DateField, widgets
from wtforms import SelectField
from wtforms import SubmitField
from wtforms.validators import Length, Required
from wtforms.validators import Regexp


class FormWTFAjouterCarte(FlaskForm):
    """
        Dans le formulaire "genres_ajouter_wtf.html" on impose que le champ soit rempli.
        Définition d'un "bouton" submit avec un libellé personnalisé.
    """
    marque_regexp = "^([A-Z]|[a-zÀ-ÖØ-öø-ÿ])[A-Za-zÀ-ÖØ-öø-ÿ]*['\- ]?[A-Za-zÀ-ÖØ-öø-ÿ]+$"
    marque_wtf = StringField("Clavioter la marque du véhicule ", validators=[Length(min=2, max=40, message="min 2 max 40"),
                                                                   Regexp(marque_regexp,
                                                                          message="Pas de chiffres, de caractères "
                                                                                  "spéciaux, "
                                                                                  "d'espace à double, de double "
                                                                                  "apostrophe, de double trait union")
                                                                   ])
    modele_regexp = "^[a-zA-Z0-9_ .-]*$"
    modele_wtf = StringField("Clavioter le modèle de la marque ", validators=[Length(min=2, max=40, message="min 2 max 40"),
                                                                   Regexp(modele_regexp,
                                                                          message="pas de caractère spéciaux")
                                                                   ])
    num_chassis_regexp = "^[a-zA-Z0-9_.-]*$"
    num_chassis_wtf = StringField("Clavioter le N° chassis ", validators=[Length(min=1, max=30, message="min 2 max 30"),
                                                                   Regexp(num_chassis_regexp,
                                                                          message="Format exacte de la carte grise")
                                                                   ])
    num_rcp_type_regexp = "^[a-zA-Z0-9_.-]*$"
    num_rcp_type_wtf = StringField("Clavioter le N° de récéption par type ", validators=[Length(min=1, max=20, message="min 10, max 20"),
                                                                   Regexp(num_rcp_type_regexp,
                                                                          message="Pas de chiffres, de caractères "
                                                                                  "spéciaux, "
                                                                                  "d'espace à double, de double "
                                                                                  "apostrophe, de double trait union")
                                                                   ])
    date_exp_regexp = "^(19|20)\d\d[- /.](0[1-9]|1[012])[- /.](0[1-9]|[12][0-9]|3[01])$"

    date_exp_wtf = StringField("Clavioter la date de la dernière expertise ", validators=[Length(min=1, max=10, message="max 10"),
                                                                   Regexp(date_exp_regexp,
                                                                          message="format AAAA-MM-JJ")
                                                                   ])
    date_first_cir_regexp = "^(19|20)\d\d[- /.](0[1-9]|1[012])[- /.](0[1-9]|[12][0-9]|3[01])$"
    date_first_cir_wtf = StringField("Clavioter la première mise en circulation ", validators=[Length(min=1, max=254, message="max 254"),
                                                                   Regexp(date_first_cir_regexp,
                                                                          message="format AAAA-MM-JJ")
                                                                   ])

    submit = SubmitField("Enregistrer la carte grise")


class FormWTFUpdateCarte(FlaskForm):
    """
        Dans le formulaire "genre_update_wtf.html" on impose que le champ soit rempli.
        Définition d'un "bouton" submit avec un libellé personnalisé.
    """
    marque_update_regexp = "^([A-Z]|[a-zÀ-ÖØ-öø-ÿ])[A-Za-zÀ-ÖØ-öø-ÿ]*['\- ]?[A-Za-zÀ-ÖØ-öø-ÿ]+$"
    marque_update_wtf = StringField("Clavioter la marque du véhicule ", validators=[Length(min=2, max=20, message="min 2 max 20"),
                                                                    Regexp(marque_update_regexp,
                                                                          message="Pas de chiffres, de "
                                                                                         "caractères "
                                                                                         "spéciaux, "
                                                                                         "d'espace à double, de double "
                                                                                         "apostrophe, de double trait "
                                                                                         "union")
                                                                          ])
    modele_update_regexp = "^([A-Z]|[a-zÀ-ÖØ-öø-ÿ])[A-Za-zÀ-ÖØ-öø-ÿ]*['\- ]?[A-Za-zÀ-ÖØ-öø-ÿ]+$"
    modele_update_wtf = StringField("Clavioter le modèle de la marque ", validators=[Length(min=2, max=40, message="min 2 max 20"),
                                                                   Regexp(modele_update_regexp,
                                                                          message="Pas de chiffres, de caractères "
                                                                                  "spéciaux, "
                                                                                  "d'espace à double, de double "
                                                                                  "apostrophe, de double trait union")
                                                                   ])
    num_chassis_update_regexp = "^([A-Z]|[a-zÀ-ÖØ-öø-ÿ])[A-Za-zÀ-ÖØ-öø-ÿ]*['\- ]?[A-Za-zÀ-ÖØ-öø-ÿ]+$"
    num_chassis_update_wtf = StringField("Clavioter le N° chassis ", validators=[Length(min=1, max=20, message="max 20"),
                                                                   Regexp(num_chassis_update_regexp,
                                                                          message="Format AAAA-MM-JJ")
                                                                   ])
    num_rcp_type_update_regexp = "^[a-zA-Z0-9_.-]*$"
    num_rcp_type_update_wtf = StringField("Clavioter le N° de récéption par type ", validators=[Length(min=1, max=10, message="min 1, max 10"),
                                                                   Regexp(num_rcp_type_update_regexp,
                                                                          message="Pas de chiffres, de caractères "
                                                                                  "spéciaux, "
                                                                                  "d'espace à double, de double "
                                                                                  "apostrophe, de double trait union")
                                                                   ])
    date_exp_update_regexp = "^(19|20)\d\d[- /.](0[1-9]|1[012])[- /.](0[1-9]|[12][0-9]|3[01])$"
    date_exp_update_wtf = StringField("Clavioter la date de la dernière expertise ", validators=[Length(min=1, max=20, message="max 254"),
                                                                   Regexp(date_exp_update_regexp,
                                                                          message="nomPrenom@domaineOuAutre.chComFr")
                                                                   ])
    date_first_cir_update_regexp = "^(19|20)\d\d[- /.](0[1-9]|1[012])[- /.](0[1-9]|[12][0-9]|3[01])$"
    date_first_cir_update_wtf = StringField("Clavioter la date de la première mise en circualtion ", validators=[Length(min=1, max=20, message="max 254"),
                                                                   Regexp(date_first_cir_update_regexp,
                                                                          message="nomPrenom@domaineOuAutre.chComFr")
                                                  ])
    submit = SubmitField("Update genre")


class FormWTFDeleteCarte(FlaskForm):
    """
        Dans le formulaire "genre_delete_wtf.html"

        nom_genre_delete_wtf : Champ qui reçoit la valeur du genre, lecture seule. (readonly=true)
        submit_btn_del : Bouton d'effacement "DEFINITIF".
        submit_btn_conf_del : Bouton de confirmation pour effacer un "genre".
        submit_btn_annuler : Bouton qui permet d'afficher la table "t_genre".
    """
    marque_delete_wtf = StringField("Effacer cette personne")
    submit_btn_del = SubmitField("Effacer personne")
    submit_btn_conf_del = SubmitField("Etes-vous sur d'effacer ?")
    submit_btn_annuler = SubmitField("Annuler")
