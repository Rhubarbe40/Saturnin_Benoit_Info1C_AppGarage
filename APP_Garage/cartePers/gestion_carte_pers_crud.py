"""
    Fichier : gestion_films_genres_crud.py
    Auteur : OM 2021.05.01
    Gestions des "routes" FLASK et des données pour l'association entre les films et les genres.
"""
import sys

import pymysql
from flask import flash
from flask import redirect
from flask import render_template
from flask import request
from flask import session
from flask import url_for

from APP_Garage import obj_mon_application
from APP_Garage.database.connect_db_context_manager import MaBaseDeDonnee
from APP_Garage.erreurs.exceptions import *
from APP_Garage.erreurs.msg_erreurs import *
from APP_Garage.cartePers.gestion_carte_wtf_forms import FormWTFAjouterCarte
from APP_Garage.cartePers.gestion_carte_wtf_forms import FormWTFDeleteCarte
from APP_Garage.cartePers.gestion_carte_wtf_forms import FormWTFUpdateCarte

"""
    Nom : cartePers_afficher
    Auteur : OM 2021.05.01
    Définition d'une "route" /cartePers_afficher
    
    But : Afficher les personnes avec les personnes associés pour chaque carte grise.
    
    Paramètres : id_carte_grise_sel = 0 >> toutes les personnes.
                 id_carte_grise_sel = "n" affiche la personne dont l'id est "n"
                 
"""


@obj_mon_application.route("/cartePers_afficher/<int:id_carte_grise_sel>", methods=['GET', 'POST'])
def cartePers_afficher(id_carte_grise_sel):
    if request.method == "GET":
        try:
            try:
                # Renvoie une erreur si la connexion est perdue.
                MaBaseDeDonnee().connexion_bd.ping(False)
            except Exception as Exception_init_cartePers_afficher:
                code, msg = Exception_init_cartePers_afficher.args
                flash(f"{error_codes.get(code, msg)} ", "danger")
                flash(f"Exception _init_cartePers_afficher problème de connexion BD : {sys.exc_info()[0]} "
                      f"{Exception_init_cartePers_afficher.args[0]} , "
                      f"{Exception_init_cartePers_afficher}", "danger")
                raise MaBdErreurConnexion(f"{msg_erreurs['ErreurConnexionBD']['message']} {erreur.args[0]}")

            with MaBaseDeDonnee().connexion_bd.cursor() as mc_afficher:
                strsql_cartePers_afficher_data = """SELECT id_carte_grise, marque_car, modele_car, num_chassis, num_rcp_type, date_expert, date_mise_circul,
                                                            GROUP_CONCAT(nom_personne," ", prenom_personne) as cartePers FROM t_avoir_carte_grise
                                                            RIGHT JOIN t_carte_grise ON t_carte_grise.id_carte_grise = t_avoir_carte_grise.fk_carte_grise
                                                            LEFT JOIN t_personne ON t_personne.id_personne = t_avoir_carte_grise.fk_personne
                                                            GROUP BY id_carte_grise"""
                if id_carte_grise_sel == 0:
                    # le paramètre 0 permet d'afficher tous les films
                    # Sinon le paramètre représente la valeur de l'id du film
                    mc_afficher.execute(strsql_cartePers_afficher_data)
                else:
                    # Constitution d'un dictionnaire pour associer l'id de la personne sélectionné avec un nom de variable
                    valeur_id_carte_grise_selected_dictionnaire = {"value_id_carte_grise_selected": id_carte_grise_sel}
                    # En MySql l'instruction HAVING fonctionne comme un WHERE... mais doit être associée à un GROUP BY
                    # L'opérateur += permet de concaténer une nouvelle valeur à la valeur de gauche préalablement définie.
                    strsql_cartePers_afficher_data += """ HAVING id_carte_grise= %(value_id_carte_grise_selected)s"""

                    mc_afficher.execute(strsql_cartePers_afficher_data, valeur_id_carte_grise_selected_dictionnaire)

                # Récupère les données de la requête.
                data_cartePers_afficher = mc_afficher.fetchall()
                print("data_pers ", data_cartePers_afficher, " Type : ", type(data_cartePers_afficher))

                # Différencier les messages.
                if not data_cartePers_afficher and id_carte_grise_sel == 0:
                    flash("""La table "t_carte_grise" est vide. !""", "warning")
                elif not data_cartePers_afficher and id_carte_grise_sel > 0:
                    # Si l'utilisateur change l'id_carte_grise dans l'URL et qu'il ne correspond à aucune personne
                    flash(f"La carte grise {id_carte_grise_sel} demandé n'existe pas !!", "warning")
                else:
                    flash(f"Données Cartes grises et personnes affichés !!", "success")

        except Exception as Exception_cartePers_afficher:
            code, msg = Exception_cartePers_afficher.args
            flash(f"{error_codes.get(code, msg)} ", "danger")
            flash(f"Exception cartePers_afficher : {sys.exc_info()[0]} "
                  f"{Exception_cartePers_afficher.args[0]} , "
                  f"{Exception_cartePers_afficher}", "danger")

    # Envoie la page "HTML" au serveur.
    return render_template("cartePers/cartePers_afficher.html", data=data_cartePers_afficher)


"""
    nom: edit_cartePers_selected
    On obtient un objet "objet_dumpbd"

    Récupère la liste de toutes les personnes des cartes grises sélectionné par le bouton "MODIFIER" de "cartePers_afficher.html"
    
    Dans une liste déroulante particulière (tags-selector-tagselect), on voit :
    1) Tous les personnes contenus dans la "t_personnes".
    2) Les personnes attribués au cartes grises selectionné.
    3) Les personnes non-attribués au cartes grises sélectionné.

    On signale les erreurs importantes

"""


@obj_mon_application.route("/edit_cartePers_selected", methods=['GET', 'POST'])
def edit_cartePers_selected():
    if request.method == "GET":
        try:
            with MaBaseDeDonnee().connexion_bd.cursor() as mc_afficher:
                strsql_pers_afficher = """SELECT id_personne, nom_personne FROM t_personne ORDER BY id_personne ASC"""
                mc_afficher.execute(strsql_pers_afficher)
            data_pers_all = mc_afficher.fetchall()
            print("dans edit_cartePers_selected ---> data_pers_all", data_pers_all)

            # Récupère la valeur de "id_carte_grise" du formulaire html "cartePers_afficher.html"
            # l'utilisateur clique sur le bouton "Modifier" et on récupère la valeur de "id_carte_grise"
            # grâce à la variable "id_cartePers_edit_html" dans le fichier "cartePers_afficher.html"
            # href="{{ url_for('edit_cartePers_selected', id_cartePers_edit_html=row.id_carte_grise) }}"
            id_cartePers_edit = request.values['id_cartePers_edit_html']

            # Mémorise l'id de la carte grise dans une variable de session
            # (ici la sécurité de l'application n'est pas engagée)
            # il faut éviter de stocker des données sensibles dans des variables de sessions.
            session['session_id_cartePers_edit'] = id_cartePers_edit

            # Constitution d'un dictionnaire pour associer l'id du film sélectionné avec un nom de variable
            valeur_id_carte_grise_selected_dictionnaire = {"value_id_carte_grise_selected": id_cartePers_edit}

            # Récupère les données grâce à 3 requêtes MySql définie dans la fonction cartePers_afficher_data
            # 1) Sélection de la personne choisi
            # 2) Sélection des personnes "déjà" attribués pour la carte.
            # 3) Sélection des personnes "pas encore" attribués pour la carte choisi.
            # ATTENTION à l'ordre d'assignation des variables retournées par la fonction "cartePers_afficher_data"
            data_cartePers_selected, data_cartePers_non_attribues, data_cartePers_attribues = \
                cartePers_afficher_data(valeur_id_carte_grise_selected_dictionnaire)

            print(data_cartePers_selected)
            lst_data_carte_grise_selected = [item['id_carte_grise'] for item in data_cartePers_selected]
            print("lst_data_carte_grise_selected  ", lst_data_carte_grise_selected,
                  type(lst_data_carte_grise_selected))

            # Dans le composant "tags-selector-tagselect" on doit connaître
            # les personnes qui ne sont pas encore sélectionnés.
            lst_data_cartePers_non_attribues = [item['id_personne'] for item in data_cartePers_non_attribues]
            session['session_lst_data_cartePers_non_attribues'] = lst_data_cartePers_non_attribues
            print("lst_data_cartePers_non_attribues  ", lst_data_cartePers_non_attribues,
                  type(lst_data_cartePers_non_attribues))

            # Dans le composant "tags-selector-tagselect" on doit connaître
            # les personnes qui sont déjà sélectionnés.
            lst_data_cartePers_old_attribues = [item['id_personne'] for item in data_cartePers_attribues]
            session['session_lst_data_cartePers_old_attribues'] = lst_data_cartePers_old_attribues
            print("lst_data_cartePers_old_attribues  ", lst_data_cartePers_old_attribues,
                  type(lst_data_cartePers_old_attribues))

            print(" data data_cartePers_selected", data_cartePers_selected, "type ", type(data_cartePers_selected))
            print(" data data_cartePers_non_attribues ", data_cartePers_non_attribues, "type ",
                  type(data_cartePers_non_attribues))
            print(" data_cartePers_attribues ", data_cartePers_attribues, "type ",
                  type(data_cartePers_attribues))

            # Extrait les valeurs contenues dans la table "t_personne", colonne "nom_personne_"
            # Le composant javascript "tagify" pour afficher les tags n'a pas besoin de l'id_personne
            lst_data_cartePers_non_attribues = [item['nom_personne'] for item in data_cartePers_non_attribues]
            print("lst_all_pers gf_edit_cartePers_selected ", lst_data_cartePers_non_attribues,
                  type(lst_data_cartePers_non_attribues))

        except Exception as Exception_edit_cartePers_selected:
            code, msg = Exception_edit_cartePers_selected.args
            flash(f"{error_codes.get(code, msg)} ", "danger")
            flash(f"Exception edit_cartePers_selected : {sys.exc_info()[0]} "
                  f"{Exception_edit_cartePers_selected.args[0]} , "
                  f"{Exception_edit_cartePers_selected}", "danger")

    return render_template("cartePers/cartePers_modifier_tags_dropbox.html",
                           #data_genres=data_pers_all,
                           data_carte_selected=data_cartePers_selected,
                           data_pers_attribues=data_cartePers_attribues,
                           data_pers_non_attribues=data_cartePers_non_attribues)


"""
    nom: update_cartePers_selected

    Récupère la liste de toutes les personnes de la carte grise sélectionnée par le bouton "MODIFIER" de "cartePers_afficher.html"
    
    Dans une liste déroulante particulière (tags-selector-tagselect), on voit :
    1) Tous les peronnes contenus dans la "t_personne".
    2) Les personnes attribués à la carte grise selectionné.
    3) Les cartePers non-attribués à la carte grise sélectionné.

    On signale les erreurs importantes
"""


@obj_mon_application.route("/update_cartePers_selected", methods=['GET', 'POST'])
def update_cartePers_selected():
    if request.method == "POST":
        try:
            # Récupère l'id de la carte grise sélectionnée
            id_carte_grise_selected = session['session_id_cartePers_edit']
            print("session['session_id_cartePers_edit'] ", session['session_id_cartePers_edit'])

            # Récupère la liste des personnes qui ne sont pas associés à la carte grise sélectionnée.
            old_lst_data_cartePers_non_attribues = session['session_lst_data_cartePers_non_attribues']
            print("old_lst_data_cartePers_non_attribues ", old_lst_data_cartePers_non_attribues)

            # Récupère la liste des personnes qui sont associés à la carte grise sélectionnée.
            old_lst_data_cartePers_attribues = session['session_lst_data_cartePers_old_attribues']
            print("old_lst_data_cartePers_old_attribues ", old_lst_data_cartePers_attribues)

            # Effacer toutes les variables de session.
            session.clear()

            # Récupère ce que l'utilisateur veut modifier comme personne dans le composant "tags-selector-tagselect"
            # dans le fichier "cartePers_modifier_tags_dropbox.html"
            new_lst_str_cartePers = request.form.getlist('name_select_tags')
            print("new_lst_str_cartePers ", new_lst_str_cartePers)

            # OM 2021.05.02 Exemple : Dans "name_select_tags" il y a ['4','65','2']
            # On transforme en une liste de valeurs numériques. [4,65,2]
            new_lst_int_cartePers_old = list(map(int, new_lst_str_cartePers))
            print("new_lst_cartePers ", new_lst_int_cartePers_old, "type new_lst_cartePers ",
                  type(new_lst_int_cartePers_old))

            # Pour apprécier la facilité de la vie en Python... "les ensembles en Python"
            # https://fr.wikibooks.org/wiki/Programmation_Python/Ensembles
            # OM 2021.05.02 Une liste de "id_personne" qui doivent être effacés de la table intermédiaire "t_avoir_carte_grise".
            lst_diff_pers_delete_b = list(
                set(old_lst_data_cartePers_attribues) - set(new_lst_int_cartePers_old))
            print("lst_diff_pers_delete_b ", lst_diff_pers_delete_b)

            # Une liste de "id_personne" qui doivent être ajoutés à la "t_avoir_carte_grise"
            lst_diff_pers_insert_a = list(
                set(new_lst_int_cartePers_old) - set(old_lst_data_cartePers_attribues))
            print("lst_diff_pers_insert_a ", lst_diff_pers_insert_a)

            # SQL pour insérer une nouvelle association entre
            # "fk_carte_grise"/"id_carte_grise" et "fk_personne"/"id_personne" dans la "t_avoir_carte_grise"
            strsql_insert_cartePers = """INSERT INTO t_avoir_carte_grise (id_avoir_carte_grise, fk_personne, fk_carte_grise)
                                                    VALUES (NULL, %(value_fk_personne)s, %(value_fk_carte_grise)s)"""

            # SQL pour effacer une (des) association(s) existantes entre "id_carte_grise" et "id_personne" dans la "t_avoir_carte_grise"
            strsql_delete_cartePers = """DELETE FROM t_avoir_carte_grise WHERE fk_personne = %(value_fk_personne)s AND fk_carte_grise = %(value_fk_carte_grise)s"""

            with MaBaseDeDonnee() as mconn_bd:
                # Pour la carte grise sélectionné, parcourir la liste des personnes à INSÉRER dans la "t_avoir_carte_grise".
                # Si la liste est vide, la boucle n'est pas parcourue.
                for id_pers_ins in lst_diff_pers_insert_a:
                    # Constitution d'un dictionnaire pour associer l'id de la carte grise sélectionné avec un nom de variable
                    # et "id_pers_ins" (l'id de la personne dans la liste) associé à une variable.
                    valeurs_carte_sel_pers_sel_dictionnaire = {"value_fk_carte_grise": id_carte_grise_selected,
                                                               "value_fk_personne": id_pers_ins}

                    mconn_bd.mabd_execute(strsql_insert_cartePers, valeurs_carte_sel_pers_sel_dictionnaire)

                # Pour la carte grise sélectionné, parcourir la liste des personnes à EFFACER dans la "t_avoir_carte_grise".
                # Si la liste est vide, la boucle n'est pas parcourue.
                for id_pers_del in lst_diff_pers_delete_b:
                    # Constitution d'un dictionnaire pour associer l'id de la personne sélectionné avec un nom de variable
                    # et "id_pers_del" (l'id du personne dans la liste) associé à une variable.
                    valeurs_carte_sel_pers_sel_dictionnaire = {"value_fk_carte_grise": id_carte_grise_selected,
                                                               "value_fk_personne": id_pers_del}

                    # Du fait de l'utilisation des "context managers" on accède au curseur grâce au "with".
                    # la subtilité consiste à avoir une méthode "mabd_execute" dans la classe "MaBaseDeDonnee"
                    # ainsi quand elle aura terminé l'insertion des données le destructeur de la classe "MaBaseDeDonnee"
                    # sera interprété, ainsi on fera automatiquement un commit
                    mconn_bd.mabd_execute(strsql_delete_cartePers, valeurs_carte_sel_pers_sel_dictionnaire)

        except Exception as Exception_update_cartePers_selected:
            code, msg = Exception_update_cartePers_selected.args
            flash(f"{error_codes.get(code, msg)} ", "danger")
            flash(f"Exception update_cartePers_selected : {sys.exc_info()[0]} "
                  f"{Exception_update_cartePers_selected.args[0]} , "
                  f"{Exception_update_cartePers_selected}", "danger")

    # Après cette mise à jour de la table intermédiaire "t_avoir_carte_grise",
    # on affiche les cartes grises et le(urs) personne(s) associé(s).
    return redirect(url_for('cartePers_afficher', id_carte_grise_sel=id_carte_grise_selected))


"""
    nom: cartePers_afficher_data

    Récupère la liste de toutes les personnes de la carte grises sélectionné par le bouton "MODIFIER" de "cartePers_afficher.html"
    Nécessaire pour afficher tous les "TAGS" des personnes, ainsi l'utilisateur voit les personnes à disposition

    On signale les erreurs importantes
"""


def cartePers_afficher_data(valeur_id_carte_grise_selected_dict):
    print("valeur_id_carte_grise_selected_dict...", valeur_id_carte_grise_selected_dict)
    try:

        strsql_carte_grise_selected = """SELECT id_carte_grise, marque_car, modele_car, num_chassis, num_rcp_type, date_expert, date_mise_circul, GROUP_CONCAT(id_personne) as cartePers FROM t_avoir_carte_grise
                                        INNER JOIN t_carte_grise ON t_carte_grise.id_carte_grise = t_avoir_carte_grise.fk_carte_grise
                                        INNER JOIN t_personne ON t_personne.id_personne = t_avoir_carte_grise.fk_personne
                                        WHERE id_carte_grise = %(value_id_carte_grise_selected)s"""

        strsql_cartePers_non_attribues = """SELECT id_personne, nom_personne FROM t_personne WHERE id_personne not in(SELECT id_personne as idcartePers FROM t_avoir_carte_grise
                                                    INNER JOIN t_carte_grise ON t_carte_grise.id_carte_grise = t_avoir_carte_grise.fk_carte_grise
                                                    INNER JOIN t_personne ON t_personne.id_personne = t_avoir_carte_grise.fk_personne
                                                    WHERE id_carte_grise = %(value_id_carte_grise_selected)s)"""

        strsql_cartePers_attribues = """SELECT id_carte_grise, id_personne, nom_personne FROM t_avoir_carte_grise
                                            INNER JOIN t_carte_grise ON t_carte_grise.id_carte_grise = t_avoir_carte_grise.fk_carte_grise
                                            INNER JOIN t_personne ON t_personne.id_personne = t_avoir_carte_grise.fk_personne
                                            WHERE id_carte_grise = %(value_id_carte_grise_selected)s"""

        # Du fait de l'utilisation des "context managers" on accède au curseur grâce au "with".
        with MaBaseDeDonnee().connexion_bd.cursor() as mc_afficher:
            # Envoi de la commande MySql
            mc_afficher.execute(strsql_cartePers_non_attribues, valeur_id_carte_grise_selected_dict)
            # Récupère les données de la requête.
            data_cartePers_non_attribues = mc_afficher.fetchall()
            # Affichage dans la console
            print("cartePers_afficher_data ----> data_cartePers_non_attribues ", data_cartePers_non_attribues,
                  " Type : ",
                  type(data_cartePers_non_attribues))

            # Envoi de la commande MySql
            mc_afficher.execute(strsql_carte_grise_selected, valeur_id_carte_grise_selected_dict)
            # Récupère les données de la requête.
            data_carte_grise_selected = mc_afficher.fetchall()
            # Affichage dans la console
            print("data_carte_grise_selected  ", data_carte_grise_selected, " Type : ", type(data_carte_grise_selected))

            # Envoi de la commande MySql
            mc_afficher.execute(strsql_cartePers_attribues, valeur_id_carte_grise_selected_dict)
            # Récupère les données de la requête.
            data_cartePers_attribues = mc_afficher.fetchall()
            # Affichage dans la console
            print("data_cartePers_attribues ", data_cartePers_attribues, " Type : ",
                  type(data_cartePers_attribues))

            # Retourne les données des "SELECT"
            return data_carte_grise_selected, data_cartePers_non_attribues, data_cartePers_attribues
    except pymysql.Error as pymysql_erreur:
        code, msg = pymysql_erreur.args
        flash(f"{error_codes.get(code, msg)} ", "danger")
        flash(f"pymysql.Error Erreur dans cartePers_afficher_data : {sys.exc_info()[0]} "
              f"{pymysql_erreur.args[0]} , "
              f"{pymysql_erreur}", "danger")
    except Exception as exception_erreur:
        code, msg = exception_erreur.args
        flash(f"{error_codes.get(code, msg)} ", "danger")
        flash(f"Exception Erreur dans cartePers_afficher_data : {sys.exc_info()[0]} "
              f"{exception_erreur.args[0]} , "
              f"{exception_erreur}", "danger")
    except pymysql.err.IntegrityError as IntegrityError_cartePers_afficher_data:
        code, msg = IntegrityError_cartePers_afficher_data.args
        flash(f"{error_codes.get(code, msg)} ", "danger")
        flash(f"pymysql.err.IntegrityError Erreur dans cartePers_afficher_data : {sys.exc_info()[0]} "
              f"{IntegrityError_cartePers_afficher_data.args[0]} , "
              f"{IntegrityError_cartePers_afficher_data}", "danger")


"""
    Auteur : OM 2021.03.22
    Définition d'une "route" /carte_ajouter

    Test : ex : http://127.0.0.1:5005/pers_ajouter

    Paramètres : sans

    But : Ajouter une personne

    Remarque :  Dans le champ "name_genre_html" du formulaire "pers/pers_ajouter.html",
                le contrôle de la saisie s'effectue ici en Python.
                On transforme la saisie en minuscules.
                On ne doit pas accepter des valeurs vides, des valeurs avec des chiffres,
                des valeurs avec des caractères qui ne sont pas des lettres.
                Pour comprendre [A-Za-zÀ-ÖØ-öø-ÿ] il faut se reporter à la table ASCII https://www.ascii-code.com/
                Accepte le trait d'union ou l'apostrophe, et l'espace entre deux mots, mais pas plus d'une occurence.
"""


@obj_mon_application.route("/carte_ajouter", methods=['GET', 'POST'])
def carte_ajouter_wtf():
    form = FormWTFAjouterCarte()
    if request.method == "POST":
        try:
            try:
                # Renvoie une erreur si la connexion est perdue.
                MaBaseDeDonnee().connexion_bd.ping(False)
            except Exception as erreur:
                flash(f"Dans Gestion carte_pers ...terrible erreur, il faut connecter une base de donnée", "danger")
                print(f"Exception grave Classe constructeur Gestion carte_pers {erreur.args[0]}")
                raise MaBdErreurConnexion(f"{msg_erreurs['ErreurConnexionBD']['message']} {erreur.args[0]}")

            if form.validate_on_submit():
                marque_wtf = form.marque_wtf.data
                modele_wtf = form.modele_wtf.data
                num_chassis_wtf = form.num_chassis_wtf.data
                num_rcp_type_wtf = form.num_rcp_type_wtf.data
                date_exp_wtf = form.date_exp_wtf.data
                date_first_cir_wtf = form.date_first_cir_wtf.data

                marque = marque_wtf.capitalize()
                modele = modele_wtf.capitalize()
                num_chassis = num_chassis_wtf
                num_rcp_type = num_rcp_type_wtf
                date_exp = date_exp_wtf
                date_first_cir = date_first_cir_wtf

                valeurs_insertion_dictionnaire = {"value_marque_car": marque,
                                                  "value_modele_car": modele,
                                                  "value_num_chassis": num_chassis,
                                                  "value_num_rcp_type": num_rcp_type,
                                                  "value_date_expert": date_exp,
                                                  "value_date_mise_circul": date_first_cir}

                print("valeurs_insertion_dictionnaire ", valeurs_insertion_dictionnaire)

                strsql_insert_carte = """INSERT INTO t_carte_grise (id_carte_grise,marque_car,modele_car,num_chassis,num_rcp_type,date_expert,date_mise_circul) VALUES (NULL,%(value_marque_car)s,%(value_modele_car)s,%(value_num_chassis)s,%(value_num_rcp_type)s,%(value_date_expert)s,%(value_date_mise_circul)s)"""
                with MaBaseDeDonnee() as mconn_bd:
                    mconn_bd.mabd_execute(strsql_insert_carte, valeurs_insertion_dictionnaire)

                flash(f"Données insérées !!", "success")
                print(f"Données insérées !!")

                # Pour afficher et constater l'insertion de la valeur, on affiche en ordre inverse. (DESC)
                return redirect(url_for('cartePers_afficher', order_by='DESC', id_carte_grise_sel=0))

        # ATTENTION à l'ordre des excepts, il est très important de respecter l'ordre.
        except pymysql.err.IntegrityError as erreur_carte_doublon:
            # Dérive "pymysql.err.IntegrityError" dans "MaBdErreurDoublon" fichier "erreurs/exceptions.py"
            # Ainsi on peut avoir un message d'erreur personnalisé.
            code, msg = erreur_carte_doublon.args

            flash(f"{error_codes.get(code, msg)} ", "warning")

        # OM 2020.04.16 ATTENTION à l'ordre des excepts, il est très important de respecter l'ordre.
        except (pymysql.err.OperationalError,
                pymysql.ProgrammingError,
                pymysql.InternalError,
                TypeError) as erreur_gest_genr_crud:
            code, msg = erreur_gest_genr_crud.args

            flash(f"{error_codes.get(code, msg)} ", "danger")
            flash(f"Erreur dans Gestion genres CRUD : {sys.exc_info()[0]} "
                  f"{erreur_gest_genr_crud.args[0]} , "
                  f"{erreur_gest_genr_crud}", "danger")

    return render_template("carte/carte_ajouter_wtf.html", form=form)


"""
    Auteur : OM 2021.03.29
    Définition d'une "route" /carte_update

    Test : ex cliquer sur le menu "carte / Personne" puis cliquer sur le bouton "EDIT" d'une "carte grise"

    Paramètres : sans

    But : Editer(update) une carte grise qui a été sélectionné dans le formulaire "cartePers_afficher.html"

    Remarque :  Dans le champ "nom_genre_update_wtf" du formulaire "genres/genre_update_wtf.html",
                le contrôle de la saisie s'effectue ici en Python.
                On transforme la saisie en minuscules.
                On ne doit pas accepter des valeurs vides, des valeurs avec des chiffres,
                des valeurs avec des caractères qui ne sont pas des lettres.
                Pour comprendre [A-Za-zÀ-ÖØ-öø-ÿ] il faut se reporter à la table ASCII https://www.ascii-code.com/
                Accepte le trait d'union ou l'apostrophe, et l'espace entre deux mots, mais pas plus d'une occurence.
"""


@obj_mon_application.route("/carte_update", methods=['GET', 'POST'])
def carte_update_wtf():
    # L'utilisateur vient de cliquer sur le bouton "EDIT". Récupère la valeur de "id_genre"
    id_carte_grise_update = request.values['id_carte_grise_btn_edit_html']

    # Objet formulaire pour l'UPDATE
    form_update = FormWTFUpdateCarte()
    try:
        print(" on submit ", form_update.validate_on_submit())
        if form_update.validate_on_submit():
            # Récupèrer la valeur du champ depuis "genre_update_wtf.html" après avoir cliqué sur "SUBMIT".
            # Puis la convertir en lettres minuscules.

            marque_update_wtf = form_update.marque_update_wtf.data
            modele_update_wtf = form_update.modele_update_wtf.data
            num_chassis_update_wtf = form_update.num_chassis_update_wtf.data
            num_rcp_type_update_wtf = form_update.num_rcp_type_update_wtf.data
            date_exp_update_wtf = form_update.date_exp_update_wtf.data
            date_first_cir_update_wtf = form_update.date_first_cir_update_wtf.data

            marque_update = marque_update_wtf.capitalize()
            modele_update = modele_update_wtf.capitalize()
            num_chassis_update = num_chassis_update_wtf
            num_rcp_type_update = num_rcp_type_update_wtf
            date_exp_update = date_exp_update_wtf
            date_first_cir_update = date_first_cir_update_wtf

            valeur_update_dictionnaire = {"value_id_carte_grise": id_carte_grise_update,
                                          "value_marque_car": marque_update,
                                          "value_modele_car": modele_update,
                                          "value_num_chassis": num_chassis_update,
                                          "value_num_rcp_type": num_rcp_type_update,
                                          "value_date_expert": date_exp_update,
                                          "value_date_mise_circul": date_first_cir_update}

            print("valeur_update_dictionnaire ", valeur_update_dictionnaire)

            str_sql_update_intitulegenre = """UPDATE t_carte_grise SET marque_car = %(value_marque_car)s, modele_car = %(value_modele_car)s, num_chassis = %(value_num_chassis)s, num_rcp_type = %(value_num_rcp_type)s, date_expert = %(value_date_expert)s, date_mise_circul = %(value_date_mise_circul)s  WHERE id_carte_grise = %(value_id_carte_grise)s"""
            with MaBaseDeDonnee() as mconn_bd:
                mconn_bd.mabd_execute(str_sql_update_intitulegenre, valeur_update_dictionnaire)

            flash(f"Donnée mise à jour !!", "success")
            print(f"Donnée mise à jour !!")

            # afficher et constater que la donnée est mise à jour.
            # Affiche seulement la valeur modifiée, "ASC" et l'"id_genre_update"
            return redirect(url_for('cartePers_afficher', order_by="ASC", id_carte_grise_sel=id_carte_grise_update))
        elif request.method == "GET":
            # Opération sur la BD pour récupérer "id_genre" et "intitule_genre" de la "t_genre"
            str_sql_id_genre = "SELECT id_carte_grise, marque_car, modele_car, num_chassis, num_rcp_type, date_expert, date_mise_circul FROM t_carte_grise WHERE id_carte_grise = %(value_id_carte_grise)s"
            valeur_select_dictionnaire = {"value_id_carte_grise": id_carte_grise_update}
            mybd_curseur = MaBaseDeDonnee().connexion_bd.cursor()
            mybd_curseur.execute(str_sql_id_genre, valeur_select_dictionnaire)
            # Une seule valeur est suffisante "fetchone()", vu qu'il n'y a qu'un seul champ "nom genre" pour l'UPDATE
            data_nom_genre = mybd_curseur.fetchone()
            print("data_nom_genre ", data_nom_genre, " type ", type(data_nom_genre), " genre ",
                  data_nom_genre["marque_car"])

            # Afficher la valeur sélectionnée dans le champ du formulaire "genre_update_wtf.html"
            form_update.marque_update_wtf.data = data_nom_genre["marque_car"]
            form_update.modele_update_wtf.data = data_nom_genre["modele_car"]
            form_update.num_chassis_update_wtf.data = data_nom_genre["num_chassis"]
            form_update.num_rcp_type_update_wtf.data = data_nom_genre["num_rcp_type"]
            form_update.date_exp_update_wtf.data = data_nom_genre["date_expert"]
            form_update.date_first_cir_update_wtf.data = data_nom_genre["date_mise_circul"]

    # OM 2020.04.16 ATTENTION à l'ordre des excepts, il est très important de respecter l'ordre.
    except KeyError:
        flash(f"__KeyError dans pers_update_wtf : {sys.exc_info()[0]} {sys.exc_info()[1]} {sys.exc_info()[2]}",
              "danger")
    except ValueError:
        flash(f"Erreur dans pers_update_wtf : {sys.exc_info()[0]} {sys.exc_info()[1]}", "danger")
    except (pymysql.err.OperationalError,
            pymysql.ProgrammingError,
            pymysql.InternalError,
            pymysql.err.IntegrityError,
            TypeError) as erreur_gest_genr_crud:
        code, msg = erreur_gest_genr_crud.args
        flash(f"attention : {error_codes.get(code, msg)} {erreur_gest_genr_crud} ", "danger")
        flash(f"Erreur dans carte_update_wtf : {sys.exc_info()[0]} "
              f"{erreur_gest_genr_crud.args[0]} , "
              f"{erreur_gest_genr_crud}", "danger")
        flash(f"__KeyError dans carte_update_wtf : {sys.exc_info()[0]} {sys.exc_info()[1]} {sys.exc_info()[2]}",
              "danger")

    return render_template("carte/carte_update_wtf.html", form_update=form_update)


"""
    Auteur : OM 2021.04.08
    Définition d'une "route" /carte_delete

    Test : ex. cliquer sur le menu "genres" puis cliquer sur le bouton "DELETE" d'un "genre"

    Paramètres : sans

    But : Effacer(delete) un genre qui a été sélectionné dans le formulaire "persPneu_afficher.html"

    Remarque :  Dans le champ "nom_genre_delete_wtf" du formulaire "pers/pers_delete_wtf.html",
                le contrôle de la saisie est désactivée. On doit simplement cliquer sur "DELETE"
"""


@obj_mon_application.route("/carte_delete", methods=['GET', 'POST'])
def carte_delete_wtf():
    data_films_attribue_genre_delete = None
    btn_submit_del = None
    # L'utilisateur vient de cliquer sur le bouton "DELETE". Récupère la valeur de "id_genre"
    id_carte_grise_delete = request.values['id_carte_grise_btn_delete_html']

    # Objet formulaire pour effacer le genre sélectionné.
    form_delete = FormWTFDeleteCarte()
    try:
        print(" on submit ", form_delete.validate_on_submit())
        if request.method == "POST" and form_delete.validate_on_submit():

            if form_delete.submit_btn_annuler.data:
                return redirect(url_for("cartePers_afficher", order_by="ASC", id_carte_grise_sel=0))

            if form_delete.submit_btn_conf_del.data:
                # Récupère les données afin d'afficher à nouveau
                # le formulaire "genres/genre_delete_wtf.html" lorsque le bouton "Etes-vous sur d'effacer ?" est cliqué.
                data_films_attribue_genre_delete = session['data_films_attribue_genre_delete']
                print("data_films_attribue_genre_delete ", data_films_attribue_genre_delete)

                flash(f"Effacer la carte grise de façon définitive de la BD !!!", "danger")
                # L'utilisateur vient de cliquer sur le bouton de confirmation pour effacer...
                # On affiche le bouton "Effacer genre" qui va irrémédiablement EFFACER le genre
                btn_submit_del = True

            if form_delete.submit_btn_del.data:
                valeur_delete_dictionnaire = {"value_id_carte_grise": id_carte_grise_delete}
                print("valeur_delete_dictionnaire ", valeur_delete_dictionnaire)

                str_sql_delete_carte_pers = """DELETE FROM t_avoir_carte_grise WHERE fk_carte_grise = %(value_id_carte_grise)s"""
                str_sql_delete_id_carte_grise = """DELETE FROM t_carte_grise WHERE id_carte_grise = %(value_id_carte_grise)s"""
                # Manière brutale d'effacer d'abord la "fk_genre", même si elle n'existe pas dans la "t_genre_film"
                # Ensuite on peut effacer le genre vu qu'il n'est plus "lié" (INNODB) dans la "t_genre_film"
                with MaBaseDeDonnee() as mconn_bd:
                    mconn_bd.mabd_execute(str_sql_delete_carte_pers, valeur_delete_dictionnaire)
                    mconn_bd.mabd_execute(str_sql_delete_id_carte_grise, valeur_delete_dictionnaire)

                flash(f"carte grise définitivement effacé !!", "success")
                print(f"carte grise définitivement effacé !!")

                # afficher les données
                return redirect(url_for('cartePers_afficher', order_by="ASC", id_carte_grise_sel=0))

        if request.method == "GET":
            valeur_select_dictionnaire = {"value_id_carte_grise": id_carte_grise_delete}
            print(id_carte_grise_delete, type(id_carte_grise_delete))

            # Requête qui affiche tous les films_genres qui ont le genre que l'utilisateur veut effacer
            str_sql_genres_films_delete = """SELECT id_avoir_carte_grise, nom_personne, id_carte_grise, marque_car FROM t_avoir_carte_grise 
                                            INNER JOIN t_carte_grise ON t_avoir_carte_grise.fk_carte_grise = t_carte_grise.id_carte_grise
                                            INNER JOIN t_personne ON t_avoir_carte_grise.fk_personne = t_personne.id_personne
                                            WHERE fk_carte_grise = %(value_id_carte_grise)s"""

            mybd_curseur = MaBaseDeDonnee().connexion_bd.cursor()

            mybd_curseur.execute(str_sql_genres_films_delete, valeur_select_dictionnaire)
            data_films_attribue_genre_delete = mybd_curseur.fetchall()
            print("data_films_attribue_genre_delete...", data_films_attribue_genre_delete)

            # Nécessaire pour mémoriser les données afin d'afficher à nouveau
            # le formulaire "genres/genre_delete_wtf.html" lorsque le bouton "Etes-vous sur d'effacer ?" est cliqué.
            session['data_films_attribue_genre_delete'] = data_films_attribue_genre_delete

            # Opération sur la BD pour récupérer "id_personne" et "nom_personne" de la "t_personne"
            str_sql_id_genre = "SELECT id_carte_grise, marque_car FROM t_carte_grise WHERE id_carte_grise = %(value_id_carte_grise)s"

            mybd_curseur.execute(str_sql_id_genre, valeur_select_dictionnaire)
            # Une seule valeur est suffisante "fetchone()",
            # vu qu'il n'y a qu'un seul champ "nom genre" pour l'action DELETE
            data_marque = mybd_curseur.fetchone()
            print("data_nom_genre ", data_marque, " type ", type(data_marque), " marque ",
                  data_marque["marque_car"])

            # Afficher la valeur sélectionnée dans le champ du formulaire "genre_delete_wtf.html"
            form_delete.marque_delete_wtf.data = data_marque["marque_car"]

            # Le bouton pour l'action "DELETE" dans le form. "genre_delete_wtf.html" est caché.
            btn_submit_del = False

    # OM 2020.04.16 ATTENTION à l'ordre des excepts, il est très important de respecter l'ordre.
    except KeyError:
        flash(f"__KeyError dans carte_delete_wtf : {sys.exc_info()[0]} {sys.exc_info()[1]} {sys.exc_info()[2]}",
              "danger")
    except ValueError:
        flash(f"Erreur dans carte_delete_wtf : {sys.exc_info()[0]} {sys.exc_info()[1]}", "danger")
    except (pymysql.err.OperationalError,
            pymysql.ProgrammingError,
            pymysql.InternalError,
            pymysql.err.IntegrityError,
            TypeError) as erreur_gest_genr_crud:
        code, msg = erreur_gest_genr_crud.args
        flash(f"attention : {error_codes.get(code, msg)} {erreur_gest_genr_crud} ", "danger")

        flash(f"Erreur dans pers_delete_wtf : {sys.exc_info()[0]} "
              f"{erreur_gest_genr_crud.args[0]} , "
              f"{erreur_gest_genr_crud}", "danger")

        flash(f"__KeyError dans carte_delete_wtf : {sys.exc_info()[0]} {sys.exc_info()[1]} {sys.exc_info()[2]}",
              "danger")

    return render_template("carte/carte_delete_wtf.html",
                           form_delete=form_delete,
                           btn_submit_del=btn_submit_del,
                           data_personne_associes=data_films_attribue_genre_delete)

