Bienvenu dans votre application de gestion de données client et facturation
---


# Pour faire foncionner cette base de donnée, il vous faudra préparer votre ordinateur avec certain paramètre :
## la première chose à faire avant de commencer les étapes d'installation est de télécharger, selon votre matériel, PyCharm en cliquant ici --> https://www.jetbrains.com/fr-fr/pycharm/download/#section=windows
* pour l'installation de PyCharm, vous devez cocher la case "Ajouter le chemin" ou "Add to the PATH" 
* lancez l'application PyCharm
* sur la page d'accueil, dans l'onglet Project, cliquez sur le bouton "Get From VCS"
* Si GitLab n'est pas installé, Cliqué sur le lien "DownLaud Git..."
* une fois Git installé, copier, coller le lien --> https://gitlab.com/Rhubarbe40/Saturnin_Benoit_Info1C_AppGarage.git puis cliquez sur le bouton "Clone"
* attendez patiemment que l'toutes l'installation se fasse correctement.
* quand PyCharm vous le demendera, accepter d'installer l'environement de la base de données
### une fois tout cela effectué, vous pouvez passer à l'étape 1
## étape 1 : Téléchargement de l'interpréteur de language Python
* Rendez-vous à l'adresse suivante pour téléchargé la version 3.9.2 de python --> https://www.python.org/downloads/release/python-392/.

#### IMPORTANT, lors de l'insallation, n'oubliez pas de cocher la case " Add to the path ".
* Téléchargez la version de python selon votre matériel :
  * Windows
  * MacOS
  * linux
  
### Une fois l'insallation réussie, rendez-vous à l'étape N°2. Sinon, supprimer " python 3.9.2 " et recommencez à l'étape N°1.

## étape 2 : Téléchargement du serveur local UwAmp 
* Si vous possédez du matériel Apple, rendez-vous à l'étape 2 bis.
* rendez-vous à cette adresse pour télécharger le serveur local --> https://www.uwamp.com/fr/?page=download.
* installez la version 3.1.0 de UwAmp en cliquant sur " Télécharger Exe/Install (36 Mo)  ".

## étape 2 bis (Apple): Téléchargement du serveur local Xampp
* si vous êtes sur une interface MacOs, vous allez devoir télécharger le serveur Xampp
* rendez-vous ici pour ce faire et téléchargé, comme toujours, la version en rapport avec votre matériel --> https://www.apachefriends.org/fr/index.html
* Une fois téléchargé, allez chercher le fichier .env et modifier la ligne 10 (PASS_MYSQL="root") comme cela PASS_MYSQL=""

### Une fois l'installation réussie, rendez-vous à l'étape N°3. Sinon, supprimer " UwAmp " ou " Xampp " et recommencez à l'étape N°2.

## étape 3 : Démarrage du serveur local UwAmp
* cliquez sur l'icône UwAmp de votre bureau afin de lancer le démarrage du serveur.
* Cliquez sur le bouton " démarrer " en haut à gauche de la fenêtre qui s'affiche.
* Constater que le serveur " Apache "  et " MySQL " sont bien démarrés

##### IMPORTANT, par la suite, l'étape N°3, ci-dessus, sera obligatoire pour faire fonctionner cette application. Ne l'oubliez pas !

## étape 4 : Importation de la base de données
* Vous allez devoir importer la base de données dans le PHPMyAdmin
* Pour ce faire, faites un clic droite sur le fichier "1_ImportationDumpSql.py" qui se trouve dans le répertoire "zzzdemos"
* allez sur la fenêtre du serveur UwAmp et cliquer sur le bouton "PHPMyAdmin"
* Claviotez le UserID et le mot de passe avec "root" puis Enter.
* Constatez l'apparition de la base de donnée dans le menu de gauche. Elle se nomera "saturnin_benoit_info1c"
* Si vous constatez que la base n'est pas présente, cliquez sur le bouton de mise à jour en haut à droite du menu (icône de sychronisation), et revérifiez.
* En cas de d'erreur, vérifier que dans le fichier ".env" que le UserID et le mot de passe sont bien définis avec les valeurs citées dans le point 4 de l'étape 4.

##### IMPORTANT, cette étape est unique. une fois effectué, vous allez bientôt pouvoir priofiter, enfin, du remplissage de votre nouvelle base de données.

## étape final : lancement du fichier "1_run_server_flask.py"
* une fois toutes les étapes réussie et sans erreur, vous allez pouvoir profiter de votre application tant attendue.

# ATTENTION, une fois le fichier "1_run_server_flask.py" lancé, merci de réduire (pas fermer !) sans plus attendre votre application PyCharm afin d'éviter tout problème de modification du code de votre application. 




 



